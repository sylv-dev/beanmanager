/******************************************************************************* This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors: Revilloud Marc - initial API and implementation ******************************************************************************/
open module BeanManager {
	exports org.beanmanager;
	exports org.beanmanager.editors;
	exports org.beanmanager.editors.basic;
	exports org.beanmanager.editors.container;
	exports org.beanmanager.editors.multiNumber;
	exports org.beanmanager.editors.multiNumber.matrix;
	exports org.beanmanager.editors.multiNumber.point;
	exports org.beanmanager.editors.multiNumber.vector;
	exports org.beanmanager.editors.primitive;
	exports org.beanmanager.editors.primitive.number;
	exports org.beanmanager.editors.time;
	exports org.beanmanager.ihmtest;
	exports org.beanmanager.rmi;
	exports org.beanmanager.rmi.client;
	exports org.beanmanager.rmi.server;
	exports org.beanmanager.struct;
	exports org.beanmanager.testbean;
	exports org.beanmanager.tools;

	requires java.desktop;
	requires java.rmi;
	requires javafx.controls;
	requires transitive vecmath;
	requires transitive org.scenicview.scenicview;
	requires java.base;
}