/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.tools;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import javafx.application.Platform;
import javafx.geometry.Point2D;
import javafx.stage.Screen;

public class FxUtils {

	private FxUtils() {}

	public static void runLaterIfNeeded(Runnable runnable) {
		if (Platform.isFxApplicationThread())
			runnable.run();
		else
			try {
				Platform.runLater(runnable);
			} catch (IllegalStateException e) {
				runnable.run();
			}
	}

	public static boolean isLocationInScreenBounds(Point2D position) {
		for (Screen screen : Screen.getScreens())
			if (screen.getVisualBounds().contains(position))
				return true;
		return false;
	}

	public static void traverse() {
		// impl_traverse(Direction.NEXT); RT-21596
		try {
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_TAB);
			robot.keyRelease(KeyEvent.VK_TAB);
		} catch (AWTException ex) {
			ex.printStackTrace();
		}
	}
}
