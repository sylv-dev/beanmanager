/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.tools;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.EventListener;
import java.util.function.Consumer;

import org.beanmanager.editors.PropertyViewChangeListener;

public class EventListenerList<T extends EventListener> {
	private volatile ImmutableWeakList<T> listenerWeakList;
	private volatile Object[] listenerStrongList;

	public static void main(String[] args) {
		EventListenerList<PropertyViewChangeListener> listenerList = new EventListenerList<>();
		/* PropertyViewChangeListener val = */test(listenerList);
		new Thread(() -> {
			while (true) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				listenerList.forEach(litener -> litener.viewchanged("patate", false));
				System.out.println(listenerList.getListenerCount());
				System.out.println(listenerList);
				// System.out.println(val);
				System.gc();
				listenerList.cleanWeakList();
				System.out.println(listenerList);
			}
		}).start();
	}

	private static PropertyViewChangeListener test(EventListenerList<PropertyViewChangeListener> listenerList) {
		PropertyViewChangeListener strongListener = new PropertyViewChangeListener() {

			@Override
			public void viewchanged(String propertyName, boolean reloadValue) {
				System.out.println("listener strong 1");
			}

			@Override
			public String toString() {
				return "listener strong 1";
			}
		};
		PropertyViewChangeListener strongListener2 = new PropertyViewChangeListener() {

			@Override
			public void viewchanged(String propertyName, boolean reloadValue) {
				System.out.println("listener strong 2");
			}

			@Override
			public String toString() {
				return "listener strong 2";
			}
		};
		listenerList.addStrongRef(strongListener);
		listenerList.forEach(litener -> litener.viewchanged("patate", false));
		listenerList.addStrongRef(strongListener2);
		listenerList.forEach(litener -> litener.viewchanged("patate", false));
		// listenerList.removeStrongRef(strongListener2);
		// listenerList.forEach(litener -> litener.viewchanged("patate", false));
		// listenerList.removeStrongRef(strongListener);
		// listenerList.forEach(litener -> litener.viewchanged("patate", false));

		PropertyViewChangeListener weakListener = new PropertyViewChangeListener() {

			@Override
			public void viewchanged(String propertyName, boolean reloadValue) {
				System.out.println("listener weak 1");
			}

			@Override
			public String toString() {
				return "listener weak 1";
			}
		};
		PropertyViewChangeListener weakListener2 = new PropertyViewChangeListener() {

			@Override
			public void viewchanged(String propertyName, boolean reloadValue) {
				System.out.println("listener weak 2");
			}

			@Override
			public String toString() {
				return "listener weak 2";
			}
		};
		listenerList.addWeakRef(weakListener);
		listenerList.forEach(litener -> litener.viewchanged("patate", false));
		listenerList.addWeakRef(weakListener2);
		listenerList.forEach(litener -> litener.viewchanged("patate", false));
		// listenerList.removeWeakRef(weakListener2);
		// listenerList.forEach(litener -> litener.viewchanged("patate", false));
		// listenerList.removeWeakRef(weakListener);
		// listenerList.forEach(litener -> litener.viewchanged("patate", false));
		return weakListener;
	}

	public synchronized void addStrongRef(T listener) {
		if (listener == null)
			return;
		if (this.listenerStrongList == null)
			this.listenerStrongList = new Object[] { listener };
		else {
			int i = this.listenerStrongList.length;
			Object[] tmp = new Object[i + 1];
			System.arraycopy(this.listenerStrongList, 0, tmp, 0, i);
			tmp[i] = listener;
			this.listenerStrongList = tmp;
		}
	}

	public synchronized void addStrongRefIfAbsent(T listener) {
		Object[] listenerStrongList = this.listenerStrongList;
		if (listenerStrongList != null)
			for (Object l : listenerStrongList)
				if (l.equals(listener))
					return;
		addStrongRef(listener);
	}

	public synchronized void removeStrongRef(T listener) {
		if (listener == null)
			return;
		for (int i = 0; i < this.listenerStrongList.length; i++)
			if (this.listenerStrongList[i] == listener) {
				if (this.listenerStrongList.length == 1)
					this.listenerStrongList = null;
				else {
					Object[] tmp = new Object[this.listenerStrongList.length - 1];
					System.arraycopy(this.listenerStrongList, 0, tmp, 0, i);
					if (i < tmp.length)
						System.arraycopy(this.listenerStrongList, i + 1, tmp, i, tmp.length - i);
					this.listenerStrongList = tmp.length == 0 ? null : tmp;
				}
				return;
			}
	}

	public synchronized void addWeakRef(T listener) {
		if (listener == null)
			return;
		if (this.listenerWeakList == null) {
			ReferenceQueue<T> queue = new ReferenceQueue<>();
			this.listenerWeakList = new ImmutableWeakList<>(new WeakReference[] { new WeakReference<>(listener, queue) }, queue);
		} else {
			WeakReference<?>[] oldListenerList = this.listenerWeakList.listenerList;
			WeakReference<?>[] temp = new WeakReference<?>[oldListenerList.length + 1];
			System.arraycopy(oldListenerList, 0, temp, 0, oldListenerList.length);
			temp[oldListenerList.length] = new WeakReference<>(listener, this.listenerWeakList.queue);
			this.listenerWeakList = new ImmutableWeakList<>(temp, this.listenerWeakList.queue);
		}
	}

	public synchronized void addWeakRefIfAbsent(T listener) {
		if (this.listenerWeakList == null || !this.listenerWeakList.contains(listener))
			addWeakRef(listener);
	}

	public synchronized void removeWeakRef(T listener) {
		if (listener == null)
			return;
		WeakReference<?>[] oldListenerList = this.listenerWeakList.listenerList;
		for (int i = 0; i < oldListenerList.length; i++)
			if (oldListenerList[i].get() == listener) {
				if (this.listenerWeakList.listenerList.length == 1)
					this.listenerWeakList = null;
				else {
					WeakReference<?>[] tmp = new WeakReference<?>[oldListenerList.length - 1];
					System.arraycopy(oldListenerList, 0, tmp, 0, i);
					if (i < tmp.length)
						System.arraycopy(oldListenerList, i + 1, tmp, i, tmp.length - i);
					this.listenerWeakList = new ImmutableWeakList<>(tmp, this.listenerWeakList.queue);
				}
				return;
			}
	}

	public synchronized void cleanWeakList() {
		if (this.listenerWeakList != null && this.listenerWeakList.listenerList.length == 0)
			this.listenerWeakList = null;
	}

	@SuppressWarnings("unchecked")
	public void forEach(Consumer<T> runnable) {
		Object[] listenerStrongList = this.listenerStrongList;
		ImmutableWeakList<T> listenerWeakList = this.listenerWeakList;
		if (listenerStrongList != null)
			for (Object listener : listenerStrongList)
				runnable.accept((T) listener);
		if (listenerWeakList != null)
			listenerWeakList.forEach(runnable);
	}

	public int getListenerCount() {
		int size = 0;
		ImmutableWeakList<T> listenerWeakList = this.listenerWeakList;
		Object[] listenerStrongList = this.listenerStrongList;
		if (listenerWeakList != null) {
			System.gc(); // Force to clear weak reference
			listenerWeakList.expungeStaleEntries(); // Force to iterate and remove empty ref
			size += listenerWeakList.size();
		}
		if (listenerStrongList != null)
			size += listenerStrongList.length;
		return size;
	}

	class ImmutableWeakList<U> {
		private final ReferenceQueue<U> queue;
		private WeakReference<?>[] listenerList;

		public ImmutableWeakList(WeakReference<?>[] listenerList, ReferenceQueue<U> queue) {
			this.listenerList = listenerList;
			this.queue = queue;
		}

		public int size() {
			return this.listenerList.length;
		}

		public boolean contains(T o) {
			for (WeakReference<?> weakReference : this.listenerList) {
				Object val = weakReference.get();
				if (val != null && val.equals(o))
					return true;
			}
			return false;
		}

		public void forEach(Consumer<? super T> action) {
			for (WeakReference<?> weakReference : this.listenerList) {
				@SuppressWarnings("unchecked")
				T val = (T) weakReference.get();
				if (val != null)
					action.accept(val);
			}
		}

		public void expungeStaleEntries() {
			Reference<? extends U> x = this.queue.poll();
			if (x != null)
				synchronized (this.queue) {
					do
						for (int i = 0; i < this.listenerList.length; i++)
							if (this.listenerList[i] == x) {
								WeakReference<?>[] tmp = new WeakReference<?>[this.listenerList.length - 1];
								System.arraycopy(this.listenerList, 0, tmp, 0, i);
								if (i < tmp.length)
									System.arraycopy(this.listenerList, i + 1, tmp, i, tmp.length - i);
								this.listenerList = tmp;
								break;
							}
					while ((x = this.queue.poll()) != null);
				}
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			forEach(val -> sb.append(val + ", "));
			return sb.length() == 0 ? sb.toString() : sb.substring(0, sb.length() - 2);
		}
	}

	@Override
	public String toString() {
		return "EventListenerWeakList: StrongRef: " + (this.listenerStrongList == null ? "[]" : Arrays.toString(this.listenerStrongList)) + ", WeakRef: "
				+ (this.listenerWeakList == null ? "[]" : "[" + this.listenerWeakList + "]");
	}
}
