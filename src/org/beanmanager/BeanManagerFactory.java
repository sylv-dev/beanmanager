/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager;

import org.beanmanager.rmi.RMIBeanManager;
import org.beanmanager.rmi.client.RemoteBean;

public class BeanManagerFactory {
	public static boolean rmiMode = true;

	private BeanManagerFactory() {}

	public static BeanManager create(Object bean, String local) {
		return rmiMode ? createRMI(bean, local) : new BeanManager(bean, local);
	}

	private static BeanManager createRMI(Object bean, String local) {
		return bean instanceof RemoteBean ? new RMIBeanManager((RemoteBean) bean, local) : new BeanManager(bean, local);
	}
}
