/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.rmi.client;

import java.beans.IntrospectionException;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;

import org.beanmanager.rmi.server.RMIBeanCallBack;
import org.beanmanager.rmi.server.RMIBeanImpl;
import org.beanmanager.rmi.server.RMIBeanServer;
import org.beanmanager.rmi.server.RMIBeanServerImpl;

@FunctionalInterface
interface CheckedFunction<T, R> {
	R apply(T t) throws IOException;
}

// Gérer InetAddress.getLocalHost().getHostName(), remplacer par l'ip
public class RemoteBean implements RemoteBeanImpl {
	private static final ConcurrentHashMap<Integer, RMIBeanServerHandler> REMOTE_SERVER = new ConcurrentHashMap<>();
	private final String identifier;
	private String hostName;
	private final int port;
	private final RMIBeanServerImpl rmiBeanServer;
	private final RMIBeanServerHandler processHandler;
	protected RMIBeanImpl remoteBean;
	private boolean hasProcessExit;

	public RemoteBean(Class<? extends Object> type, String id, String hostName, int port, boolean createRemoteOperator, RMIBeanCallBack callBack, boolean eraseCallBackIfPresent) throws Exception {
		this(type, id, hostName, port, createRemoteOperator, callBack, eraseCallBackIfPresent, -1);
	}

	public RemoteBean(Class<? extends Object> type, String identifier, String hostName, int port, boolean createRemoteOperator, RMIBeanCallBack callBack, boolean eraseCallBackIfPresent, int timeOut)
			throws Exception {
		this.identifier = identifier;
		this.hostName = hostName;
		this.port = port;
		if (createRemoteOperator) {
			var processCreationException = new Object() {
				public Exception e = null;
			};
			RMIBeanServerHandler processHandler = REMOTE_SERVER.computeIfAbsent(port, a -> {
				try {
					Process process = new ProcessBuilder().command(System.getProperty("java.home") + File.separator + "bin" + File.separator + "java", "-cp",
							System.getProperty("java.class.path") + System.getProperty("path.separator") + System.getProperty("jdk.module.path"), getServer().getCanonicalName(),
							Integer.toString(port)/* , _class.getCanonicalName() */, Boolean.toString(Boolean.valueOf(true)), Boolean.toString(Boolean.valueOf(false))).inheritIO().start();
					return new RMIBeanServerHandler(process, connectToRemoteServer(hostName, port, timeOut));
				} catch (IOException | NotBoundException | InterruptedException e) {
					processCreationException.e = e;
					return null;
				}
			});
			if (processCreationException.e != null)
				throw processCreationException.e;
			processHandler.beanCount.incrementAndGet();
			processHandler.process.onExit().thenApply((p) -> this.hasProcessExit = true);
			this.processHandler = processHandler;
			this.rmiBeanServer = processHandler.rmiBeanServer;
		} else {
			this.processHandler = null;
			this.rmiBeanServer = connectToRemoteServer(hostName, port, timeOut);
		}
		if (!this.rmiBeanServer.createRemoteBean(type, identifier, callBack, eraseCallBackIfPresent))// , callBack
			throw new Exception("Remote bean creation fail");

		long time = System.currentTimeMillis();
		while (!this.hasProcessExit) {
			try {
				this.remoteBean = (RMIBeanImpl) Naming.lookup("rmi://" + hostName + ":" + port + "/" + port + "_" + identifier);
				break;
			} catch (NotBoundException | MalformedURLException | RemoteException e) {
				if (timeOut > 0 && System.currentTimeMillis() - time > timeOut)
					return;
				if (e instanceof InterruptedException)
					break;
			}
			if (Thread.currentThread().isInterrupted())
				throw new InterruptedException();
		}
	}

	public RemoteBean(Class<?> type, String id, String hostName, int port, boolean createRemoteOperator) throws Exception {
		this(type, id, hostName, port, createRemoteOperator, new RMIBeanCallBack(), false, -1);
	}

	public RemoteBean(Class<?> type, String id, String hostName, int port, boolean createRemoteOperator, int timeout) throws Exception {
		this(type, id, hostName, port, createRemoteOperator, new RMIBeanCallBack(), false, timeout);
	}

	@Override
	public void addBeanAndSubBeanPropertyChangeListener(RMIBeanAndSubBeanPropertyChangeListenerImpl listener) throws RemoteException {
		if (this.remoteBean != null)
			try {
				this.remoteBean.addBeanAndSubBeanPropertyChangeListener(listener);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
	}

	@Override
	public void addPropertyChangeListener(RMIPropertyChangeListenerImpl listener) throws RemoteException {
		if (this.remoteBean != null)
			try {
				this.remoteBean.addPropertyChangeListener(listener);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
	}

	public boolean connectionError() {
		return this.hasProcessExit;
	}

	private RMIBeanServerImpl connectToRemoteServer(String hostName, int port, int timeOut) throws NotBoundException, MalformedURLException, RemoteException, InterruptedException {
		long time = System.currentTimeMillis();
		while (!this.hasProcessExit) {
			try {
				return (RMIBeanServerImpl) Naming.lookup("rmi://" + hostName + ":" + port + "/" + port);
			} catch (NotBoundException | MalformedURLException | RemoteException e) {
				if (e instanceof InterruptedException)
					break;
				if (timeOut > 0 && System.currentTimeMillis() - time > timeOut)
					throw e;
			}
			if (Thread.currentThread().isInterrupted())
				throw new InterruptedException();
		}
		return null;
	}

	public void destroy() {
		// System.err.println("start destruction");
		if (this.processHandler != null && this.processHandler.beanCount.decrementAndGet() == 0)
			REMOTE_SERVER.compute(this.port, (port, processHandler) -> {
				try {
					processHandler.rmiBeanServer.destroy();
				} catch (RemoteException ex) {
					ex.printStackTrace();
				}
				Process process = processHandler.process;
				process.destroy();
				try {
					process.onExit().get();
				} catch (InterruptedException | ExecutionException e) {
					process.destroyForcibly();
					try {
						process.onExit().get();
					} catch (InterruptedException | ExecutionException ex) {
						ex.printStackTrace();
					}
					e.printStackTrace();
				}
				process = null;
				return null;
			});
		else
			try {
				this.rmiBeanServer.destroyRemoteBean(this.identifier);
				// TODO
				// java.rmi.UnmarshalException: Error unmarshaling return header; nested exception is:
				// java.io.EOFException
				// at java.rmi/sun.rmi.transport.StreamRemoteCall.executeCall(StreamRemoteCall.java:236)
				// at java.rmi/sun.rmi.server.UnicastRef.invoke(UnicastRef.java:161)
				// at java.rmi/java.rmi.server.RemoteObjectInvocationHandler.invokeRemoteMethod(RemoteObjectInvocationHandler.java:209)
				// at java.rmi/java.rmi.server.RemoteObjectInvocationHandler.invoke(RemoteObjectInvocationHandler.java:161)
				// at com.sun.proxy.$Proxy17.destroyRemoteBean(Unknown Source)
				// at BeanManager/rmi.client.RemoteBean.destroy(RemoteBean.java:212)
				// at ScenariumFx/fileManager.scenario.dataFlowDiagram.DiagramScheduler.destroyOperator(DiagramScheduler.java:700)
				// at ScenariumFx/fileManager.scenario.dataFlowDiagram.MultiCoreDiagramScheduler$ThreadedBlock.run(MultiCoreDiagramScheduler.java:461)
				// Caused by: java.io.EOFException
				// at java.base/java.io.DataInputStream.readByte(DataInputStream.java:272)
				// at java.rmi/sun.rmi.transport.StreamRemoteCall.executeCall(StreamRemoteCall.java:222)
				// ... 7 more

			} catch (RemoteException e) {
				e.printStackTrace();
			}
		// System.err.println("end of destruction");
		this.remoteBean = null;
	}

	public InetSocketAddress getAddress() {
		return new InetSocketAddress(this.hostName, this.port);
	}

	public Class<?> getBeanClass() {
		try {
			return this.remoteBean == null ? null : this.remoteBean.getBeanClass();
		} catch (RemoteException e) {
			e.printStackTrace();
			return null;
		}
	}

	protected Class<?> getServer() {
		return RMIBeanServer.class;
	}

	public Object getSubBeanValue(Class<?> beanType, String beanName, String propertyName) {
		try {
			return this.remoteBean == null ? null : this.remoteBean.getSubBeanValue(beanType, beanName, propertyName);
		} catch (RemoteException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | IntrospectionException e) {
			e.printStackTrace();
			return null;
		}
	}

	public Object getValue(String propertyName) {
		try {
			return this.remoteBean == null ? null : this.remoteBean.getValue(propertyName);
		} catch (RemoteException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | IntrospectionException e) {
			e.printStackTrace();
			return null;
		}
	}

	public Object[] invokeMethod(String methodname, Object... args) {
		try {
			return this.remoteBean == null ? null : this.remoteBean.invokeMethod(methodname, args);
		} catch (RemoteException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
			return null;
		}
	}

	public boolean isConnected() {
		return this.remoteBean != null;
	}

	public boolean isDynamicAnnotation() {
		try {
			return this.remoteBean == null ? null : this.remoteBean.isDynamicAnnotation();
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean isDynamicEnableBean() {
		try {
			return this.remoteBean == null ? null : this.remoteBean.isDynamicEnableBean();
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean isDynamicVisibleBean() {
		try {
			return this.remoteBean == null ? null : this.remoteBean.isDynamicVisibleBean();
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean isUpdatableViewBean() {
		try {
			return this.remoteBean == null ? null : this.remoteBean.isUpdatableViewBean();
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public void removeBeanAndSubBeanPropertyChangeListener(RMIBeanAndSubBeanPropertyChangeListenerImpl listener) throws RemoteException {
		if (this.remoteBean != null)
			try {
				this.remoteBean.removeBeanAndSubBeanPropertyChangeListener(listener);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
	}

	@Override
	public void removePropertyChangeListener(RMIPropertyChangeListenerImpl listener) throws RemoteException {
		if (this.remoteBean != null)
			try {
				this.remoteBean.removePropertyChangeListener(listener);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
	}

	public void setEnable() {
		if (this.remoteBean != null)
			try {
				this.remoteBean.setEnable();
			} catch (RemoteException e) {
				e.printStackTrace();
			}
	}

	public void setSubBeanValue(Class<?> beanType, String beanName, String propertyName, boolean asText, Object value) {
		if (this.remoteBean != null)
			try {
				this.remoteBean.setSubBeanValue(beanType, beanName, propertyName, asText, value);
			} catch (RemoteException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | IntrospectionException e) {
				e.printStackTrace();
			}
	}

	public void setValue(String propertyName, boolean asText, Object value) {
		if (this.remoteBean != null)
			try {
				this.remoteBean.setValue(propertyName, asText, value);
			} catch (RemoteException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | IntrospectionException e) {
				e.printStackTrace();
			}
	}

	@Override
	public String toString() {
		try {
			return this.remoteBean == null ? null : this.remoteBean.remoteToString();
		} catch (RemoteException e) {
			e.printStackTrace();
			return null;
		}
	}
}

class RMIBeanServerHandler {
	public final Process process;
	public final RMIBeanServerImpl rmiBeanServer;
	public AtomicInteger beanCount;

	public RMIBeanServerHandler(Process process, RMIBeanServerImpl rmiBeanServer) {
		this.process = process;
		this.rmiBeanServer = rmiBeanServer;
		this.beanCount = new AtomicInteger(0);
	}
}
