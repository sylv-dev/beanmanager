/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.rmi.client;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import org.beanmanager.tools.TriConsumer;

public class RMIBeanAndSubBeanPropertyChangeListener extends UnicastRemoteObject implements RMIBeanAndSubBeanPropertyChangeListenerImpl {
	private static final long serialVersionUID = 1L;
	private final TriConsumer<Class<?>, String, String> beanPropertyChangeListener;

	public RMIBeanAndSubBeanPropertyChangeListener(TriConsumer<Class<?>, String, String> beanPropertyChangeListener) throws RemoteException {
		super();
		this.beanPropertyChangeListener = beanPropertyChangeListener;
	}

	@Override
	public void rMIBeanAndSubBeanPropertyChanged(Class<?> beanType, String beanName, String propertyName) throws RemoteException {
		this.beanPropertyChangeListener.accept(beanType, beanName, propertyName);
	}

}
