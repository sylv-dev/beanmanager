/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.rmi.client;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RMIBeanAndSubBeanPropertyChangeListenerImpl extends Remote {
	public void rMIBeanAndSubBeanPropertyChanged(Class<?> beanType, String beanName, String propertyName) throws RemoteException;

}
