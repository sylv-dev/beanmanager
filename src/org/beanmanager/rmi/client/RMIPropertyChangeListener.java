/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.rmi.client;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.function.Consumer;

public class RMIPropertyChangeListener extends UnicastRemoteObject implements RMIPropertyChangeListenerImpl {
	private static final long serialVersionUID = 1L;
	private final Consumer<String> propertyChangeListener;

	public RMIPropertyChangeListener(Consumer<String> propertyChangeListener) throws RemoteException {
		super();
		this.propertyChangeListener = propertyChangeListener;
	}

	@Override
	public void rmiPropertyChanged(String name) throws RemoteException {
		this.propertyChangeListener.accept(name);
	}
}
