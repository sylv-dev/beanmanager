/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.rmi;

import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.rmi.RemoteException;
import java.util.HashMap;

import org.beanmanager.BeanAndSubBeanPropertyChangeListener;
import org.beanmanager.BeanManager;
import org.beanmanager.editors.PropertyEditor;
import org.beanmanager.editors.container.BeanEditor;
import org.beanmanager.ihmtest.FxTest;
import org.beanmanager.rmi.client.RMIBeanAndSubBeanPropertyChangeListener;
import org.beanmanager.rmi.client.RemoteBean;
import org.beanmanager.testbean.TypeTest;

public class RMIBeanManager extends BeanManager {
	public static void main(String[] args) {
		TypeTest tt = new TypeTest();
		TypeTest r = new TypeTest();
		// tt.setAbeille((short) 9);
		new BeanManager(tt, "").copyTo(new BeanManager(r, ""));
		BeanManager.switchDefaultBeanLocation("");
		RemoteBean rb;
		try {
			rb = new RemoteBean(TypeTest.class, InetAddress.getLocalHost().getHostName(), "0", 1099, true);
			RMIBeanManager beanGui = new RMIBeanManager(rb, "patate");
			BeanEditor.registerBean(rb, defaultDir);
			beanGui.load();
			new FxTest().launchIHM(args, s -> beanGui.getEditor(), e -> {
				beanGui.saveIfChanged();
				rb.destroy();
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private HashMap<PropertyDescriptor, PropertyEditor<?>> propertyEditorsMap;

	private HashMap<BeanAndSubBeanPropertyChangeListener, RMIBeanAndSubBeanPropertyChangeListener> beanAndSubBeanPropertyChangeListenerMap;

	public RMIBeanManager(RemoteBean bean, String local) {
		super(bean, local);
	}

	@Override
	public void addBeanAndSubBeanPropertyChangeListener(BeanAndSubBeanPropertyChangeListener listener) {
		try {
			RMIBeanAndSubBeanPropertyChangeListener rMIBeanAndSubBeanPropertyChangeListener = new RMIBeanAndSubBeanPropertyChangeListener(listener::propertyChange);
			if (this.beanAndSubBeanPropertyChangeListenerMap == null)
				this.beanAndSubBeanPropertyChangeListenerMap = new HashMap<>();
			this.beanAndSubBeanPropertyChangeListenerMap.put(listener, rMIBeanAndSubBeanPropertyChangeListener);
			((RemoteBean) this.bean).addBeanAndSubBeanPropertyChangeListener(rMIBeanAndSubBeanPropertyChangeListener);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected Class<?> getBeanClass() {
		return ((RemoteBean) this.bean).getBeanClass();
	}

	@Override
	protected InetSocketAddress getLocale() {
		return ((RemoteBean) this.bean).getAddress();
	}

	private PropertyEditor<?> getPropertyEditor(Object bean, PropertyDescriptor pd) {
		if (this.propertyEditorsMap == null)
			this.propertyEditorsMap = new HashMap<>();
		PropertyEditor<?> editor = this.propertyEditorsMap.get(pd);
		if (editor == null) {
			editor = BeanManager.getPropertyEditor(pd, pd.getPropertyType(), bean, "");
			this.propertyEditorsMap.put(pd, editor);
		}
		return editor;
	}

	public Object getSubBeanValue(Class<?> beanType, String beanName, String propertyName) {
		Object value = ((RemoteBean) this.bean).getSubBeanValue(beanType, beanName, propertyName);
		PropertyDescriptor pd = new BeanManager(BeanEditor.getRegisterBean(beanType, beanName).bean, "").getPropertyDescriptors(propertyName);
		Class<?> type = pd.getPropertyType();
		if (!type.isPrimitive() && !Serializable.class.isAssignableFrom(type)) {
			PropertyEditor<?> editor = getPropertyEditor(this.bean, pd);
			editor.setAsText((String) value);
			value = editor.getValue();
		}
		return value;
	}

	@Override
	protected Object getValue(PropertyDescriptor pd, Object bean) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Object value = ((RemoteBean) bean).getValue(pd.getName());
		Class<?> type = pd.getPropertyType();
		if (!type.isPrimitive() && !Serializable.class.isAssignableFrom(type)) {
			PropertyEditor<?> editor = getPropertyEditor(bean, pd);
			editor.setAsText((String) value);
			value = editor.getValue();
		}
		return value;
	}

	@Override
	protected Object invokeMethod(Class<?> beanClass, Object bean, String methodname)
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		return ((RemoteBean) bean).invokeMethod(methodname);
	}

	@Override
	protected boolean isDynamicAnnotation(Object bean) {
		return ((RemoteBean) bean).isDynamicAnnotation();
	}

	@Override
	protected boolean isDynamicEnableBean(Object bean) {
		return ((RemoteBean) bean).isDynamicEnableBean();
	}

	@Override
	protected boolean isDynamicVisibleBean(Object bean) {
		return ((RemoteBean) bean).isDynamicVisibleBean();
	}

	@Override
	protected boolean isUpdatableViewBean(Object bean) {
		return ((RemoteBean) bean).isUpdatableViewBean();
	}

	@Override
	public void removeBeanAndSubBeanPropertyChangeListener(BeanAndSubBeanPropertyChangeListener listener) {
		try {
			if (this.beanAndSubBeanPropertyChangeListenerMap == null)
				return;
			RMIBeanAndSubBeanPropertyChangeListener rMIBeanAndSubBeanPropertyChangeListener = this.beanAndSubBeanPropertyChangeListenerMap.get(listener);
			((RemoteBean) this.bean).removeBeanAndSubBeanPropertyChangeListener(rMIBeanAndSubBeanPropertyChangeListener);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void setEnable(Object bean) {
		((RemoteBean) bean).setEnable();
	}

	public void setSubBeanValue(Class<?> beanType, String beanName, String propertyName, Object value) {
		if (value == null || value instanceof Serializable)
			((RemoteBean) this.bean).setSubBeanValue(beanType, beanName, propertyName, false, value);
		else {
			PropertyDescriptor pd = new BeanManager(BeanEditor.getRegisterBean(beanType, beanName).bean, "").getPropertyDescriptors(propertyName);
			if (pd != null) {
				PropertyEditor<?> editor = getPropertyEditor(this.bean, pd);
				editor.setValueFromObj(value);
				((RemoteBean) this.bean).setSubBeanValue(beanType, beanName, propertyName, true, editor.getAsText());
			}
		}
	}

	@Override
	protected void setValue(PropertyDescriptor pd, Object bean, Object value) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		if (value == null || value instanceof Serializable)
			((RemoteBean) bean).setValue(pd.getName(), false, value);
		else {
			PropertyEditor<?> editor = getPropertyEditor(bean, pd);
			editor.setValueFromObj(value);
			((RemoteBean) bean).setValue(pd.getName(), true, editor.getAsText());
		}
	}
}
