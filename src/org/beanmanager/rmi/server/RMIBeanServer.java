/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.rmi.server;

import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.HashSet;

import org.beanmanager.BeanManager;
import org.beanmanager.editors.container.BeanEditor;

import javafx.application.Platform;

class RemoteBeanHandle {
	public final String name;
	public final RMIBean remoteBean;
	public int cpt;

	public RemoteBeanHandle(String name, RMIBean remoteBean) {
		this.name = name;
		this.remoteBean = remoteBean;
		this.cpt = 1;
	}
}

public class RMIBeanServer extends UnicastRemoteObject implements RMIBeanServerImpl {
	private static final long serialVersionUID = 1L;
	private static final HashMap<String, RemoteBeanHandle> REMOTE_BEANS = new HashMap<>();
	private static boolean verbose = false;
	private static int port = 0;

	public static void main(String[] args) throws RemoteException {
		new RMIBeanServer(args);
	}

	protected RMIBeanServer() throws RemoteException {
		super();
	}

	public RMIBeanServer(String[] args) throws RemoteException {
		if (args.length != 3) {
			System.err.println(
					"RMIBeanServer must be launch with three arguments:\n-port: the port for the remote bean\n-terminateIfParentTerminate: tell if the RMIBeanServer must terminate if the father process terminate\n-verbose: Explicitly displays in the console all operations performed");
			System.exit(-1);
		}
		try {
			port = Integer.parseInt(args[0]);
			if (port < 0 || port > Short.MAX_VALUE * 2 + 1) {
				System.err.println("The port property wmust be in the interval [0, " + Short.MAX_VALUE * 2 + 1 + "]");
				System.exit(-2);
			}
		} catch (NumberFormatException e) {
			System.err.println("The port property is not formated as an integer");
			System.exit(-3);
		}
		try {
			if (Boolean.parseBoolean(args[1]))
				ProcessHandle.current().parent().ifPresent(a -> a.onExit().thenRun(() -> System.exit(1)));
		} catch (Exception e) {
			System.err.println("The port terminateIfParentTerminate is not formated as an boolean");
			System.exit(-5);
		}
		verbose = Boolean.parseBoolean(args[2]);
		if (verbose)
			System.out.println("Start of RMIBeanServer on port: " + port);
		BeanEditor.needToLoadBrotherBean = false;
		try {
			System.setProperty("java.rmi.server.hostname", InetAddress.getLocalHost().getHostName());
			LocateRegistry.createRegistry(port);
			Naming.rebind("rmi://" + java.net.InetAddress.getLocalHost().getHostName() + ":" + port + "/" + port, getRMIServer());
			if (verbose)
				System.out.println("RMIBeanServer ready on port: " + port);
		} catch (RemoteException | UnknownHostException | MalformedURLException | IllegalArgumentException | SecurityException e) {
			System.err.println("Server creation failed: " + e);
			// e.printStackTrace();
		}
	}

	@Override
	public synchronized boolean createRemoteBean(Class<? extends Object> type, String identifier, RMIBeanCallBackImpl callBack, boolean eraseCallBackIfPresent) throws RemoteException {
		try {
			String name = "rmi://" + InetAddress.getLocalHost().getHostName() + ":" + port + "/" + port + "_" + identifier;
			RemoteBeanHandle remoteBeanHandle = REMOTE_BEANS.get(identifier);
			// System.err.println("createRemoteBean: " + name);
			if (remoteBeanHandle != null) {
				// System.err.println("the bean already exist");
				remoteBeanHandle.cpt++;
				if (eraseCallBackIfPresent)
					// System.out.println("change bean callback");
					remoteBeanHandle.remoteBean.setBeanCallBack(callBack);
				// System.out.println("change bean callback ok");
			} else {
				// System.err.println("the bean does not exist, create new remote bean");
				RMIBean remoteBean = getRMIObject(type, identifier, callBack);
				REMOTE_BEANS.put(identifier, new RemoteBeanHandle(name, remoteBean));
				Naming.rebind(name, remoteBean);
				if (verbose)
					System.out.println("RemoteBean: " + identifier + " of type: " + type.getCanonicalName() + " created");
			}
			return true;
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException | MalformedURLException
				| UnknownHostException e) {
			e.printStackTrace();
			System.err.println("RemoteBean creation failed: " + e);
			return false;
		}
	}

	@Override
	public void destroy() throws RemoteException {
		Platform.exit();
	}

	@Override
	public synchronized boolean destroyRemoteBean(String identifier) throws RemoteException {
		RemoteBeanHandle remoteBeanHandle = REMOTE_BEANS.get(identifier);
		if (remoteBeanHandle != null && --remoteBeanHandle.cpt == 0) {
			var success = new Object() {
				boolean sucess;
			};
			try {
				Naming.unbind(remoteBeanHandle.name);
			} catch (RemoteException | MalformedURLException | NotBoundException e) {
				System.err.println("RemoteBean destruction failed: " + e);
				e.printStackTrace();
				success.sucess = false;
			}
			REMOTE_BEANS.remove(identifier);
			if (verbose)
				System.out.println("RemoteBean: " + remoteBeanHandle.name.substring(remoteBeanHandle.name.lastIndexOf("_") + 1) + " destructed");
			subBeanGarbageCollector(remoteBeanHandle.remoteBean.bean);
			return success.sucess;
		}
		return true;
	}

	protected RMIBean getRMIObject(Class<?> type, String identifier, RMIBeanCallBackImpl callBack)
			throws RemoteException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		return new RMIBean(type, identifier, callBack);
	}

	protected RMIBeanServer getRMIServer() throws RemoteException {
		return new RMIBeanServer();
	}

	private void populateSubBeans(Object fatherBean, HashSet<Object> subBeans) {
		for (Object subBean : BeanManager.getSubBeans(fatherBean, ""))
			if (subBeans.add(subBean))
				populateSubBeans(subBean, subBeans);
	}

	private void subBeanGarbageCollector(Object fatherBean) {
		HashSet<Object> subBeansCandidateToRemote = new HashSet<>();
		subBeansCandidateToRemote.add(fatherBean);
		populateSubBeans(fatherBean, subBeansCandidateToRemote);
		for (RemoteBeanHandle rbh : REMOTE_BEANS.values()) {
			HashSet<Object> otherSubBeans = new HashSet<>();
			otherSubBeans.add(rbh.remoteBean.bean);
			populateSubBeans(rbh.remoteBean.bean, otherSubBeans);
			subBeansCandidateToRemote.removeAll(otherSubBeans);
		}
		subBeansCandidateToRemote.forEach(sbtg -> BeanEditor.unregisterBean(sbtg, false));
	}
}