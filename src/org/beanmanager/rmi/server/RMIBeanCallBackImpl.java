/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.rmi.server;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RMIBeanCallBackImpl extends Remote {
	byte[] getBean(Class<?> beanType, String beanName) throws RemoteException;
}
