/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.rmi;

import java.net.InetAddress;

import org.beanmanager.rmi.client.RemoteBean;

public class BeanTest {
	public static void main(String[] args) {
		try {
			RemoteBean remoteBean = new RemoteBean(BeanTest.class, null, InetAddress.getLocalHost().getHostName(), 1099, true);
			remoteBean.setValue("patate", false, 5);
			System.out.println(remoteBean.getClass() + " " + remoteBean.toString() + remoteBean.getValue("patate"));
			remoteBean.destroy();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	int patate = 5;

	public int getPatate() {
		return this.patate;
	}

	public void setPatate(int patate) {
		this.patate = patate + 2;
	}

	@Override
	public String toString() {
		return "Je suis beanTest";
	}
}
