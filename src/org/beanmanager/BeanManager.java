/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager;

import java.beans.EventSetDescriptor;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyChangeListener;
import java.beans.PropertyDescriptor;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;
import java.nio.file.NoSuchFileException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutionException;
import java.util.function.BiFunction;
import java.util.function.BooleanSupplier;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.swing.event.EventListenerList;

import org.beanmanager.editors.ChoiceEditor;
import org.beanmanager.editors.DynamicAnnotationBean;
import org.beanmanager.editors.DynamicChoiceBox;
import org.beanmanager.editors.DynamicEnableBean;
import org.beanmanager.editors.DynamicPossibilities;
import org.beanmanager.editors.DynamicSizeEditor;
import org.beanmanager.editors.DynamicSizeEditorChangeListener;
import org.beanmanager.editors.DynamicVisibleBean;
import org.beanmanager.editors.LocalisedEditor;
import org.beanmanager.editors.PropertyEditor;
import org.beanmanager.editors.PropertyEditorManager;
import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.SubBeanChangeListener;
import org.beanmanager.editors.TransientProperty;
import org.beanmanager.editors.UpdatableViewBean;
import org.beanmanager.editors.container.BeanEditor;
import org.beanmanager.editors.container.BeanInfo;
import org.beanmanager.editors.container.DynamicBeanInfo;
import org.beanmanager.editors.container.PropertyContainerEditor;
import org.beanmanager.editors.multiNumber.MultiNumberEditor;
import org.beanmanager.editors.multiNumber.MultiNumberElementNames;
import org.beanmanager.ihmtest.FxTest;
import org.beanmanager.testbean.TypeTest;
import org.beanmanager.tools.FxUtils;
import org.beanmanager.tools.Logger;
import org.beanmanager.tools.TriConsumer;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.RowConstraints;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.StrokeType;
import javafx.util.Duration;

public class BeanManager implements BeanListChangeListener, DynamicSizeEditorChangeListener {
	public static final List<Class<?>> PRIMITIVES = List.of(boolean.class, byte.class, double.class, float.class, int.class, long.class, short.class);
	public static final List<Class<?>> WRAPPERS = List.of(Boolean.class, Byte.class, Double.class, Float.class, Integer.class, Long.class, Short.class);
	private static final org.beanmanager.tools.EventListenerList<LoadModuleListener> MODULE_LOADED_LISTENERS = new org.beanmanager.tools.EventListenerList<>();
	private static final HashMap<String, ClassLoader> REGISTER_MODULES = new HashMap<>();
	public static final Character MODULE_SEPARATOR = '!';
	private static final String PROPERTY_SUFFIX = ": ";
	public static final String SERIALIZE_EXT = ".txt";
	public static String defaultDir = "";
	public static boolean isHiddenFieldVisible = false;
	public static boolean isExpertFieldVisible = true;
	private static WeakHashMap<Class<?>, List<PropertyDescriptor>> propertyDescriptorsCache = new WeakHashMap<>();
	private static EventListenerList subBeanlisteners = new EventListenerList();
	public static BiFunction<Object, File, Boolean> loadBeanMethod;
	public static final String SIZE_TO_DATA_SEPARATOR = " ";
	private HashMap<String, PropertyDescriptor> propertyDescriptorMap;
	protected Object bean;
	private final Class<?> beanClass;
	private final EventListenerList listeners = new EventListenerList();
	public boolean isEdited = false;
	private HashMap<Label, ObjectViewDesc> mapLabelObject;
	protected String local;
	private boolean isUpdatingView = false;
	public boolean canGrow;
	private HashMap<Node, ChangeListener<Boolean>> disabledProperties;
	private HashMap<Node, ChangeListener<Boolean>> visibleProperties;
	private HashMap<BeanAndSubBeanPropertyChangeListener, HashMap<Object, BeanManagerListener>> beanAndSubBeanPropertyChangeListenerMap;

	public static void main(String[] args) {
		// IntreClass _bean = new IntreClass();
		TypeTest testBean = new TypeTest();
		final BeanManager beanGui = new BeanManager(testBean, defaultDir);
		beanGui.addPropertyChangeFromBeanManagerListener(e -> {
			System.out.println("ppchangeFromBM: " + e);
		});
		beanGui.addPropertyChangeListener(pcl -> {
			System.out.println("ppchange: " + pcl.getPropertyName());
			System.out.println("ppchange sub beans: " + BeanManager.getSubBeans(testBean, ""));
		});

		// méthode pour avoir la liste des sous bean
		BeanManager.getSubBeans(testBean, ""); // Il faut le faire en récursif pour prendre tous les sous beans avec liste de visite pour pas revisiter

		BeanEditor.registerBean(testBean, defaultDir);
		beanGui.load();
		new FxTest().launchIHM(args, s -> {
			GridPane editor = beanGui.getEditor();
			beanGui.addSizeChangeListener(() -> s.sizeToScene());
			ScrollPane sp = new ScrollPane(editor);
			sp.setFitToWidth(true);
			return sp;
		}, e -> beanGui.saveIfChanged(), false);
	}

	public static void purgeBean(Object beanToRemove, boolean delete, boolean purgeSubBean) {
		for (BeanDesc<?> parent : BeanEditor.getAllBeans()) {
			Object parentBean = parent.bean;
			boolean propertyRemoved = false;
			for (final PropertyDescriptor pd : getPropertyDescriptors(parentBean.getClass())) {
				PropertyEditor<?> editor = getPropertyEditor(pd, pd.getPropertyType(), parentBean, "");
				try {
					if (editor instanceof PropertyContainerEditor && !((PropertyContainerEditor<?>) editor).isInline()) {
						PropertyContainerEditor<?> containerEditor = (PropertyContainerEditor<?>) editor;
						editor.setValueFromObj(pd.getReadMethod().invoke(parentBean, new Object[0]));
						List<Object> beans = containerEditor.getSubBeans();
						if (beans == null)
							continue;
						for (Object bean : beans)
							if (bean == beanToRemove) {
								containerEditor.removeSubBeans(beanToRemove);
								propertyRemoved = true;
							}
						if (propertyRemoved)
							pd.getWriteMethod().invoke(parentBean, containerEditor.getValue());
					}
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
				}
			}
			if (propertyRemoved)
				new BeanManager(parent.bean, parent.local).save(false);
		}
		BeanEditor.unregisterBean(beanToRemove, delete);
		if (purgeSubBean)
			for (Object subBean : BeanManager.getSubBeans(beanToRemove, ""))
				if (!isLinkedBean(subBean))
					purgeBean(subBean, delete, purgeSubBean);
	}

	public static void removeSubBeanChangeListener(SubBeanChangeListener listener) {
		subBeanlisteners.remove(SubBeanChangeListener.class, listener);
	}

	public static Class<?> toPrimitive(Class<?> type) {
		for (int i = WRAPPERS.size(); i-- != 0;)
			if (type == WRAPPERS.get(i))
				return PRIMITIVES.get(i);
		return type;
	}

	public static Class<?> toWrapper(Class<?> type) {
		for (int i = PRIMITIVES.size(); i-- != 0;)
			if (type == PRIMITIVES.get(i))
				return WRAPPERS.get(i);
		return type;
	}

	public BeanManager(Object bean, String local) {
		if (bean == null)
			throw new IllegalArgumentException("bean cannot be null");
		if (local == null)
			throw new IllegalArgumentException("local cannot be null");
		this.bean = bean;
		this.beanClass = getBeanClass();
		this.local = local;
	}

	public void addBeanAndSubBeanPropertyChangeListener(BeanAndSubBeanPropertyChangeListener listener) {
		if (listener == null)
			return;
		synchronized (listener) {
			HashMap<Object, BeanManagerListener> map = new HashMap<>();
			if (this.beanAndSubBeanPropertyChangeListenerMap == null)
				this.beanAndSubBeanPropertyChangeListenerMap = new HashMap<>();
			this.beanAndSubBeanPropertyChangeListenerMap.put(listener, map);
			addBeanAndSubBeanPropertyChangeListener(this.bean, this.bean, map, listener, () -> !this.beanAndSubBeanPropertyChangeListenerMap.containsKey(listener));
		}
	}

	private PropertyChangeListener addBeanAndSubBeanPropertyChangeListener(Object fatherBean, Object bean, HashMap<Object, BeanManagerListener> map,
			BeanAndSubBeanPropertyChangeListener mainBeanListener, BooleanSupplier isCancelled) {
		java.beans.PropertyChangeListener subBeanListener = l -> {
			if (l.getOldValue() == l.getNewValue())
				return;
			synchronized (mainBeanListener) {
				if (isCancelled.getAsBoolean())
					return;
				Object oldValue = l.getOldValue();
				if (oldValue != null && map.get(oldValue) != null)
					subBeanGarbageCollector(fatherBean, map, mainBeanListener);
				Object subBean = l.getNewValue();
				BeanDesc<?> beanDesc = subBean == null ? null : BeanEditor.getBeanDesc(subBean);
				if (beanDesc != null)
					addToListener(fatherBean, subBean, map, mainBeanListener, isCancelled);
			}
			mainBeanListener.propertyChange(bean.getClass(), BeanEditor.getBeanDesc(bean).name, l.getPropertyName()); // Si je le met dans synchronized je peux avoir un dead lock
		};
		addPropertyChangeListener(subBeanListener);
		for (Object subBean : getSubBeansList(bean, this.local))
			addToListener(fatherBean, subBean, map, mainBeanListener, isCancelled);
		return subBeanListener;
	}

	public void addPropertyChangeFromBeanManagerListener(PropertyChangeFromBeanManagerListener listener) {
		this.listeners.add(PropertyChangeFromBeanManagerListener.class, listener);
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		if (listener == null)
			return;
		try {
			for (EventSetDescriptor esd : Introspector.getBeanInfo(this.beanClass).getEventSetDescriptors())
				if (esd.getName().equals("propertyChange"))
					try {
						esd.getAddListenerMethod().invoke(this.bean, listener);
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
						ex.printStackTrace();
					}
		} catch (IntrospectionException e) {}
	}

	public void addSizeChangeListener(DynamicSizeEditorChangeListener listener) {
		this.listeners.add(DynamicSizeEditorChangeListener.class, listener);
	}

	private void addToListener(Object fatherBean, Object subBean, HashMap<Object, BeanManagerListener> map, BeanAndSubBeanPropertyChangeListener mainBeanListener, BooleanSupplier isCancelled) {
		BeanManagerListener brc = map.get(subBean);
		if (brc == null) {
			BeanManager bm = new BeanManager(subBean, this.local);
			brc = new BeanManagerListener(bm);
			map.put(subBean, brc);
			PropertyChangeListener subBeanListener = bm.addBeanAndSubBeanPropertyChangeListener(fatherBean, subBean, map, mainBeanListener, isCancelled);
			brc.propertyChangeListener = subBeanListener;
		}
	}

	@Override
	public void beanListChanged() {
		fireSubBeanChange(BeanEditor.getBeanDesc(this.bean));
	}

	public void copyTo(BeanManager destinationBeanManager) {
		for (final PropertyDescriptor pd : getPropertyDescriptors())
			try {
				destinationBeanManager.setValue(pd, destinationBeanManager.getBean(), getValue(pd, this.bean));
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			}
	}

	public void copyTo(BeanManager destinationBeanManager, String propertyName) {
		PropertyDescriptor pd = getPropertyDescriptors(propertyName);
		if (pd != null)
			try {
				destinationBeanManager.setValue(pd, destinationBeanManager.getBean(), getValue(pd, this.bean));
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			}
	}

	private void firePropertyChangeFromBeanManager() {
		for (PropertyChangeFromBeanManagerListener listener : this.listeners.getListeners(PropertyChangeFromBeanManagerListener.class))
			listener.propertyChangeFromBeanManager(this.bean);
	}

	private void fireSizeChanged() {
		for (DynamicSizeEditorChangeListener listener : this.listeners.getListeners(DynamicSizeEditorChangeListener.class))
			listener.sizeChanged();
	}

	public void addSubBeanExpandedListener(SubBeanExpansionListener listener) {
		this.listeners.add(SubBeanExpansionListener.class, listener);
	}

	public void removeSubBeanExpandedListener(SubBeanExpansionListener listener) {
		this.listeners.remove(SubBeanExpansionListener.class, listener);
	}

	private void fireSubBeanExpanded(boolean expanded, String subBeanName, BeanManager subBeanManager, GridPane subBeanGridpane) {
		for (SubBeanExpansionListener listener : this.listeners.getListeners(SubBeanExpansionListener.class))
			listener.subBeanExpanded(expanded, subBeanName, subBeanManager, subBeanGridpane);
	}

	private ArrayList<String> getAsText(String parent) {
		ArrayList<String> lines = new ArrayList<>();
		for (final PropertyDescriptor pd : getPropertyDescriptors()) {
			PropertyEditor<?> editor = getPropertyEditor(pd, pd.getPropertyType(), this.bean, parent);
			if (editor == null) {
				System.err.println("no editor for: " + pd.getPropertyType());
				continue;
			}
			String stringValue = null;
			try {
				if (editor instanceof BeanEditor && ((BeanEditor<?>) editor).isInline()) {
					Object value = getValue(pd, this.bean);
					if (value != null) {
						stringValue = "{";
						ArrayList<String> subLines = new BeanManager(value, this.local).getAsText(parent);
						if (((BeanEditor<?>) editor).hasTypeChoice())
							stringValue += BeanManager.getDescriptorFromClass(value.getClass()) + PropertyEditor.PROPERTY_SEPARATOR;
						for (int i = 0; i < subLines.size() - 1; i++)
							stringValue += subLines.get(i) + PropertyEditor.PROPERTY_SEPARATOR;
						if (subLines.size() == 0)
							continue;
						stringValue += subLines.get(subLines.size() - 1) + "}";
					} else
						stringValue = "{}";
					if (editor.canContainForbiddenCharacter())
						stringValue = stringValue.length() + SIZE_TO_DATA_SEPARATOR + stringValue;
				} else {
					PropertyInfo fieldInfoAnno;
					if ((fieldInfoAnno = getAnno(PropertyInfo.class, this.bean.getClass(), pd)) != null && !fieldInfoAnno.savable())
						continue;
					editor.setValueFromObj(getValue(pd, this.bean));

					stringValue = editor.getAsText();
					if (editor.canContainForbiddenCharacter())
						stringValue = stringValue.length() + SIZE_TO_DATA_SEPARATOR + stringValue;
				}
				lines.add(pd.getName() + ":" + (stringValue == null ? /* String.valueOf((Object) null) */"" : stringValue));
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			}
		}
		return lines;
	}

	public Object getBean() {
		return this.bean;
	}

	protected Class<?> getBeanClass() {
		return this.bean.getClass();
	}

	public GridPane getEditor() {
		return getEditor((List<String>) null);
	}

	public GridPane getEditor(List<String> filteredProperties) {
		ArrayList<Thread> toLaunch = new ArrayList<>();
		GridPane editor = getEditor(toLaunch, filteredProperties);
		for (Thread thread : toLaunch)
			thread.start();
		return editor;
	}

	private GridPane getEditor(ArrayList<Thread> toLaunch, List<String> filteredProperties) {
		// if(propertiesPanel != null) //Pk??? je peux pas refresh sinon, ou sinon il faut une facon de le détruire
		// return propertiesPanel;
		GridPane propertiesPanel = new GridPane();
		ColumnConstraints column2 = new ColumnConstraints();
		column2.setHgrow(Priority.ALWAYS);
		ColumnConstraints column1 = new ColumnConstraints();
		propertiesPanel.getColumnConstraints().addAll(column1, column2);
		propertiesPanel.setAlignment(Pos.CENTER);

		ArrayList<EditableProperty> editablePropertiesList = new ArrayList<>();
		ArrayList<PropertyEditor<?>> waitingEditor = new ArrayList<>();
		ArrayList<String> waitingPossibleChoicesMethod = new ArrayList<>();
		List<PropertyDescriptor> propDesc = getPropertyDescriptors();
		if (filteredProperties != null)
			propDesc = propDesc.stream().filter(a -> !filteredProperties.contains(a.getName())).collect(Collectors.toList());
		for (final PropertyDescriptor pd : propDesc) {
			boolean isHidden = false;
			boolean isExpert = false;
			boolean readOnly = false;
			int index = -1;
			PropertyInfo fieldInfoAnno = getAnno(PropertyInfo.class, this.beanClass, pd);
			if (fieldInfoAnno != null) {
				if (!fieldInfoAnno.viewable())
					continue;
				isHidden = fieldInfoAnno.hidden();
				isExpert = fieldInfoAnno.expert();
				index = fieldInfoAnno.index();
				readOnly = fieldInfoAnno.readOnly();
			}
			if (isHidden && !isHiddenFieldVisible || isExpert && !isExpertFieldVisible)
				continue;
			PropertyEditor<?> pe = getPropertyEditor(pd, pd.getPropertyType(), this.bean, this.local);
			if (pe instanceof LocalisedEditor)
				((LocalisedEditor) pe).setLocale(getLocale());
			if (pe instanceof ChoiceEditor)
				try {
					DynamicChoiceBox choiceBoxAnno = getAnno(DynamicChoiceBox.class, this.beanClass, pd);
					if (choiceBoxAnno != null)
						if (choiceBoxAnno.useSwingWorker()) {
							waitingEditor.add(pe);
							waitingPossibleChoicesMethod.add(choiceBoxAnno.possibleChoicesMethod());
						} else
							((ChoiceEditor<?>) pe).setChoicesFromObjects((Object[]) invokeMethod(this.beanClass, this.bean, choiceBoxAnno.possibleChoicesMethod(), Object[].class));
				} catch (Exception e) {
					e.printStackTrace();
				}
			try {
				DynamicPossibilities possibilitiesAnno = getAnno(DynamicPossibilities.class, this.beanClass, pd);
				if (possibilitiesAnno != null)
					if (possibilitiesAnno.useSwingWorker()) {
						waitingEditor.add(pe);
						waitingPossibleChoicesMethod.add(possibilitiesAnno.possibleChoicesMethod());
					} else
						pe.setPossibilitiesFromObjects((Object[]) invokeMethod(this.beanClass, this.bean, possibilitiesAnno.possibleChoicesMethod(), toWrapper(pd.getPropertyType()).arrayType()));
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (pe instanceof MultiNumberEditor) {
				MultiNumberElementNames multiNumberElementNamesAnno = getAnno(MultiNumberElementNames.class, this.beanClass, pd);
				if (multiNumberElementNamesAnno != null)
					((MultiNumberEditor<?, ?>) pe).setNames(multiNumberElementNamesAnno.names());
			}
			if (pe == null)
				continue;
			if (pe instanceof BeanEditor)
				((BeanEditor<?>) pe).addBeanListChangedListener(this);
			// Je sais plus pk... seul le BeanEditor peut créer un bean
			// else if (pe instanceof ArrayEditor && ((ArrayEditor<?>) pe).getDefaultPatternEditor(null) instanceof BeanEditor)
			// ((ArrayEditor<?>) pe).addBeanListChangeListener(this);
			editablePropertiesList.add(new EditableProperty(pd, pe, index, readOnly));
		}
		LinkedHashMap<String, EditableProperty> editableProperties = editablePropertiesList.stream()
				.sorted((a, b) -> a.id != b.id ? Integer.compare(a.id, b.id) : a.pd.getName().compareTo(b.pd.getName()))
				.collect(Collectors.toMap(a -> a.pd.getName(), a -> a, (x, y) -> y, LinkedHashMap::new));
		int row = 0;
		for (final EditableProperty ep : editableProperties.values()) {
			PropertyEditor<?> pe = ep.pe;
			final PropertyDescriptor pd = ep.pd;
			Label nameLabel = new Label(pd.getName() + PROPERTY_SUFFIX/* , Label.LEFT */);
			Tooltip tt = new Tooltip(getToolTipText(ep));
			tt.setShowDelay(Duration.seconds(1));
			tt.setShowDuration(Duration.minutes(1));
			nameLabel.setTooltip(tt);
			try {
				pe.setValueFromObj(getValue(pd, this.bean));
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
				ex.printStackTrace();
			}
			// Mis après, sinon le getCustomEditor fait un event, NON sinon les valeurs par défaut obligatoire ne sont pas prises en compte (numberEditor et slider par ex.)
			pe.addPropertyChangeListener(() -> {
				try {
					setValue(pd, this.bean, pe.getValue()); // Comment savoir si un event est partie?
					this.isEdited = true;
					FxUtils.runLaterIfNeeded(() -> {
						if (this.mapLabelObject != null)
							for (Map.Entry<Label, ObjectViewDesc> entry : this.mapLabelObject.entrySet()) {
								ObjectViewDesc ovd = entry.getValue();
								if (ovd.editorPanel != null && ovd.name.equals(pd.getName())) {
									updateSubBeanExpension(entry.getKey(), null);
									updateSubBeanExpension(entry.getKey(), null);
									break;
								}
							}
					});
					firePropertyChangeFromBeanManager(); // Pour tout les set value dans un editor
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
					System.err.println("cannot set the value for the bean: " + this.bean + "\nThe editor: " + pe + "\nStackTrace: ");
					ex.printStackTrace();
				}
			});

			Region ce;
			boolean isWaitingEditor = waitingEditor.contains(pe);
			if (pe.hasCustomEditor()) {
				if (isWaitingEditor) {
					Object value = pe.getValue();
					ce = new Label(value != null ? value.toString() : "", new ImageView(new Image(getClass().getResourceAsStream("/waitingdot.gif")))/* , JLabel.CENTER */);
				} else {
					Region tempCe = pe.getEditor();
					if (tempCe == null)
						tempCe = new Label(pe.getClass().getSimpleName() + " did not return an editor region");
					ce = tempCe;
				}
				ep.ce = ce;
				ce.setDisable(ep.ro);
			} else
				ce = defaultLabelWhenNoEditor(ep);
			if (isWaitingEditor) {
				int currRow = row;
				if (toLaunch == null)
					toLaunch = new ArrayList<>();
				toLaunch.add(new Thread(new Task<Object[]>() {
					@Override
					public Object[] call() {
						try {
							return (Object[]) invokeMethod(BeanManager.this.beanClass, BeanManager.this.bean, waitingPossibleChoicesMethod.get(waitingEditor.indexOf(pe)),
									toWrapper(pd.getPropertyType()).arrayType());
						} catch (Exception e) {
							e.printStackTrace();
							return null;
						}
					}

					@Override
					protected void done() {
						try {
							pe.setPossibilitiesFromObjects(get());
						} catch (InterruptedException | ExecutionException e) {
							e.printStackTrace();
						}
						Platform.runLater(() -> {
							propertiesPanel.getChildren().remove(ce);
							Region ce;
							if (pe.hasCustomEditor()) {
								ce = pe.getEditor();
								ce.setDisable(ep.ce.isDisable() || ep.ro);
								ep.ce = ce;
							} else
								ce = defaultLabelWhenNoEditor(ep);
							propertiesPanel.add(ce, 1, currRow);
							fireSizeChanged(); // Commenter avant et je sais pas pk, la fenétre n'a pas la bonne taille
												// sinon
						});
					};
				}));
			}
			propertiesPanel.add(ce, 1, row);
			GridPane.setHalignment(ce, HPos.CENTER);
			GridPane.setValignment(ce, VPos.CENTER);

			RowConstraints rowConstraints = new RowConstraints();
			boolean canGrow = pe.canGrow();
			if (canGrow)
				canGrow = true;
			rowConstraints.setVgrow(canGrow ? Priority.ALWAYS : Priority.NEVER);
			propertiesPanel.getRowConstraints().add(rowConstraints);

			if (pe instanceof BeanEditor) {
				Polygon disclosureNode = new Polygon(0.0, 0.0, 6.0, 4.0, 0.0, 8.0);
				disclosureNode.setFill(new Color(51 / 255.0, 51 / 255.0, 51 / 255.0, 1));
				disclosureNode.setStroke(Color.BLACK);
				disclosureNode.setStrokeWidth(0.0);
				disclosureNode.setStrokeType(StrokeType.INSIDE);
				HBox.setMargin(disclosureNode, new Insets(0, 4, 0, 4));
				HBox disclosureAndNameBox = new HBox(0, disclosureNode, nameLabel);
				disclosureAndNameBox.setOnMouseClicked(e -> {
					if (e.getButton() == MouseButton.PRIMARY)
						updateSubBeanExpension(nameLabel, null);
				});
				disclosureAndNameBox.setAlignment(Pos.CENTER_LEFT);
				propertiesPanel.add(disclosureAndNameBox, 0, row);

				if (this.mapLabelObject == null)
					this.mapLabelObject = new HashMap<>();
				ObjectViewDesc ovd = new ObjectViewDesc(pd.getName(), (BeanEditor<?>) pe, row + 1);
				this.mapLabelObject.put(nameLabel, ovd);
				BeanInfo anno;
				if ((anno = getAnno(BeanInfo.class, this.beanClass, pd)) != null && anno.alwaysExtend())
					updateSubBeanExpension(nameLabel, toLaunch);
				rowConstraints = new RowConstraints();
				if (ovd.beanManager != null)
					rowConstraints.setVgrow(ovd.beanManager.canGrow ? Priority.ALWAYS : Priority.NEVER);
				propertiesPanel.getRowConstraints().add(rowConstraints);
				row++; // Si le gars clique, il faut decaler la suite
			} else
				propertiesPanel.add(nameLabel, 0, row);
			row++;

			if (pe instanceof DynamicSizeEditor)
				((DynamicSizeEditor) pe).addSizeChangeListener(this);
		}
		addPropertyChangeListener(pcl -> {
			char[] c = pcl.getPropertyName().toCharArray();
			c[0] = Character.toLowerCase(c[0]);
			String pName = new String(c);
			// System.out.println("property changed: " + pName);
			editableProperties.get(pName).pe.setValueFromObj(pcl.getNewValue());
			// for (EditableProperty ep : editableProperties)
			// if (ep.pd.getName().equals(pName))
			// ep.pe.setValueFromObj(pcl.getNewValue());
		});
		if (row == 0)
			propertiesPanel.add(new Label("no properties for: " + this.beanClass.getSimpleName()), 0, 0);

		if (isDynamicAnnotation(this.bean))
			DynamicAnnotationBean.addAnnotationChangeListener(this.bean, propertyName -> {
				if (Character.isUpperCase(propertyName.charAt(0)))
					propertyName = Character.toLowerCase(propertyName.charAt(0)) + propertyName.substring(1);
				EditableProperty ep = editableProperties.get(propertyName);
				if (ep == null)
					return;
				PropertyEditorManager.updateDynamicAnnotationEditorProperties(ep.pd.getPropertyType(), ep.pe, getAnno(this.beanClass, ep.pd),
						getMethodInvocator(BeanManagerFactory.create(this.bean, ""), this.beanClass));
				// for (EditableProperty ep : editableProperties)
				// if (ep.pd.getName().equals(propertyName)) {
				// PropertyEditorManager.updateDynamicAnnotationEditorProperties(ep.pd.getPropertyType(), ep.pe, getAnno(beanClass, ep.pd), getMethodInvocator(BeanManagerFactory.create(bean, ""),
				// beanClass));
				// break;
				// }
			});
		if (isDynamicEnableBean(this.bean)) {
			DynamicEnableBean.addEnableChangeListener(this.bean, (propertyName, enable) -> {
				if (Character.isUpperCase(propertyName.charAt(0)))
					propertyName = Character.toLowerCase(propertyName.charAt(0)) + propertyName.substring(1);
				EditableProperty ep = editableProperties.get(propertyName);
				// for (EditableProperty ep : editableProperties)
				// if (ep.pd.getName().equals(propertyName)) {
				if (ep == null)
					return;
				Region component = ep.ce;
				if (!enable) { // TODO PK??? éviter interaction?
					ChangeListener<Boolean> changeListener = (a, oldDisableValue, newDisableValue) -> {
						if (!newDisableValue && this.disabledProperties != null && this.disabledProperties.containsKey(ep.ce))
							component.setDisable(true);
					};
					component.disabledProperty().addListener(changeListener);
					if (this.disabledProperties == null)
						this.disabledProperties = new HashMap<>();
					this.disabledProperties.put(ep.ce, changeListener);
				} else if (this.disabledProperties != null) {
					ChangeListener<Boolean> changeListener = this.disabledProperties.get(component);
					if (changeListener != null) {
						component.disabledProperty().removeListener(changeListener);
						this.disabledProperties.remove(component);
						if (this.disabledProperties.isEmpty())
							this.disabledProperties = null;
					}
				}
				component.setDisable(!enable);
				// break;
				// }
			});
			setEnable(this.bean);
		}
		if (isDynamicVisibleBean(this.bean)) {
			DynamicVisibleBean.addVisibleChangeListener(this.bean, (propertyName, visible) -> {
				if (Character.isUpperCase(propertyName.charAt(0)))
					propertyName = Character.toLowerCase(propertyName.charAt(0)) + propertyName.substring(1);
				EditableProperty ep = editableProperties.get(propertyName);
				if (ep == null)
					return;
				// for (EditableProperty ep : editableProperties)
				// if (ep.pd.getName().equals(propertyName)) {
				Region component = ep.ce;
				if (!visible) {
					ChangeListener<Boolean> changeListener = (a, oldDisableValue, newDisableValue) -> {
						if (!newDisableValue && this.visibleProperties != null && this.visibleProperties.containsKey(ep.ce))
							component.setVisible(false);
					};
					component.visibleProperty().addListener(changeListener);
					if (this.visibleProperties == null)
						this.visibleProperties = new HashMap<>();
					this.visibleProperties.put(ep.ce, changeListener);
				} else if (this.visibleProperties != null) {
					ChangeListener<Boolean> changeListener = this.visibleProperties.get(component);
					if (changeListener != null) {
						component.visibleProperty().removeListener(changeListener);
						this.visibleProperties.remove(component);
						if (this.visibleProperties.isEmpty())
							this.visibleProperties = null;
					}
				}
				component.setVisible(visible);
				component.setManaged(visible);
				for (Node node : propertiesPanel.getChildren())
					if (GridPane.getColumnIndex(node) == 0 && node instanceof Label) {
						Label label = (Label) node;
						String labelText = label.getText();
						int indexOfNameEnd = labelText.indexOf(":");
						if (indexOfNameEnd != -1) {
							String name = labelText.substring(0, indexOfNameEnd);
							if (name.equals(propertyName)) {
								label.setVisible(visible);
								label.setManaged(visible);
								break;
							}
						}
					}
				sizeChanged();
				// break;
				// }
			});
			setVisible(this.bean);
		}

		if (isUpdatableViewBean(this.bean))
			UpdatableViewBean.addViewChangeListener(this.bean, (propertyName, reloadValue) -> {
				if (this.isUpdatingView)
					return;
				if (Character.isUpperCase(propertyName.charAt(0)))
					propertyName = Character.toLowerCase(propertyName.charAt(0)) + propertyName.substring(1);
				this.isUpdatingView = true;
				EditableProperty ep = editableProperties.get(propertyName);
				// for (EditableProperty ep : editableProperties)
				// if (ep.pd.getName().equals(propertyName)) {
				if (ep == null)
					return;
				PropertyDescriptor pd = ep.pd;
				PropertyEditor<?> pe = ep.pe;
				try {
					Annotation choiceBoxAnno = getAnno(DynamicPossibilities.class, this.beanClass, pd);
					if (choiceBoxAnno != null)
						pe.setPossibilitiesFromObjects(
								(Object[]) invokeMethod(this.beanClass, this.bean, ((DynamicPossibilities) choiceBoxAnno).possibleChoicesMethod(), toWrapper(pd.getPropertyType()).arrayType()));
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (pe instanceof ChoiceEditor)
					try {
						Annotation choiceBoxAnno = getAnno(DynamicChoiceBox.class, this.beanClass, pd);
						if (choiceBoxAnno != null)
							((ChoiceEditor<?>) pe)
									.setChoicesFromObjects((Object[]) invokeMethod(this.beanClass, this.bean, ((DynamicChoiceBox) choiceBoxAnno).possibleChoicesMethod(), Object[].class));
					} catch (Exception e) {
						e.printStackTrace();
					}
				if (pe instanceof BeanEditor)
					try {
						Annotation dynamicBeanInfoAnno = getAnno(DynamicBeanInfo.class, this.beanClass, pd);
						if (dynamicBeanInfoAnno != null)
							((BeanEditor<?>) pe)
									.setSubClasses((Class<?>[]) invokeMethod(this.beanClass, this.bean, ((DynamicBeanInfo) dynamicBeanInfoAnno).possibleSubclassesMethodName(), Class[].class));
					} catch (Exception e) {
						e.printStackTrace();
					}
				if (reloadValue)
					try {
						pe.setValueFromObjWithoutEvent(getValue(ep.pd, this.bean));
					} catch (IllegalArgumentException | IllegalAccessException | InvocationTargetException e) {
						e.printStackTrace();
					}
				// pe.updateView();
				FxUtils.runLaterIfNeeded(() -> fireSizeChanged());
				// break;
				// }
				this.isUpdatingView = false;
			});
		return propertiesPanel;
	}

	private static Label defaultLabelWhenNoEditor(final EditableProperty ep) {
		return new Label("No editor for " + ep.pd.getPropertyType().getSimpleName() + " property");
	}

	protected InetSocketAddress getLocale() {
		return null;
	}

	public List<PropertyDescriptor> getPropertyDescriptors() {
		return getPropertyDescriptors(this.beanClass);
	}

	public PropertyDescriptor getPropertyDescriptors(String name) {
		if (this.propertyDescriptorMap == null) {
			this.propertyDescriptorMap = new HashMap<>();
			for (PropertyDescriptor pd : getPropertyDescriptors())
				this.propertyDescriptorMap.put(pd.getName(), pd);
		}
		return this.propertyDescriptorMap.get(name);
	}

	private String getToolTipText(EditableProperty ep) {
		String simpleName = ep.pd.getPropertyType().getSimpleName();
		String propertyName = ep.pd.getPropertyType().getName();
		String info = null;
		String unit = null;
		PropertyInfo fieldInfoAnno = getAnno(PropertyInfo.class, this.beanClass, ep.pd);
		if (fieldInfoAnno != null) {
			info = fieldInfoAnno.info();
			unit = fieldInfoAnno.unit();
		}
		String text = "";
		if (info != null && !info.isEmpty())
			text += info + "\n";
		text += "Type: " + (simpleName.equals(propertyName) ? simpleName : simpleName + " -> " + propertyName);
		String description = ep.pe.getDescription();
		if (description != null)
			text += " " + description;
		text += "\n";
		if (unit != null && !unit.isEmpty())
			text += "Unit: " + unit;
		return text;
	}

	protected Object getValue(PropertyDescriptor pd, Object bean) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Method rm = pd.getReadMethod();
		if (!rm.canAccess(bean)) {// !rm.canAccess(bean)
			rm.setAccessible(true);
			Object val = rm.invoke(bean, (Object[]) null);
			rm.setAccessible(false);
			return val;
		}
		return rm.invoke(bean, (Object[]) null);
	}

	public Object getValue(String propertyName) {
		PropertyDescriptor pd = getPropertyDescriptors(propertyName);
		if (pd != null)
			try {
				return getValue(pd, this.bean);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			}
		return null;
	}

	protected Object invokeMethod(Class<?> beanClass, Object bean, String methodname)
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		Method m = beanClass.getMethod(methodname);
		if (!m.canAccess(bean)) {
			m.setAccessible(true);
			Object val = m.invoke(bean);
			m.setAccessible(false);
			return val;
		}
		return beanClass.getMethod(methodname).invoke(bean);
	}

	protected Object invokeMethod(Class<?> beanClass, Object bean, String methodname, Class<?> expectedReturnClass)
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		Object res = invokeMethod(beanClass, bean, methodname);
		if (res != null && expectedReturnClass != null && !expectedReturnClass.isAssignableFrom(res.getClass()))
			throw new IllegalArgumentException("The invocation of: " + beanClass.getSimpleName() + "." + methodname + " return " + res.getClass().getName()
					+ " instance instead of expected return instance type: " + expectedReturnClass.getName());
		return res;
	}

	protected boolean isDynamicAnnotation(Object bean) {
		return bean instanceof DynamicAnnotationBean;
	}

	protected boolean isDynamicEnableBean(Object bean) {
		return bean instanceof DynamicEnableBean;
	}

	protected boolean isDynamicVisibleBean(Object bean) {
		return bean instanceof DynamicVisibleBean;
	}

	protected boolean isUpdatableViewBean(Object bean) {
		return bean instanceof UpdatableViewBean;
	}

	public boolean load() {
		if (getBeansFile() == null)
			System.err.println("cannot find: " + this.bean);
		return load(getBeansFile());
	}

	public boolean load(File file) {
		if (loadBeanMethod != null)
			return loadBeanMethod.apply(this.bean, file);
		Logger.log(Logger.SAVE_OR_LOAD_FILE, "load: " + file);
		try {
			load(new FileInputStream(file), file.getParent() + File.separator);
		} catch (IOException ex) {
			if (ex instanceof NoSuchFileException | ex instanceof FileNotFoundException)
				System.err.println("The file: " + file + " does not exist");
			else
				ex.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean load(InputStream inputStream, String local) throws IOException {
		HashMap<String, String> mapProperties = new HashMap<>();
		try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))) {
			String line;
			while ((line = br.readLine()) != null) {
				int i = line.indexOf(":");
				if (i != -1)
					mapProperties.put(line.substring(0, i), line.substring(i + 1, line.length()));
			}
		}
		if (local == null)
			local = System.getProperty("user.dir");
		loadPropertiesFromMap(mapProperties, local);
		return true;
	}

	private void loadPropertiesFromMap(HashMap<String, String> mapProperties, String local) {
		for (final PropertyDescriptor pd : getPropertyDescriptors()) {
			PropertyInfo fieldInfoAnno = getAnno(PropertyInfo.class, this.bean.getClass(), pd);
			if (fieldInfoAnno != null && fieldInfoAnno.readOnly())
				continue;
			Class<?> type = pd.getPropertyType();
			PropertyEditor<?> editor = getPropertyEditor(pd, pd.getPropertyType(), this.bean, local + File.separator);
			if (editor == null) {
				System.err.println("No editor for the type: " + type + "\nfor the bean: " + this.beanClass.getName() + "\nfor the method: " + pd.getName());
				editor = getPropertyEditor(pd, pd.getPropertyType(), this.bean, local + File.separator);
				continue;
			}
			boolean propertyError;
			Object value = null;
			if (fieldInfoAnno == null || fieldInfoAnno.savable())
				try {
					String textValue = mapProperties.get(pd.getName());
					if (textValue != null) { // Pas commenter sinon je mets a null les nouvelles propriété..., pk commenter avant?
						if (editor instanceof BeanEditor && ((BeanEditor<?>) editor).isInline() && textValue.length() != 0)
							try {
								HashMap<String, String> mapProp = new HashMap<>();
								int beginIndex = textValue.indexOf("{") + 1;
								Class<?> inlineType;
								if (((BeanEditor<?>) editor).hasTypeChoice()) {
									int indexOfSemiColon = textValue.indexOf(";");
									if (indexOfSemiColon != -1) {
										String className = textValue.substring(beginIndex, indexOfSemiColon);
										try {
											inlineType = BeanManager.getClassFromDescriptor(className);
										} catch (ClassNotFoundException e) {
											inlineType = null;
											for (Class<?> class1 : ((BeanEditor<?>) editor).getSubClasses())
												if (className.equals(class1.getName())) {
													inlineType = class1;
													break;
												}
											if (inlineType == null) {
												System.err.println("Cannot find class: " + className);
												continue;
											}
										}
										beginIndex = indexOfSemiColon + 1;
									} else
										inlineType = null;
								} else
									inlineType = pd.getPropertyType();
								ArrayList<PropertyDescriptor> inlinePds = new ArrayList<>(inlineType == null ? List.of() : BeanManager.getPropertyDescriptors(inlineType));
								while (beginIndex != textValue.length()) {
									int colonIndex = textValue.indexOf(":", beginIndex);
									if (colonIndex == -1)
										break;
									String propertyName = null;
									try {
										propertyName = textValue.substring(beginIndex, colonIndex);
									} catch (StringIndexOutOfBoundsException e) {
										e.printStackTrace();
									}
									PropertyDescriptor propertyDescriptor = null;
									for (PropertyDescriptor otherPropertyDescriptor : inlinePds)
										if (otherPropertyDescriptor.getName().equals(propertyName)) {
											propertyDescriptor = otherPropertyDescriptor;
											break;
										}
									if (propertyDescriptor != null) {
										inlinePds.remove(propertyDescriptor);
										PropertyDescriptor finalPropertyDescriptor = propertyDescriptor;
										PropertyEditor<?> inlineEditor = PropertyEditorManager.findEditor(propertyDescriptor.getPropertyType(), local, null,
												() -> getGenericTypes(finalPropertyDescriptor));
										if (inlineEditor != null) {
											colonIndex++;
											int endIndex;
											if (inlineEditor.canContainForbiddenCharacter()) {
												int spaceIndex = textValue.indexOf(" ", colonIndex);
												int nbChar = Integer.parseInt(textValue.substring(colonIndex, spaceIndex++));
												endIndex = spaceIndex + nbChar;
												mapProp.put(propertyName, textValue.substring(spaceIndex, endIndex));
											} else {
												int nbAccolade = 0;
												endIndex = colonIndex;
												Character letter;
												while (textValue.length() != endIndex && ((letter = textValue.charAt(endIndex)) != ';' || nbAccolade != 0)) {
													if (letter == '}')
														nbAccolade--;
													else if (letter == '{')
														nbAccolade++;
													endIndex++;
												}
												if (textValue.length() == endIndex)
													endIndex--;
												mapProp.put(propertyName, textValue.substring(colonIndex, endIndex));
											}
											beginIndex = endIndex + 1;
										}
									} else
										break;
								}
								value = getValue(pd, this.bean);
								if (value == null && inlineType != null) {
									if (pd.getPropertyType().isMemberClass()) {
										Constructor<?> con = inlineType.getConstructor(this.beanClass);
										if (!con.canAccess(this.bean))
											con.setAccessible(true);
										value = con.newInstance(this.bean);
									} else {
										Constructor<?> con = inlineType.getConstructor(); // TODO et le inline type, il est pas utilisé???
										if (!con.canAccess(null))
											con.setAccessible(true);
										value = con.newInstance();
									}
									setValue(pd, this.bean, value); // Pas cette ligne avant et j'avais getValue(pd, bean) qui était donc naturellement à null...
								}
								Object v = getValue(pd, this.bean);
								if (v != null)
									new BeanManager(getValue(pd, this.bean), local).loadPropertiesFromMap(mapProp, local);
							} catch (Exception e) {
								e.printStackTrace();
								propertyError = true;
								System.err.println("error during setting inline object properties: " + pd.getName() + ", in class: " + this.bean);
							}
						else {
							editor.setAsText(textValue);
							value = editor.getValue();
						}
						propertyError = false;
					} else
						propertyError = true;
				} catch (Exception ex) {
					ex.printStackTrace();
					propertyError = true;
					System.err.println("error during setting properties: " + pd.getName() + ", in class: " + this.bean);
					continue;
				}
			else {
				try {
					value = getValue(pd, this.bean);
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
				}
				propertyError = false;
			}
			// if (fieldInfoAnno != null && fieldInfoAnno.readOnly()) //Pas utilile de faire un get si readonly
			// continue;
			if (value == null && (!editor.isNullable() || propertyError)) { // Si valeur null et interdit on set pas
				if (editor instanceof BeanEditor) // On register par contre si c'est un objet
					try {
						Object defaultVal = getValue(pd, this.bean);
						if (defaultVal == null && !editor.isNullable())
							try {
								defaultVal = ((BeanEditor<?>) editor).createDefaultObject();
								setValue(pd, this.bean, defaultVal);
							} catch (InstantiationException | NoSuchMethodException | SecurityException e) {
								e.printStackTrace();
							}
						if (defaultVal != null && (fieldInfoAnno == null || fieldInfoAnno.savable()) && !((BeanEditor<?>) editor).isInline()) {
							BeanEditor.registerCreatedBeanAndCreateSubBean(defaultVal, local);
							this.isEdited = true; // Faut forcer la sauvegarde du fichier
						}
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						System.err.println("Cannot get the value: " + value + " \nfor the bean: " + this.beanClass.getName() + "\nfor the method: " + pd.getName() + "\ncause: " + e.getCause());
						e.printStackTrace();
					}
			} else
				try {
					if (value != null || !type.isPrimitive())
						setValue(pd, this.bean, value);
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					System.err.println("Cannot set the value: " + value + " \nfor the bean: " + this.beanClass.getName() + "\nfor the method: " + pd.getName() + "\ncause: " + e.getCause());
					e.printStackTrace();
				}
		}
		if (this.isEdited) // Ajout du 31/08/17: Si il manque un subbean à un bean je le construit, mais si je sauvegarde pas, je vais recréer à chaque fois le sous bean
			saveIfChanged();
	}

	private void populateSubBeans(Object fatherBean, HashSet<Object> subBeans) {
		for (Object subBean : getSubBeans(fatherBean, this.local))
			if (subBeans.add(subBean))
				populateSubBeans(subBean, subBeans);
	}

	public void removeBeanAndSubBeanPropertyChangeListener(BeanAndSubBeanPropertyChangeListener listener) {
		if (listener == null)
			return;
		synchronized (listener) {
			if (this.beanAndSubBeanPropertyChangeListenerMap == null)
				return;
			HashMap<Object, BeanManagerListener> map = this.beanAndSubBeanPropertyChangeListenerMap.remove(listener);
			if (map != null)
				for (BeanManagerListener bml : map.values())
					bml.beanManager.removeBeanAndSubBeanPropertyChangeListener(listener);
		}
	}

	public void removePropertyChangeFromBeanManagerListener(PropertyChangeFromBeanManagerListener listener) {
		this.listeners.remove(PropertyChangeFromBeanManagerListener.class, listener);
	}

	public void removePropertyChangeListener(java.beans.PropertyChangeListener listener) {
		if (listener == null)
			return;
		try {
			for (EventSetDescriptor esd : Introspector.getBeanInfo(this.beanClass).getEventSetDescriptors())
				if (esd.getName().equals("propertyChange"))
					try {
						esd.getRemoveListenerMethod().invoke(getBean(), listener);
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
						ex.printStackTrace();
					}
		} catch (IntrospectionException e) {}
	}

	public void removeSizeChangeListener(DynamicSizeEditorChangeListener listener) {
		this.listeners.remove(DynamicSizeEditorChangeListener.class, listener);
	}

	public void reset() {
		if (this.bean instanceof ResetListener && ((ResetListener) this.bean).reset())
			return;
		try {
			Object defaultBean = this.beanClass.getConstructor().newInstance();
			for (final PropertyDescriptor pd : getPropertyDescriptors()) {
				PropertyInfo fieldInfoAnno;
				if ((fieldInfoAnno = getAnno(PropertyInfo.class, this.bean.getClass(), pd)) == null || !fieldInfoAnno.readOnly()) // pk savable???
					setValue(pd, this.bean, getValue(pd, defaultBean));
			}
			createSubBeans(this.bean, this.local);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
	}

	public boolean save(boolean saveSubBean) {
		File beanFile = getBeansFile();
		if (beanFile != null) {
			save(beanFile, saveSubBean);
			saveVisibleSubBean();
			return true;
		}
		return false;
	}

	public void save(File serClassFile, boolean saveSubBean) {
		Logger.log(Logger.SAVE_OR_LOAD_FILE, "save: " + serClassFile);
		try {
			save(new FileOutputStream(serClassFile), serClassFile.getParent());
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (saveSubBean)
			saveSubBeans();
	}

	public void save(OutputStream outputStream, String local) throws IOException {
		if (local == null)
			local = System.getProperty("user.dir");
		try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(outputStream, StandardCharsets.UTF_8))) {
			for (final PropertyDescriptor pd : getPropertyDescriptors()) {
				PropertyInfo fieldInfoAnno;
				if ((fieldInfoAnno = getAnno(PropertyInfo.class, this.bean.getClass(), pd)) != null && !fieldInfoAnno.savable())
					continue;
				PropertyEditor<?> editor = getPropertyEditor(pd, pd.getPropertyType(), this.bean, local);
				if (editor == null) {
					System.err.println("no editor for: " + pd.getPropertyType());
					continue;
				}
				try {
					String stringValue = null;
					if (editor instanceof BeanEditor && ((BeanEditor<?>) editor).isInline()) {
						stringValue = "{";
						Object value = getValue(pd, this.bean);
						if (value != null) {
							ArrayList<String> lines = new BeanManager(value, local).getAsText(local);
							if (((BeanEditor<?>) editor).hasTypeChoice())
								stringValue += value.getClass().getName() + PropertyEditor.PROPERTY_SEPARATOR;
							for (int i = 0; i < lines.size() - 1; i++)
								stringValue += lines.get(i) + PropertyEditor.PROPERTY_SEPARATOR;
							if (!lines.isEmpty())
								stringValue += lines.get(lines.size() - 1) + "}";
						} else
							stringValue = "";
					} else {
						editor.setValueFromObj(getValue(pd, this.bean));
						stringValue = editor.getAsText();
					}
					bw.write(pd.getName() + ":" + (stringValue == null ? /* String.valueOf((Object) null) */"" : stringValue));
					bw.newLine();
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public boolean saveIfChanged() {
		boolean saveFather = false;
		this.isEdited |= saveVisibleSubBean();
		if (this.isEdited) {
			this.isEdited = false;
			File file = getBeansFile();
			if (file != null)
				save(file, false);
			else
				saveFather = true;
		}
		if (this.bean instanceof SaveListener)
			((SaveListener) this.bean).save(this.isEdited);
		return saveFather;
	}

	public boolean saveIfChanged(File file) {
		boolean saveFather = false;
		this.isEdited |= saveVisibleSubBean();
		if (this.isEdited) {
			this.isEdited = false;
			save(file, false);
		}
		if (this.bean instanceof SaveListener)
			((SaveListener) this.bean).save(this.isEdited);
		return saveFather;
	}

	private File getBeansFile() { // TODO Module getDescriptorFromClass
		BeanDesc<?> beanDesc = BeanEditor.getBeanDesc(this.bean);
		return beanDesc == null ? null : new File(beanDesc.local + beanDesc.getSimpleDescriptor() + SERIALIZE_EXT);
	}

	public void saveSubBeans() {
		LinkedList<Object> nodeHeap = new LinkedList<>();
		nodeHeap.push(this.bean);
		while (!nodeHeap.isEmpty())
			for (Object subBean : BeanManager.getSubBeans(nodeHeap.pop(), this.local)) {
				new BeanManager(subBean, this.local).save(false);
				nodeHeap.push(subBean);
			}
	}

	private boolean saveVisibleSubBean() {
		boolean saveFather = false;
		if (this.mapLabelObject != null)
			for (ObjectViewDesc ovd : this.mapLabelObject.values())
				if (ovd.beanManager != null)
					if (ovd.objEditor.isInline())
						saveFather = true;
					else
						saveFather |= ovd.beanManager.saveIfChanged();
		return saveFather;
	}

	protected void setEnable(Object bean) {
		((DynamicEnableBean) bean).setEnable();
	}

	protected void setValue(PropertyDescriptor pd, Object bean, Object value) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Method wm = pd.getWriteMethod();
		if (!wm.canAccess(bean)) {// !wm.canAccess(bean)
			wm.setAccessible(true);
			wm.invoke(bean, value instanceof BeanDesc ? ((BeanDesc<?>) value).bean : value);
			wm.setAccessible(false);
		} else
			wm.invoke(bean, value instanceof BeanDesc ? ((BeanDesc<?>) value).bean : value);
	}

	public void setValue(String propertyName, Object value) {
		PropertyDescriptor pd = getPropertyDescriptors(propertyName);
		if (pd != null)
			try {
				setValue(pd, this.bean, value);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			}
	}

	protected void setVisible(Object bean) {
		((DynamicVisibleBean) bean).setVisible();
	}

	@Override
	public void sizeChanged() {
		fireSizeChanged();
	}

	private void subBeanGarbageCollector(Object fatherBean, HashMap<Object, BeanManagerListener> map, BeanAndSubBeanPropertyChangeListener listener) {
		HashSet<Object> subBeans = new HashSet<>();
		populateSubBeans(fatherBean, subBeans);
		ArrayList<Object> beansToRemove = new ArrayList<>();
		for (BeanManagerListener brc : map.values()) {
			Object beanCandidateToRemove = brc.beanManager.bean;
			if (!subBeans.contains(beanCandidateToRemove)) {
				brc.beanManager.removePropertyChangeListener(brc.propertyChangeListener);
				beansToRemove.add(beanCandidateToRemove);
			}
		}
		for (Object beanToRemove : beansToRemove)
			map.remove(beanToRemove);
	}

	private void updateSubBeanExpension(Label label, ArrayList<Thread> toLaunch) {
		ObjectViewDesc ovd = this.mapLabelObject.get(label);
		HBox disclosureAndNameBox = (HBox) label.getParent();
		GridPane gridPane = (GridPane) disclosureAndNameBox.getParent(); // NullPointerException
		Polygon disclosureNode = (Polygon) disclosureAndNameBox.getChildren().get(0);
		BeanEditor<?> objEditor = ovd.objEditor;
		Object beandesc = objEditor.getValue();
		BeanManager listenerBm = null;
		boolean expanded = ovd.editorPanel == null;
		if (expanded) {
			Region editorPanel;
			if (beandesc == null)
				editorPanel = new Label(String.valueOf((Object) null));
			else {
				BeanManager bm = new BeanManager(beandesc, this.local);
				editorPanel = bm.getEditor(toLaunch, null);
				bm.addSizeChangeListener(this);
				ovd.beanManager = bm;
				ObservableList<RowConstraints> rowConstraints = gridPane.getRowConstraints();
				if (ovd.gridy < rowConstraints.size())
					rowConstraints.get(ovd.gridy).setVgrow(bm.canGrow ? Priority.ALWAYS : Priority.NEVER);
				listenerBm = bm;
			}
			editorPanel.setPadding(new Insets(5));
			editorPanel.setBorder(new Border(new BorderStroke(Color.DARKGREY, BorderStrokeStyle.SOLID, new CornerRadii(10), null, new Insets(5, 0, 5, 10))));
			gridPane.add(editorPanel, 0, ovd.gridy, 2, 1);
			ovd.editorPanel = editorPanel;
			disclosureNode.setRotate(90);
		} else {
			gridPane.getChildren().remove(ovd.editorPanel);
			ovd.editorPanel = null;
			if (ovd.beanManager != null) {
				if (ovd.beanManager.isEdited)
					if (!ovd.objEditor.isInline())
						ovd.beanManager.saveIfChanged();
				ovd.beanManager.removeSizeChangeListener(this);
				listenerBm = ovd.beanManager;
				ovd.beanManager = null;
			}
			disclosureNode.setRotate(0);
		}
		fireSizeChanged();
		fireSubBeanExpanded(expanded, getPropertyNameFromLabelName(label.getText()), listenerBm, !(ovd.editorPanel instanceof GridPane) ? null : (GridPane) ovd.editorPanel);
	}

	public static void addSubBeanChangeListener(SubBeanChangeListener listener) {
		subBeanlisteners.add(SubBeanChangeListener.class, listener);
	}

	public static void createSubBeans(Object parentBean, String local) {
		if (createSubBeansWithoutEvent(parentBean, local))
			fireSubBeanChange(BeanEditor.getBeanDesc(parentBean));
	}

	private static boolean createSubBeansWithoutEvent(Object parentBean, String local) {
		boolean hasSubBeans = false;
		for (final PropertyDescriptor pd : getPropertyDescriptors(parentBean.getClass())) {
			PropertyEditor<?> editor = getPropertyEditor(pd, pd.getPropertyType(), parentBean, local);
			try {
				if (editor instanceof PropertyContainerEditor && !((PropertyContainerEditor<?>) editor).isInline()) {
					PropertyContainerEditor<?> containerEditor = (PropertyContainerEditor<?>) editor;
					PropertyInfo fieldInfoAnno;
					if ((fieldInfoAnno = getAnno(PropertyInfo.class, parentBean.getClass(), pd)) != null && fieldInfoAnno.readOnly())
						continue;
					editor.setValueFromObj(pd.getReadMethod().invoke(parentBean, new Object[0]));
					List<Object> subBeans = containerEditor.getSubBeans();
					if (subBeans == null)
						continue;
					for (Object subBean : subBeans)
						BeanEditor.registerCreatedBeanAndCreateSubBean(subBean, local);
					hasSubBeans = true;
				}
			} catch (IllegalArgumentException | IllegalAccessException | InvocationTargetException e) {}
		}
		return hasSubBeans;
	}

	public static void fireSubBeanChange(BeanDesc<?> owner) {
		for (SubBeanChangeListener listener : subBeanlisteners.getListeners(SubBeanChangeListener.class))
			listener.subBeanChange(owner);
	}

	private static Annotation[] getAnno(Class<?> beanClass, PropertyDescriptor pd) {
		String name = pd.getName();
		Class<?> beanClassForFields = beanClass;
		while (true) {
			try {
				Annotation[] annotations = beanClassForFields.getDeclaredField(name).getAnnotations();
				if (annotations != null && annotations.length != 0)
					return annotations;
			} catch (NoSuchFieldException | SecurityException e) {}
			Class<?> superClass = beanClassForFields.getSuperclass();
			if (superClass == null)
				break;
			beanClassForFields = superClass;
		}
		Method rm = pd.getReadMethod();
		while (true) {
			try {
				Annotation[] annotations = beanClass.getMethod(rm.getName(), rm.getParameterTypes()).getAnnotations();
				if (annotations != null && annotations.length != 0)
					return annotations;
			} catch (NoSuchMethodException | SecurityException e) {}
			Class<?> superClass = beanClass.getSuperclass();
			if (superClass == null)
				return null;
			beanClass = superClass;
		}
	}

	public static <T extends Annotation> T getAnno(Class<T> annoClass, Class<?> beanClass, PropertyDescriptor pd) {
		String name = pd.getName();
		Class<?> beanClassForFields = beanClass;
		while (true) {
			try {
				T annotation = beanClassForFields.getDeclaredField(name).getAnnotation(annoClass);
				if (annotation != null)
					return annotation;
			} catch (NoSuchFieldException | SecurityException e) {}
			Class<?> superClass = beanClassForFields.getSuperclass();
			if (superClass == null)
				break;
			beanClassForFields = superClass;
		}
		Method rm = pd.getReadMethod();
		while (true) {
			try {
				T annotation = beanClass.getMethod(rm.getName(), rm.getParameterTypes()).getAnnotation(annoClass);
				if (annotation != null)
					return annotation;
			} catch (NoSuchMethodException | SecurityException e) {}
			Class<?> superClass = beanClass.getSuperclass();
			if (superClass == null)
				return null;
			beanClass = superClass;
		}
	}

	public static Class<?>[] getGenericTypes(PropertyDescriptor pd) {
		ArrayList<Class<?>> genericType = new ArrayList<>();
		Type returnType = pd.getReadMethod().getGenericReturnType();
		if (returnType instanceof ParameterizedType) {
			Type d = ((ParameterizedType) returnType).getActualTypeArguments()[0];
			if (d instanceof Class<?>)
				genericType.add((Class<?>) d);
		}
		return genericType.toArray(new Class<?>[genericType.size()]);
	}

	public static List<PropertyDescriptor> getPropertyDescriptors(Class<?> beanClass) {
		List<PropertyDescriptor> pds = propertyDescriptorsCache.get(beanClass);
		if (pds != null)
			return pds;
		pds = new ArrayList<>();
		try {
			if (beanClass.getModule().getName() != null && beanClass.getModule().getName().contains("Sample"))
				System.err.println("ignored Sample properties");
			else
				for (PropertyDescriptor pd : Introspector.getBeanInfo(beanClass).getPropertyDescriptors()) {
					Method rm = pd.getReadMethod();
					Method wm = pd.getWriteMethod();
					if (rm != null && wm != null && wm.getParameterTypes()[0].isAssignableFrom(pd.getPropertyType()) && rm.getReturnType().isAssignableFrom(pd.getPropertyType())) {
						// System.out.println(pd.getName());
						Class<?> dc = rm.getDeclaringClass();
						if (dc != beanClass && dc.getAnnotation(BeanPropertiesInheritanceLimit.class) == null)
							continue;
						if (getAnno(TransientProperty.class, dc, pd) == null)
							pds.add(pd);
					}
				}
		} catch (IntrospectionException e) {}
		propertyDescriptorsCache.put(beanClass, Collections.unmodifiableList(pds));
		return pds;
	}

	public static <T> PropertyEditor<T> getPropertyEditor(PropertyDescriptor pd, Class<T> type, Object bean, String local) {
		BeanManager bm = BeanManagerFactory.create(bean, "");
		Class<?> beanClass = bm.getBeanClass();
		Function<String, Object> methodInvocator = getMethodInvocator(bm, beanClass);
		Annotation[] annos = getAnno(beanClass, pd);
		PropertyEditor<T> propEditor = PropertyEditorManager.findEditor(type, local, containerType -> getPropertyEditor(pd, containerType, bean, local), () -> getGenericTypes(pd), annos,
				methodInvocator);
		return propEditor;
	}

	private static Function<String, Object> getMethodInvocator(BeanManager bm, Class<?> beanClass) {
		return methodName -> {
			try {
				return bm.invokeMethod(beanClass, bm.getBean(), methodName);
			} catch (Exception e) {
				System.err.println(
						"Error while invoking the bean method " + methodName + " of the bean: " + bm.getBean() + " of type: " + beanClass + "\nError: " + e.getClass() + ": " + e.getMessage());
				e.printStackTrace();
				return null;
			}
		};
	}

	public static LinkedList<Object> getSubBeans(Object bean, String local) {
		LinkedList<Object> subBeansList = new LinkedList<>(new HashSet<>(getSubBeansList(bean, local)));
		Collections.sort(subBeansList, (o1, o2) -> o2.toString().compareTo(o1.toString()));
		return subBeansList;
	}

	private static ArrayList<Object> getSubBeansList(Object bean, String local) {
		ArrayList<Object> subBeansList = new ArrayList<>();
		for (final PropertyDescriptor pd : getPropertyDescriptors(bean.getClass())) {
			PropertyEditor<?> editor = getPropertyEditor(pd, pd.getPropertyType(), bean, local);
			try {
				try {
					editor.setValueFromObj(pd.getReadMethod().invoke(bean, new Object[0]));
				} catch (NullPointerException e) {
					System.err.println("No editor for:" + pd.getPropertyType().getSimpleName());
				}
				if (editor instanceof PropertyContainerEditor) {
					List<Object> subBeans = ((PropertyContainerEditor<?>) editor).getSubBeans();
					if (subBeans != null)
						subBeansList.addAll(subBeans);
				}
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {}
		}
		return subBeansList;
	}

	public static boolean isAssignable(Class<?> type1, Class<?> type2) {
		type1 = toWrapper(type1);
		type2 = toWrapper(type2);
		return type1.isAssignableFrom(type2);
	}

	private static boolean isLinkedBean(Object bean) {
		for (BeanDesc<?> parent : BeanEditor.getAllBeans()) {
			Object parentBean = parent.bean;
			for (final PropertyDescriptor pd : getPropertyDescriptors(parentBean.getClass())) {
				PropertyEditor<?> editor = getPropertyEditor(pd, pd.getPropertyType(), parentBean, "");
				try {
					if (editor instanceof PropertyContainerEditor) {
						PropertyContainerEditor<?> containerEditor = (PropertyContainerEditor<?>) editor;
						editor.setValueFromObj(pd.getReadMethod().invoke(parentBean, new Object[0]));
						List<Object> beans = containerEditor.getSubBeans();
						if (beans == null)
							continue;
						if (beans.contains(bean))
							return true;
					}
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {}
			}
		}
		return false;
	}

	static {
		BeanEditor.addStrongRefBeanRenameListener((oldBeanDesc, beanDesc) -> {
			Object beanToRename = beanDesc.bean;
			String baseName = defaultDir + BeanDesc.getSimpleBaseDescriptor(beanToRename.getClass());
			File srcFile = new File(baseName + oldBeanDesc + SERIALIZE_EXT);
			File dstFile = new File(baseName + beanDesc.name + SERIALIZE_EXT);
			srcFile.renameTo(dstFile);
			for (BeanDesc<?> parent : BeanEditor.getAllBeans()) {
				Object parentBean = parent.bean;
				for (final PropertyDescriptor pd : getPropertyDescriptors(parentBean.getClass())) {
					PropertyEditor<?> editor = getPropertyEditor(pd, pd.getPropertyType(), parentBean, "");
					try {
						if (editor instanceof PropertyContainerEditor) {
							PropertyContainerEditor<?> containerEditor = (PropertyContainerEditor<?>) editor;
							editor.setValueFromObj(pd.getReadMethod().invoke(parentBean, new Object[0]));
							List<Object> beans = containerEditor.getSubBeans();
							if (beans != null && beans.contains(beanToRename))
								new BeanManager(parentBean, beanDesc.local).save(false);
						}
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {}
				}
			}
		});
		BeanEditor.addStrongRefBeanCreatedListener(beanDesc -> new BeanManager(beanDesc.bean, beanDesc.local).save(false));
		BeanEditor.addStrongRefBeanUnregisterListener((beanDesc, delete) -> {
			if (delete)
				new File(BeanManager.defaultDir + beanDesc.getSimpleDescriptor() + BeanManager.SERIALIZE_EXT).delete();
		});
	}

	/** Switch the default bean location. Each bean will be loaded and saved from this default location except if an other location is specified when saving or loading a bean. This method does not
	 * changed the location of existing beans.
	 * @param defaultBeanLocation the default bean location */
	public static void switchDefaultBeanLocation(String defaultBeanLocation) {
		defaultDir = defaultBeanLocation;
		File opDir = new File(defaultDir);
		if (!opDir.exists())
			opDir.mkdirs();
	}

	public void forEachExpendedSubView(TriConsumer<String, BeanManager, GridPane> consumer) {
		if (this.mapLabelObject == null)
			return;
		this.mapLabelObject.values().stream().filter(ovd -> ovd.beanManager != null).forEach(ovd -> consumer.accept(ovd.name, ovd.beanManager, (GridPane) ovd.editorPanel));
	}

	public static String getPropertyNameFromLabelName(String labelText) {
		return labelText.substring(0, labelText.length() - PROPERTY_SUFFIX.length());
	}

	@Override
	public String toString() {
		return "BeanManager [Bean=" + BeanEditor.getBeanDesc(this.bean).name + "]";
	}

	public static Class<?> getClassFromDescriptor(String descriptor) throws ClassNotFoundException {
		int ioms = descriptor.indexOf(MODULE_SEPARATOR);
		return ioms == -1 ? Class.forName(descriptor) : Class.forName(descriptor.substring(ioms + 1), true, REGISTER_MODULES.get(descriptor.substring(0, ioms)));
	}

	public static String getDescriptorFromClass(Class<?> type) {
		ClassLoader cl = type.getClassLoader();
		if (cl == null || cl.equals(ClassLoader.getSystemClassLoader()))
			return type.getName();
		String moduleName = type.getModule().getName();
		return REGISTER_MODULES.get(moduleName) == null ? type.getName() : moduleName + MODULE_SEPARATOR + type.getName();
	}

	public static String getDescriptorFromClassWithSimpleName(Class<?> type) {
		ClassLoader cl = type.getClassLoader();
		if (cl == null || cl.equals(ClassLoader.getSystemClassLoader()))
			return type.getSimpleName();
		String moduleName = type.getModule().getName();
		return REGISTER_MODULES.get(moduleName) == null ? type.getSimpleName() : moduleName + MODULE_SEPARATOR + type.getSimpleName();
	}

	public static void registerModule(Module module, ClassLoader classLoader) {
		REGISTER_MODULES.put(module.getName(), classLoader);
		fireModuleLoaded(module);
	}

	public static void unregisterModule(Module module) {
		REGISTER_MODULES.remove(module.getName());
		purgeModuleFromBeans(module, false);
		fireModuleUnLoaded(module);
	}

	private static void purgeModuleFromBeans(Module moduleToRemove, boolean delete) {
		for (BeanDesc<?> parent : BeanEditor.getAllBeans())
			if (parent.bean.getClass().getModule().equals(moduleToRemove))
				purgeBean(parent.bean, false, false);
			else {
				Object parentBean = parent.bean;
				boolean propertyRemoved = false;
				for (final PropertyDescriptor pd : getPropertyDescriptors(parentBean.getClass())) {
					PropertyEditor<?> editor = getPropertyEditor(pd, pd.getPropertyType(), parentBean, "");
					try {
						if (editor instanceof PropertyContainerEditor) {
							PropertyContainerEditor<?> containerEditor = (PropertyContainerEditor<?>) editor;
							editor.setValueFromObj(pd.getReadMethod().invoke(parentBean, new Object[0]));
							List<Object> beans = containerEditor.getSubBeans();
							if (beans != null)
								for (Object bean : beans) {
									Module module = bean.getClass().getModule();
									if (module != null && module.equals(moduleToRemove)) {
										containerEditor.removeSubBeans(bean);
										propertyRemoved = true;
									}
								}
							if (propertyRemoved)
								pd.getWriteMethod().invoke(parentBean, containerEditor.getValue());
						}
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						e.printStackTrace();
					}
				}
				if (propertyRemoved)
					new BeanManager(parent.bean, parent.local).save(false);
			}
	}

	public static boolean isModuleRegistered(String moduleName) {
		return REGISTER_MODULES.get(moduleName) != null;
	}

	public static boolean isInternModule(String moduleName) {
		return false;
	}

	/** Invoked when a module is loaded. A strong reference to the listener is added to the listener list. Thus, if the observer is garbage collected, the function is not automatically removed.
	 * @param listener The listener to register */
	public static void addStrongRefModuleLoadedListener(LoadModuleListener listener) {
		MODULE_LOADED_LISTENERS.addStrongRef(listener);
	}

	/** Invoked when a module is loaded. A strong reference to the listener is added to the listener list if the listener is not already stored. Thus, if the observer is garbage collected, the
	 * function is not automatically removed.
	 * @param listener The listener to register */
	public static void addStrongRefModuleLoadedListenerIfAbsent(LoadModuleListener listener) {
		MODULE_LOADED_LISTENERS.addStrongRefIfAbsent(listener);
	}

	/** Invoked when a module is loaded. A weak reference to the listener is added to the listener list. Thus, if the observer is garbage collected, the function is automatically removed.
	 * @param listener The listener to register */
	public static void addWeakRefModuleLoadedListener(LoadModuleListener listener) {
		MODULE_LOADED_LISTENERS.addWeakRef(listener);
	}

	/** Invoked when a module is loaded. A weak reference to the listener is added to the listener list if the listener is not already stored. Thus, if the observer is garbage collected, the function
	 * is automatically removed.
	 * @param listener The listener to register */
	public static void addWeakRefModuleLoadedListenerIfAbsent(LoadModuleListener listener) {
		MODULE_LOADED_LISTENERS.addWeakRefIfAbsent(listener);
	}

	/** Remove the strong reference to a function to be called when a module is loaded.
	 * @param listener The listener to remove */
	public static void removeStrongRefModuleLoadedListener(LoadModuleListener listener) {
		MODULE_LOADED_LISTENERS.removeStrongRef(listener);
	}

	/** Remove the weak reference to a function to be called when a module is loaded.
	 * @param listener The listener to remove */
	public static void removeWeakRefModuleLoadedListener(LoadModuleListener listener) {
		MODULE_LOADED_LISTENERS.removeWeakRef(listener);
	}

	/** Fire load module event.
	 * @param module The loaded module. */
	private static void fireModuleLoaded(Module module) {
		MODULE_LOADED_LISTENERS.forEach(l -> l.loaded(module));
	}

	/** Fire unload module event.
	 * @param module The loaded module. */
	private static void fireModuleUnLoaded(Module module) {
		MODULE_LOADED_LISTENERS.forEach(l -> l.unloaded(module));
	}
}

class BeanManagerListener {
	public final BeanManager beanManager;
	public PropertyChangeListener propertyChangeListener;

	public BeanManagerListener(BeanManager beanManager) {
		this.beanManager = beanManager;
	}

	@Override
	public String toString() {
		return "BeanManagerListener [Bean=" + BeanEditor.getBeanDesc(this.beanManager.bean).name + "]";
	}
}

class EditableProperty {
	public final PropertyDescriptor pd;
	public final PropertyEditor<?> pe;
	public final int id;
	public final boolean ro;
	public Region ce;

	public EditableProperty(PropertyDescriptor pd, PropertyEditor<?> pe, int id, boolean ro) {
		this.pd = pd;
		this.pe = pe;
		this.id = id;
		this.ro = ro;
	}

	@Override
	public String toString() {
		return this.pd.getName();
	}
}

class ObjectViewDesc {
	public final String name;
	public final BeanEditor<?> objEditor;
	public final int gridy;
	public BeanManager beanManager;
	public Region editorPanel;

	public ObjectViewDesc(String name, BeanEditor<?> objEditor, int gridy) {
		this.name = name;
		this.objEditor = objEditor;
		this.gridy = gridy;
	}
}
