/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.testbean;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class SliderWithLabeledThumb extends Application {
	private final Label label = new Label();

	@Override
	public void start(Stage primaryStage) {
		Slider slider = new Slider();
		StackPane root = new StackPane(slider);
		root.setPadding(new Insets(20));

		Scene scene = new Scene(root);

		slider.applyCss();
		slider.layout();
		Pane thumb = (Pane) slider.lookup(".thumb");
		this.label.setPadding(new Insets(0));

		slider.setOnMousePressed(e -> {
			this.label.textProperty().bind(slider.valueProperty().asString("%.1f"));
			thumb.getChildren().add(this.label);
		});
		slider.setOnMouseReleased(e -> {
			thumb.getChildren().remove(this.label);
		});

		primaryStage.setScene(scene);
		primaryStage.show();

	}

	public static void main(String[] args) {
		launch(args);
	}
}