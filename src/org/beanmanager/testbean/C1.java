/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.testbean;

import java.io.Serializable;

import org.beanmanager.editors.primitive.number.NumberInfo;

public class C1 implements Serializable {
	private static final long serialVersionUID = 1L;
	@NumberInfo(min = 0)
	// @DynamincNumberInfo(minMethod = "getMinC1Val")
	private int c1Val = 5;
	private C2 c2 = new C2();

	public int getC1Val() {
		return this.c1Val;
	}

	public C2 getC2() {
		return this.c2;
	}

	public void setC1Val(int c1Val) {
		this.c1Val = c1Val;
	}

	public void setC2(C2 c2) {
		this.c2 = c2;
	}
}
