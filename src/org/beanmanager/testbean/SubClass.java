/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.testbean;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.Serializable;

public class SubClass implements Serializable {
	private static final long serialVersionUID = 1L;
	private Dimension dim = new Dimension(50, 20);
	private C1 c1 = new C1();

	public void birth() {}

	public void death() {}

	public C1 getC1() {
		return this.c1;
	}

	public Dimension getDim() {
		return this.dim;
	}

	public BufferedImage process(BufferedImage raster) {
		return null;
	}

	public void setC1(C1 c1) {
		this.c1 = c1;
	}

	public void setDim(Dimension dim) {
		this.dim = dim;
	}
}
