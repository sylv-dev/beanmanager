/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.testbean;

import java.awt.Color;

public class MoutonBlanc extends AbstractMouton {
	private static final long serialVersionUID = 1L;
	private Color color = new Color(1, 1, 1);
	private boolean isRaciste;

	public Color getColor() {
		return this.color;
	}

	public boolean isRaciste() {
		return this.isRaciste;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public void setRaciste(boolean isRaciste) {
		this.isRaciste = isRaciste;
	}
}
