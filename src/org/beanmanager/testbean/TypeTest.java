/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.testbean;

import java.io.File;
import java.net.InetSocketAddress;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.BitSet;

import javax.vecmath.GMatrix;
import javax.vecmath.GVector;
import javax.vecmath.Matrix3d;
import javax.vecmath.Point2i;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3d;

import org.beanmanager.editors.DynamicChoiceBox;
import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.basic.BitSetInfo;
import org.beanmanager.editors.multiNumber.matrix.MatrixInfo;
import org.beanmanager.editors.multiNumber.vector.VectorInfo;
import org.beanmanager.editors.primitive.number.ControlType;
import org.beanmanager.editors.primitive.number.NumberInfo;
import org.beanmanager.struct.Selection;
import org.beanmanager.struct.TreeRoot;

import javafx.scene.paint.Color;

enum Direction {
	NORTH, SOUTH, RIGHT, LEFT
}

public class TypeTest {
	private Path path;

	public Path getPath() {
		return this.path;
	}

	public void setPath(Path path) {
		this.path = path;
	}

	private Character chara;
	// private Runnable run;
	private TreeRoot<Color> tree;
	private Carotte carotteOfTheFirstRabbit = new Carotte();
	public int patatepublic = 5;
	private int corrVar = 31;
	private Integer horizon;
	private boolean cool;
	private byte patate;
	private double ours;
	@NumberInfo(min = -7, max = 70000, controlType = ControlType.TEXTFIELD_AND_SLIDER)
	@DynamicChoiceBox(useSwingWorker = false, possibleChoicesMethod = "patate")
	private double arbre;
	@PropertyInfo(info = "information", unit = "m/s", index = 2)
	private Float dauphin;
	// private Direction dir;
	@PropertyInfo(index = 3)
	private float toto;
	private String cochon;
	private File vache;
	private short abeille;
	// @FieldInfo(index = -10)
	// private Projecteur projecteur;
	private InetSocketAddress add;
	private String[] tab;
	@PropertyInfo(index = 20)
	private int[][] tab2D;
	private Color color;
	private Point2i point;
	private Point3d point3d;
	@BitSetInfo(minSize = 10, maxSize = 10)
	private BitSet bitset;
	@BitSetInfo(minSize = 0, maxSize = 64)
	private BitSet bitsetHex;
	// @FieldSetInfo(minSize = 10, maxSize = 10)
	// private CanTrame canTrame;
	@DynamicChoiceBox(possibleChoicesMethod = "getAvailableChannel")
	private String channel;
	@DynamicChoiceBox(possibleChoicesMethod = "getSeletionItems")
	private Selection<String> selection;
	private Vector3d vec;
	@VectorInfo(size = 7)
	private GVector gVec;
	private Matrix3d mat;
	@MatrixInfo(nCol = 10, nRow = 8)
	private GMatrix gmat;

	private LocalTime time;
	private LocalDateTime dateTime;
	private LocalDate date;

	int cpt = 0;

	public Double[] patate() {
		return new Double[] { 8.0, 7.8 };
	}

	// public void setRun(Runnable run) {
	// this.run = run;
	// }

	public Character getChara() {
		return this.chara;
	}

	public void setChara(Character chara) {
		this.chara = chara;
	}

	public Point2i getPoint() {
		return this.point;
	}

	public void setPoint(Point2i point) {
		this.point = point;
	}

	public Point3d getPoint3d() {
		return this.point3d;
	}

	public void setPoint3d(Point3d point3d) {
		this.point3d = point3d;
	}

	public int[][] getTab2D() {
		return this.tab2D;
	}

	public void setTab2D(int[][] tab2d) {
		this.tab2D = tab2d;
	}

	public String[] getTab() {
		return this.tab;
	}

	public void setTab(String[] tab) {
		this.tab = tab;
	}

	public InetSocketAddress getAdd() {
		return this.add;
	}

	public void setAdd(InetSocketAddress add) {
		this.add = add;
	}

	public Carotte getCarotteOfTheFirstRabbit() {
		return this.carotteOfTheFirstRabbit;
	}

	public void setCarotteOfTheFirstRabbit(Carotte carotteOfTheFirstRabbit) {
		this.carotteOfTheFirstRabbit = carotteOfTheFirstRabbit;
	}

	// public Projecteur getProjecteur() {
	// return projecteur;
	// }
	//
	// public void setProjecteur(Projecteur projecteur) {
	// this.projecteur = projecteur;
	// }

	public short getAbeille() {
		return this.abeille;
	}

	public void setAbeille(short abeille) {
		this.abeille = abeille;
	}

	public long getAnaconda() {
		return this.anaconda;
	}

	public void setAnaconda(long anaconda) {
		this.anaconda = anaconda;
	}

	private long anaconda;

	public File getVache() {
		return this.vache;
	}

	public void setVache(File vache) {
		this.vache = vache;
	}

	public String getCochon() {
		return this.cochon;
	}

	public void setCochon(String cochon) {
		this.cochon = cochon;
	}

	public float getToto() {
		return this.toto;
	}

	public void setToto(float toto) {
		this.toto = toto;
	}

	// public Direction getDir() {
	// return dir;
	// }
	//
	// public void setDir(Direction dir) {
	// this.dir = dir;
	// }

	public Float getDauphin() {
		return this.dauphin;
	}

	public void setDauphin(Float dauphin) {
		this.dauphin = dauphin;
	}

	// private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
	//
	// public void addPropertyChangeListener(PropertyChangeListener listener) {
	// pcs.addPropertyChangeListener(listener);
	// }
	//
	// public void removePropertyChangeListener(PropertyChangeListener listener) {
	// pcs.removePropertyChangeListener(listener);
	// }

	public double getOurs() {
		return this.ours;
	}

	public void setOurs(double ours) {
		// double oldValue = this.ours;
		this.ours = ours;
		// pcs.firePropertyChange("Ours", oldValue, ours);
	}

	public byte getPatate() {
		return this.patate;
	}

	public void setPatate(byte patate) {
		this.patate = patate;
		setOurs(patate);
	}

	public boolean isCool() {
		return this.cool;
	}

	public void setCool(boolean cool) {
		this.cool = cool;
	}

	public Integer getHorizon() {
		return this.horizon;
	}

	public void setHorizon(Integer horizon) {
		this.horizon = horizon;
	}

	public void birth() {}

	public void death() {}

	public int getOutputTestInt() {
		return this.cpt + 20;
	}

	public byte getOutputTestByte() {
		return (byte) (this.cpt + 10);
	}

	public double getOutputTestDouble() {
		return this.cpt + 20.56;
	}

	public double[] getOutputTestArray() {
		return new double[] { this.cpt + 10.56, this.cpt + 20.56 };
	}

	public boolean getOutputTestBoolean() {
		return this.cpt % 2 == 0;
	}

	public Color getOutputTestColor() {
		return new Color(1, this.cpt / 255.0, 0, 1);
	}

	public File getOutputTestFile() {
		return new File("fichier" + this.cpt + ".png");
	}

	public Point3f getOutputTestPoint3f() {
		return new Point3f(0, this.cpt, this.cpt * 2);
	}

	public void fireFileChanged(Path eventPath) {}

	public int getCorrVar() {
		return this.corrVar;
	}

	public void setCorrVar(int corrVar) {
		this.corrVar = corrVar;
	}

	public Color getColor() {
		return this.color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public double getArbre() {
		return this.arbre;
	}

	public void setArbre(double arbre) {
		this.arbre = arbre;
	}

	public BitSet getBitset() {
		return this.bitset;
	}

	public void setBitset(BitSet bitset) {
		this.bitset = bitset;
	}

	public BitSet getBitsetHex() {
		return this.bitsetHex;
	}

	public void setBitsetHex(BitSet bitsetHex) {
		this.bitsetHex = bitsetHex;
	}

	// public CanTrame getCanTrame() {
	// return canTrame;
	// }
	//
	// public void setCanTrame(CanTrame canTrame) {
	// this.canTrame = canTrame;
	// }

	public String getChannel() {
		return this.channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String[] getAvailableChannel() {
		return new String[] { "c1", "c2" };
	}

	public String[] getSeletionItems() {
		return new String[] { "patate", "cool" };
	}

	public Selection<String> getSelection() {
		return this.selection;
	}

	public void setSelection(Selection<String> selection) {
		this.selection = selection;
	}

	public TreeRoot<Color> getTree() {
		return this.tree;
	}

	public void setTree(TreeRoot<Color> tree) {
		this.tree = tree;
	}

	public Vector3d getVec() {
		return this.vec;
	}

	public void setVec(Vector3d vec) {
		this.vec = vec;
	}

	public Matrix3d getMat() {
		return this.mat;
	}

	public void setMat(Matrix3d mat) {
		this.mat = mat;
	}

	public GVector getgVec() {
		return this.gVec;
	}

	public void setgVec(GVector gVec) {
		this.gVec = gVec;
	}

	public GMatrix getGmat() {
		return this.gmat;
	}

	public void setGmat(GMatrix gmat) {
		this.gmat = gmat;
	}

	public LocalDate getDate() {
		return this.date;
	}

	public LocalDateTime getDateTime() {
		return this.dateTime;
	}

	public Runnable getRun() {
		return () -> System.out.println("ok");
	}

	// public void setRun(Runnable run) {
	// this.run = run;
	// }

	public LocalTime getTime() {
		return this.time;
	}

	public Patate process(String cool) {
		return new Patate("j'aime les patates " + this.cpt, 5 + this.cpt++);
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public void setDateTime(LocalDateTime dateTime) {
		this.dateTime = dateTime;
	}

	public void setTime(LocalTime time) {
		this.time = time;
	}
}
