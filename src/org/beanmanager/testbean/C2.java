/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.testbean;

import java.io.Serializable;

public class C2 implements Serializable {
	private static final long serialVersionUID = 1L;

	private int c2Val = 4;

	public int getC2Val() {
		return this.c2Val;
	}

	public void setC2Val(int c2Val) {
		this.c2Val = c2Val;
	}
}
