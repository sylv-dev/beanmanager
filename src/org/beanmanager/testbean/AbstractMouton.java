/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.testbean;

import java.io.Serializable;

public abstract class AbstractMouton implements Serializable {
	private static final long serialVersionUID = 1L;
	private boolean isCool;

	public boolean isCool() {
		return this.isCool;
	}

	public void setCool(boolean isCool) {
		this.isCool = isCool;
	}
}
