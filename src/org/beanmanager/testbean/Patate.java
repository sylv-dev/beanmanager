/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.testbean;

import java.io.Serializable;

public class Patate implements Serializable {
	private static final long serialVersionUID = 1L;
	private int rightTail;
	private String text;

	public Patate(String text, int rightTail) {
		this.text = text;
		this.rightTail = rightTail;
	}

	public int getRightTail() {
		return this.rightTail;
	}

	public String getText() {
		return this.text;
	}

	public void setRightTail(int rightTail) {
		this.rightTail = rightTail;
	}

	public void setText(String text) {
		this.text = text;
	}
}
