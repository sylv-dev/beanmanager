/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager;

import java.io.File;
import java.util.Objects;

public class BeanDesc<T> {
	public static final Character SEPARATOR = '-';
	public final T bean;
	public final String name;
	public final String local;

	public BeanDesc(T bean, String name, String local) {
		Objects.requireNonNull(local);
		if (!local.isBlank() && !local.endsWith(File.separator))
			local += File.separator;
		this.bean = bean;
		this.name = name;
		this.local = local;
	}

	@Override
	public String toString() {
		return this.name;
	}

	public String getSimpleDescriptor() {
		return BeanManager.getDescriptorFromClassWithSimpleName(this.bean.getClass()) + SEPARATOR + this.name;
	}

	public String getCompleteDescriptor() {
		return BeanManager.getDescriptorFromClass(this.bean.getClass()) + SEPARATOR + this.name;
	}

	public static String getSimpleBaseDescriptor(Class<? extends Object> beanClass) {
		return BeanManager.getDescriptorFromClassWithSimpleName(beanClass) + SEPARATOR;
	}
}
