/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;
import java.util.function.BiConsumer;

public class EditorMap {
	private final HashMap<Class<?>, EditorDescriptor<?, ?>> map = new HashMap<>();

	public void forEach(BiConsumer<? super Class<?>, ? super EditorDescriptor<?, ?>> consumer) {
		this.map.forEach(consumer);
	}

	@SuppressWarnings("unchecked")
	public <T, U extends PropertyEditor<T>> EditorDescriptor<T, U> get(Class<T> key) {
		return (EditorDescriptor<T, U>) this.map.get(key);
	}

	public Set<Class<?>> keyset() {
		return this.map.keySet();
	}

	public Collection<EditorDescriptor<?, ?>> values() {
		return this.map.values();
	}

	public <T, U extends PropertyEditor<T>> void put(Class<T> key, EditorDescriptor<T, U> editor) {
		this.map.put(key, editor);
	}
}
