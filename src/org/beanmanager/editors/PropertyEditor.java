/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Arrays;

import javax.swing.event.EventListenerList;

import org.beanmanager.editors.basic.StringEditor;
import org.beanmanager.tools.FxUtils;

import javafx.collections.FXCollections;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;

public abstract class PropertyEditor<T> {
	private static final int EDITOR = 1;
	private static final int NO_SELECTION_EDITOR = 2;
	private static final int VIEW = 3;

	public static final String GROUP_SEPARATOR = ",";
	public static final String PROPERTY_SEPARATOR = ";";
	public static final String OPEN_GROUP = "{";
	public static final String CLOSE_GROUP = "}";

	private final EventListenerList listeners = new EventListenerList();
	private ComboBox<Object> comboBox;
	private T value;
	private boolean nullable = true;
	private BorderPane editorBox;
	protected Region view;
	private T[] possibilities;
	private Region customEditor; // TODO Remove et faire getter Hbox 0
	private int hmiMode = 0;

	public T getValue() {
		return this.value;
	}

	public void setValue(T value) {
		if (this.possibilities == null || Arrays.asList(this.possibilities).contains(value))
			if (setValueWithoutEvent(value))
				firePropertyChangeListener();
	}

	@SuppressWarnings("unchecked")
	public void setValueFromObj(Object value) {
		setValue((T) value);
	}

	@SuppressWarnings("unchecked")
	public void setValueFromObjWithoutEvent(Object value) {
		setValueWithoutEvent((T) value);
	}

	public boolean setValueWithoutEvent(T value) {
		// Pas de equals!!! sinon deux peux pas set des objets
		if (this.value == value || this.value != null && Number.class.isAssignableFrom(this.value.getClass()) && this.value.equals(value))
			return false;
		this.value = value;
		if (this.hmiMode != 0)
			FxUtils.runLaterIfNeeded(() -> updateGUI());
		return true;
	}

	public abstract String getAsText();

	public abstract void setAsText(String text);

	public T readValue(DataInput raf) throws IOException {
		String text = StringEditor.readString(raf);
		try {
			setAsText(text);
		} catch (StringIndexOutOfBoundsException e) {
			e.printStackTrace();
		}
		return getValue();
	}

	public void writeValue(DataOutput raf, T value) throws IOException {
		setValue(value);
		StringEditor.writeString(raf, getAsText());
	}

	@SuppressWarnings("unchecked")
	public void writeValueFromObj(DataOutput raf, Object value) throws IOException {
		writeValue(raf, (T) value);
	}

	public final BorderPane getEditor() {
		if (this.hmiMode == NO_SELECTION_EDITOR || this.hmiMode == VIEW)
			throw new IllegalArgumentException("The Editor HMI was already initialized but not in editor mode");
		this.hmiMode = EDITOR;
		if (this.editorBox == null) {
			this.editorBox = new BorderPane();
			resetEditor();
		} else
			updateCustomEditor();
		return this.editorBox;
	}

	public Region getNoSelectionEditor() {
		if (this.hmiMode == EDITOR || this.hmiMode == VIEW)
			throw new IllegalArgumentException("The Editor HMI was already initialized but not in no selection mode");
		this.hmiMode = NO_SELECTION_EDITOR;
		if (this.customEditor == null)
			this.customEditor = getCustomEditor();
		return this.customEditor;
	}

	public final Region getView() {
		if (this.hmiMode == EDITOR || this.hmiMode == NO_SELECTION_EDITOR)
			throw new IllegalArgumentException("The Editor HMI was already initialized but not in view mode");
		this.hmiMode = VIEW;
		if (this.view == null)
			this.view = hasCustomView() ? getCustomView() : new Label(getAsText());
		else if (hasCustomView())
			updateCustomView();
		else
			((Label) this.view).setText(getAsText());
		return this.view;
	}

	protected void resetEditor() {
		if (this.editorBox != null)
			if (this.possibilities == null) {
				this.comboBox = null;
				if (this.customEditor == null)
					this.customEditor = getCustomEditor();
				this.editorBox.setCenter(this.customEditor);
				HBox.setHgrow(this.customEditor, Priority.ALWAYS);
			} else {
				this.customEditor = null;
				this.comboBox = createSelectionComboBox(this.possibilities);
				this.comboBox.setMaxWidth(Double.MAX_VALUE);
				HBox.setHgrow(this.comboBox, Priority.ALWAYS);
				this.editorBox.setCenter(this.comboBox);
			}
	}

	protected Region customEditor() {
		return this.customEditor;
	}

	protected boolean nullable() {
		return this.nullable;
	}

	protected ComboBox<Object> createSelectionComboBox(T[] possibilities) {
		ComboBox<Object> comboBox = new ComboBox<>(FXCollections.observableArrayList(possibilities));
		HBox.setHgrow(comboBox, Priority.ALWAYS);
		comboBox.setMaxWidth(Double.MAX_VALUE);
		comboBox.setValue(getValue());
		comboBox.setOnScroll(e -> {
			if (e.getDeltaY() != 0) {
				int index = comboBox.getSelectionModel().getSelectedIndex() + (e.getDeltaY() > 0 ? -1 : 1);
				comboBox.getSelectionModel().select(index < 0 ? comboBox.getItems().size() - 1 : index >= comboBox.getItems().size() ? 0 : index);
			}
			e.consume();
		});
		comboBox.valueProperty().addListener((ov, oldValue, newValue) -> setValueFromObj(newValue));
		comboBox.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.ENTER) {
				FxUtils.traverse();
				e.consume();
			}
		});
		return comboBox;
	}

	protected Region getCustomEditor() {
		return null;
	}

	protected Region getCustomView() {
		return null;
	}

	public boolean hasCustomEditor() {
		return false;
	}

	public boolean hasCustomView() {
		return false;
	}

	protected void updateCustomEditor() {}

	protected void updateCustomView() {}

	public final void updateGUI() {
		if (this.customEditor != null)
			updateCustomEditor();
		if (this.comboBox != null)
			updateComboBox(this.comboBox);
		if (this.view != null)
			if (hasCustomView())
				updateCustomView();
			else
				((Label) this.view).setText(getAsText());
	}

	protected void updateComboBox(ComboBox<Object> comboBox) {
		comboBox.setValue(getValue());
	}

	/** Specify if the visual editor can grow vertically or have a fix size
	 * @return true if the editor can grow vertically */
	public boolean canGrow() {
		return false;
	}

	/** Specify if the visual editor can update its size dynamically or not
	 * @return true if the visual editor can update its size dynamically */
	public boolean isFixedControlSized() {
		return false;
	}

	/** Get the description of the editor. Return null by default.
	 * @return the description of the editor */
	public String getDescription() {
		return null;
	}

	public int getPitch() {
		return -1;
	}

	/** Specify if the {@link #getAsText() getAsText} method can return forbidden character in group editor as ArrayEditor. Forbidden character are: ",", ";", "{", "}"
	 * @return true if the editor can grow vertically */
	public boolean canContainForbiddenCharacter() {
		return false;
	}

	public boolean isNullable() {
		return this.nullable;
	}

	public void setNullable(boolean nullable) {
		this.nullable = nullable;
	}

	public T[] getPossibilities() {
		return this.possibilities;
	}

	public void setPossibilities(T[] possibilities) {
		if (this.hmiMode == NO_SELECTION_EDITOR)
			throw new IllegalArgumentException("The Editor HMI was already initialized in no possibilities mode, cannot set possibilities");
		boolean needToChangeEditor = possibilities == null ^ this.possibilities == null;
		this.possibilities = possibilities;
		if (this.possibilities != null && !Arrays.asList(this.possibilities).contains(this.value))
			setValue(this.possibilities.length == 0 ? null : this.possibilities[0]);
		if (needToChangeEditor && this.editorBox != null)
			resetEditor();
		else if (this.comboBox != null)
			this.comboBox.setItems(FXCollections.observableArrayList(this.possibilities));
	}

	@SuppressWarnings("unchecked")
	public void setPossibilitiesFromObjects(Object[] possibilities) {
		setPossibilities((T[]) possibilities);
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		this.listeners.add(PropertyChangeListener.class, listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		this.listeners.remove(PropertyChangeListener.class, listener);
	}

	protected void firePropertyChangeListener() {
		for (PropertyChangeListener listener : this.listeners.getListeners(PropertyChangeListener.class))
			listener.propertyChange();
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + ", Value: " + this.value;
	};
}