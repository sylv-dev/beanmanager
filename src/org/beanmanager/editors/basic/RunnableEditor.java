/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.basic;

import org.beanmanager.editors.PropertyEditor;

import javafx.scene.control.Button;
import javafx.scene.layout.Region;

public class RunnableEditor extends PropertyEditor<Runnable> {

	@Override
	public String getAsText() {
		return null;
	}

	@Override
	protected Region getCustomEditor() {
		Button button = new Button("Run");
		button.setOnAction(e -> {
			Runnable runnable = getValue();
			if (runnable != null)
				runnable.run();
		});
		button.setMaxWidth(Double.MAX_VALUE);
		button.setDisable(getValue() == null);
		return button;
	}

	@Override
	public boolean hasCustomEditor() {
		return true;
	}

	@Override
	public void setAsText(String text) {}

	@Override
	public void updateCustomEditor() {
		((Button) customEditor()).setDisable(getValue() == null);
	}
}
