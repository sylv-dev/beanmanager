/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.basic;

import java.io.File;
import java.net.InetSocketAddress;
import java.util.ArrayList;

import org.beanmanager.editors.LocalisedEditor;
import org.beanmanager.editors.PropertyEditor;
import org.beanmanager.tools.FxUtils;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public abstract class AbstractPathEditor<T> extends PropertyEditor<T> implements LocalisedEditor {
	private StringEditor se;
	private String[] filters;
	private FileChooser fileChooser;
	private DirectoryChooser directoryChooser;
	private boolean isOnLocalHost = true;
	private boolean isDirectory;

	public AbstractPathEditor() {}

	public AbstractPathEditor(boolean isDirectory) {
		this.isDirectory = isDirectory;
	}

	protected abstract File getValueAsFile();

	protected abstract void setValueAsFile(File value);

	@Override
	protected Region getCustomEditor() {
		this.se = new StringEditor() {
			@Override
			protected void updateStyle() {
				TextField textField = (TextField) customEditor();
				textField.setStyle("-fx-text-fill: " + (getValue() == null || !textField.getText().isBlank() && !new File(textField.getText()).exists() ? "red"
						: getValue() == null || textField.getText().equals(getValue()) ? "black" : "green") + ";");
			}
		};
		File currentFile = getValueAsFile();
		this.se.setValue(currentFile != null ? currentFile.getPath() : null);
		this.se.addPropertyChangeListener(() -> {
			if (this.se.getValue() != null)
				setValueAsFile(new File(this.se.getValue()));
		});
		Region cse = this.se.getNoSelectionEditor();
		// addPropertyChangeListener(() -> updateView());
		cse.setOnDragOver(event -> {
			Dragboard db = event.getDragboard();
			if (event.getGestureSource() != cse && db.hasString() || db.hasFiles() && db.getFiles().size() == 1)
				event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
			event.consume();
		});
		cse.setOnDragDropped(event -> {
			Dragboard db = event.getDragboard();
			boolean success = false;
			if (db.hasFiles())
				setValueAsFile(db.getFiles().get(0));
			else if (db.hasString()) {
				setValueAsFile(new File(db.getString()));
				success = true;
			}
			event.setDropCompleted(success);
			event.consume();
		});
		if (!this.isOnLocalHost)
			return cse;
		Button button = new Button("", new ImageView(new Image(getClass().getResourceAsStream("/open_folder.png"))));
		button.setOnAction(e -> {
			if (this.fileChooser == null && this.directoryChooser == null) {
				File file = getValueAsFile();
				if (this.isDirectory) {
					this.directoryChooser = new DirectoryChooser();
					this.directoryChooser.setTitle("Directory Chooser");
					if (file != null && file.exists())
						this.directoryChooser.setInitialDirectory(file.isDirectory() ? file : file.getParentFile());
					File selectedFile = this.directoryChooser.showDialog(null);
					if (selectedFile != null)
						setValueAsFile(selectedFile);
					this.directoryChooser = null;
				} else {
					this.fileChooser = new FileChooser();
					if (file != null && file.exists())
						this.fileChooser.setInitialDirectory(file.getParentFile());
					if (this.filters != null)
						this.fileChooser.getExtensionFilters().setAll(getExtensionFilters(this.filters));
					this.fileChooser.setTitle("File Chooser");
					File selectedFile = this.fileChooser.showOpenDialog(null);
					if (selectedFile != null)
						setValueAsFile(selectedFile);
					this.fileChooser = null;
				}
			}
		});
		button.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.ENTER) {
				FxUtils.traverse();
				e.consume();
			}
		});
		HBox hBox = new HBox(cse, button);
		hBox.setPrefWidth(350);
		HBox.setHgrow(cse, Priority.ALWAYS);
		return hBox;
	}

	public static ArrayList<ExtensionFilter> getExtensionFilters(String[] filters) {
		ArrayList<ExtensionFilter> extensionFilters = new ArrayList<>();
		for (int i = 0; i < filters.length; i++) {
			String filter = filters[i];
			if (!filter.isBlank()) {
				String[] elements = filter.split(" ");
				String[] extensions = new String[elements.length - 1];
				System.arraycopy(elements, 1, extensions, 0, extensions.length);
				for (int j = 0; j < extensions.length; j++)
					extensions[j] = "*." + extensions[j];
				extensionFilters.add(new ExtensionFilter(elements[0], extensions));
			}
		}
		return extensionFilters;
	}

	@Override
	public void updateCustomEditor() {
		String path = getValueAsFile().getPath();
		if (!path.equals(this.se.getValue()))
			this.se.setValue(path);
	}

	@Override
	public boolean hasCustomEditor() {
		return true;
	}

	public void isDirectory(boolean isDirectory) {
		this.isDirectory = isDirectory;
	}

	public void setDirectory(boolean isDirectory) {
		this.isDirectory = isDirectory;
	}

	public void setFilters(String[] filters) {
		for (String filter : filters)
			if (!filter.isBlank() && filter.split(" ").length < 2)
				throw new IllegalArgumentException("Each filter must follow the pattern \"xxx vvv...\" where xxx represents the descrition and vvv the extensions");
		this.filters = filters;
	}

	@Override
	public boolean isFixedControlSized() {
		return true;
	}

	@Override
	public boolean canContainForbiddenCharacter() {
		return true;
	}

	@Override
	public void setLocale(InetSocketAddress inetSocketAdress) {
		this.isOnLocalHost = inetSocketAdress == null || inetSocketAdress.getAddress().isAnyLocalAddress() || inetSocketAdress.getAddress().isLoopbackAddress();
	}

	@Override
	public String getDescription() {
		return "of " + (this.isDirectory ? "directory" : "file");
	}
}
