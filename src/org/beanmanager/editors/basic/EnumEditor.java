/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.basic;

import org.beanmanager.editors.PropertyEditor;
import org.beanmanager.ihmtest.FxTest;

import javafx.geometry.Pos;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.VBox;

public class EnumEditor<T extends Enum<T>> extends PropertyEditor<T> {
	private final Class<T> type;

	public static void main(String[] args) {
		new FxTest().launchIHM(args, s -> new VBox(new EnumEditor<>(Pos.class).getCustomEditor(), new EnumEditor<>(Pos.class).getCustomEditor()));
	}

	public EnumEditor(Class<T> type) {
		if (!type.isEnum())
			throw new IllegalArgumentException("The type parameter must be assignable  Enum");
		this.type = type;
	}

	@Override
	public String getAsText() {
		Enum<?> enumVal = getValue();
		if (enumVal == null)
			return null;
		return enumVal.name();
	}

	@Override
	protected ComboBox<Object> getCustomEditor() {
		return createSelectionComboBox(this.type.getEnumConstants());
	}

	@Override
	public boolean hasCustomEditor() {
		return true;
	}

	@Override
	public boolean isFixedControlSized() {
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void setAsText(String text) {
		for (Enum<T> object : this.type.getEnumConstants())
			if (object.name().equals(text)) {
				setValue((T) object);
				return;
			}
		setValue(null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void updateCustomEditor() {
		((ComboBox<Enum<T>>) customEditor()).setValue(getValue());
	}
}
