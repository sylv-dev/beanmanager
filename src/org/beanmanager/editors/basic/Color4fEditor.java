/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.basic;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.StringTokenizer;

import javax.vecmath.Color4f;

import org.beanmanager.editors.PropertyEditor;
import org.beanmanager.editors.primitive.number.ControlType;
import org.beanmanager.editors.primitive.number.FloatEditor;
import org.beanmanager.ihmtest.FxTest;
import org.beanmanager.tools.FxUtils;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class Color4fEditor extends PropertyEditor<Color4f> {
	public static void main(String[] args) {
		new FxTest().launchIHM(args, s -> new VBox(new ColorEditor().getEditor(), new ColorEditor().getEditor()));
	}

	private static final Color toFxColor(Color4f color) {
		return new Color(color.x, color.y, color.z, color.w);
	}

	private static final Color4f toVecMathColor(Color color) {
		return new Color4f((float) color.getRed(), (float) color.getGreen(), (float) color.getBlue(), (float) color.getOpacity());
	}

	private FloatEditor[] fe;

	private ColorPicker colorPicker;

	@Override
	public String getAsText() {
		Color4f value = getValue();
		return Double.toString(value.x) + " " + Double.toString(value.y) + " " + Double.toString(value.z) + " " + Double.toString(value.w);
	}

	@Override
	protected HBox getCustomEditor() {
		if (getValue() == null)
			setValue(new Color4f());
		this.fe = new FloatEditor[4];
		HBox hbox = new HBox();
		hbox.setStyle("-fx-background-color: white;" + "-fx-border-width: 1px;" + "-fx-border-color: #969696 #DCDCDC #DCDCDC #969696;");
		hbox.setAlignment(Pos.BOTTOM_CENTER);
		this.colorPicker = new ColorPicker();
		this.colorPicker.setMinWidth(120);
		this.colorPicker.setMinHeight(24);
		this.colorPicker.setValue(toFxColor(getValue()));
		this.colorPicker.setOnAction(e -> {
			setValue(toVecMathColor(this.colorPicker.getValue()));
			updateValue();
		});
		this.colorPicker.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.ENTER) {
				updateValue();
				FxUtils.traverse();
				e.consume();
			}
		});
		hbox.getChildren().add(this.colorPicker);
		hbox.setFillHeight(true);
		HBox.setHgrow(this.colorPicker, Priority.ALWAYS);
		for (int i = 0; i < 4; i++) {
			FloatEditor se = new FloatEditor((short) 0, (short) 255, ControlType.TEXTFIELD);
			se.setStyle("-fx-background-color: transparent;");
			TextField c = (TextField) ((HBox) se.getNoSelectionEditor()).getChildren().get(0);
			c.setPrefWidth(40);
			HBox.setHgrow(c, Priority.ALWAYS);
			c.setPadding(new Insets(0));
			c.setAlignment(Pos.CENTER);
			c.setMinWidth(30);
			hbox.getChildren().add(c);
			if (i != 3) {
				Label l = new Label(" ");
				l.setStyle("-fx-background-color: transparent");
				hbox.getChildren().add(l);
			}
			this.fe[i] = se;
		}
		updateValue();
		for (FloatEditor se : this.fe)
			se.addPropertyChangeListener(() -> setValue(new Color4f(((Number) this.fe[0].getValue()).floatValue(), ((Number) this.fe[1].getValue()).floatValue(),
					((Number) this.fe[2].getValue()).floatValue(), ((Number) this.fe[3].getValue()).floatValue())));
		addPropertyChangeListener(() -> {
			if (this.colorPicker != null)
				this.colorPicker.setValue(toFxColor(getValue()));
		});
		TextField tf = new TextField();
		tf.setStyle("-fx-background-color: transparent;");
		return hbox;
	}

	@Override
	public int getPitch() {
		return Float.BYTES * 4;
	}

	@Override
	public boolean hasCustomEditor() {
		return true;
	}

	@Override
	public boolean isFixedControlSized() {
		return true;
	}

	@Override
	public Color4f readValue(DataInput raf) throws IOException {
		return new Color4f(raf.readFloat(), raf.readFloat(), raf.readFloat(), raf.readFloat());
	}

	@Override
	public void setAsText(String text) {
		if (text == null || text.isEmpty())
			setValue(null);
		else {
			StringTokenizer st = new StringTokenizer(text, " ");
			setValue(new Color4f(Float.parseFloat(st.nextToken()), Float.parseFloat(st.nextToken()), Float.parseFloat(st.nextToken()), Float.parseFloat(st.nextToken())));
		}
	}

	@Override
	public void updateCustomEditor() {
		this.colorPicker.setValue(toFxColor(getValue()));
		updateValue();
	}

	private void updateValue() {
		Color4f color = getValue();
		this.fe[0].setValue(color.x);
		this.fe[1].setValue(color.y);
		this.fe[2].setValue(color.z);
		this.fe[3].setValue(color.w);
	}

	@Override
	public void writeValue(DataOutput raf, Color4f value) throws IOException {
		Color4f c = value;
		raf.writeFloat(c.x);
		raf.writeFloat(c.y);
		raf.writeFloat(c.z);
		raf.writeFloat(c.w);
	}
}
