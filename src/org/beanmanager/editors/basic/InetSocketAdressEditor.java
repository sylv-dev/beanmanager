/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.basic;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;

import org.beanmanager.editors.DynamicSizeEditor;
import org.beanmanager.editors.PropertyEditor;
import org.beanmanager.editors.primitive.number.IntegerEditor;
import org.beanmanager.editors.primitive.number.ShortEditor;
import org.beanmanager.ihmtest.FxTest;
import org.beanmanager.tools.FxUtils;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class InetSocketAdressEditor extends PropertyEditor<InetSocketAddress> implements DynamicSizeEditor {
	public static void main(String[] args) throws UnknownHostException {
		InetSocketAdressEditor e1 = new InetSocketAdressEditor();
		e1.setValue(new InetSocketAddress(InetAddress.getByAddress(new byte[] { 02, 12, 64, -98 }), 2564));
		new Thread(() -> {
			try {
				Thread.sleep(3000);
				Platform.runLater(() -> {
					try {
						e1.setValue(new InetSocketAddress(InetAddress.getByAddress(new byte[] { 52, 51, 25, 95 }), 15));
					} catch (UnknownHostException e) {
						e.printStackTrace();
					}
				});
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}
		}).start();

		new FxTest().launchIHM(args, s -> new VBox(e1.getEditor(), new InetSocketAdressEditor().getEditor(), new InetSocketAdressEditor().getEditor()));
	}

	private ShortEditor[] byteEditorIp;
	private StringEditor stringEditorHostName;
	private IntegerEditor integerEditorPort;
	private TextField[] customEditorIp;
	private ContextMenu contextMenu;
	private CheckMenuItem hostName;
	private CheckMenuItem ipV4;
	private CheckMenuItem ipV6;
	private CheckMenuItem wildCard;

	@Override
	public boolean canContainForbiddenCharacter() {
		return true;
	}

	@Override
	protected HBox getCustomEditor() {
		InetSocketAddress isa = getValue();
		if (isa == null) {
			isa = new InetSocketAddress(0);
			setValue(isa);
		}
		HBox isaBox = new HBox();
		isaBox.setStyle("-fx-background-color: white;" + "-fx-border-width: 1px;" + "-fx-border-color: #969696 #DCDCDC #DCDCDC #969696;");
		EventHandler<ActionEvent> eh = e -> {
			Object src = e.getSource();
			boolean sizeChanged = this.ipV6.isSelected();
			if (src == this.ipV4) {
				this.ipV4.setSelected(true);
				this.ipV6.setSelected(false);
				this.wildCard.setSelected(false);
				this.hostName.setSelected(false);
			} else if (src == this.ipV6) {
				this.ipV4.setSelected(false);
				this.ipV6.setSelected(true);
				this.wildCard.setSelected(false);
				this.hostName.setSelected(false);
				sizeChanged = true;
			} else if (src == this.wildCard) {
				this.wildCard.setSelected(true);
				this.ipV6.setSelected(false);
				this.ipV4.setSelected(false);
				this.hostName.setSelected(false);
			} else {
				this.hostName.setSelected(true);
				this.ipV6.setSelected(false);
				this.ipV4.setSelected(false);
				this.wildCard.setSelected(false);
			}
			updateIsaBox(isaBox, true);
			if (sizeChanged)
				fireSizeChanged();
		};
		this.contextMenu = new ContextMenu();
		this.wildCard = new CheckMenuItem("WildCard");
		this.wildCard.setOnAction(eh);
		this.ipV4 = new CheckMenuItem("IpV4");
		this.ipV4.setOnAction(eh);
		this.ipV6 = new CheckMenuItem("IpV6");
		this.ipV6.setOnAction(eh);
		this.hostName = new CheckMenuItem("HostName");
		this.hostName.setOnAction(eh);
		this.contextMenu.getItems().addAll(this.wildCard, this.ipV4, this.ipV6, this.hostName);
		InetAddress add = isa.getAddress();
		if (add == null)
			this.hostName.setSelected(true);
		else {
			byte[] bytes = add.getAddress();
			boolean noFiltering = true;
			for (byte b : bytes)
				if (b != 0) {
					noFiltering = false;
					break;
				}
			(noFiltering ? this.wildCard : bytes.length == 4 ? this.ipV4 : this.ipV6).setSelected(true);
		}
		updateIsaBox(isaBox, false);
		return isaBox;
	}

	@Override
	public int getPitch() {
		return -1;
	}

	@Override
	public boolean hasCustomEditor() {
		return true;
	}

	private void propertyChange(PropertyEditor<?> editor) {
		InetSocketAddress isa = getValue();
		if (editor == this.integerEditorPort)
			setValue(new InetSocketAddress(isa.getAddress(), (Integer) editor.getValue()));
		else if (editor == this.stringEditorHostName)
			setValue(InetSocketAddress.createUnresolved(this.stringEditorHostName.getValue(), isa.getPort()));
		else
			updateAdress();
	}

	@Override
	public InetSocketAddress readValue(DataInput raf) throws IOException {
		if (raf.readBoolean()) {
			byte[] add = new byte[raf.readBoolean() ? 4 : 16];
			for (int i = 0; i < add.length; i++)
				add[i] = raf.readByte();
			return new InetSocketAddress(InetAddress.getByAddress(add), raf.readShort() & 0xFFFF);
		}
		return InetSocketAddress.createUnresolved(StringEditor.readString(raf), raf.readShort() & 0xFFFF);
	}

	@Override
	public void writeValue(DataOutput raf, InetSocketAddress value) throws IOException {
		if (value.getAddress() != null) {
			raf.writeBoolean(true);
			byte[] add = value.getAddress().getAddress();
			raf.writeBoolean(add.length == 4);
			for (int i = 0; i < add.length; i++)
				raf.write(add[i]);
		} else {
			raf.writeBoolean(false);
			StringEditor.writeString(raf, value.getHostName());
		}
		raf.writeShort(value.getPort());
	}

	@Override
	public String getAsText() {
		InetSocketAddress value = getValue();
		if (value == null)
			return null;
		if (value.getAddress() != null) {
			byte[] add = value.getAddress().getAddress();
			StringBuilder sb = new StringBuilder();
			sb.append('A');
			for (int i = 0; i < add.length - 1; i++)
				sb.append((add[i] & 0xFF) + ".");
			sb.append(add[add.length - 1] & 0xFF);
			sb.append(":" + value.getPort());
			return sb.toString();
		}
		return 'H' + value.getHostName() + ":" + value.getPort();
	}

	@Override
	public void setAsText(String text) {
		if (text == null || text.isEmpty())
			setValue(null);
		else {
			int lastIndex = text.lastIndexOf(":");
			String addOrHost = text.substring(1, lastIndex);
			int port = Integer.parseInt(text.substring(lastIndex + 1));
			if (text.startsWith("A")) {
				String[] addressParts = addOrHost.split("\\.");
				byte[] add = new byte[addressParts.length];
				for (int i = 0; i < add.length; i++)
					add[i] = (byte) (Integer.parseInt(addressParts[i]) & 0xFF);
				try {
					setValue(new InetSocketAddress(InetAddress.getByAddress(add), port));
				} catch (UnknownHostException e) {
					e.printStackTrace();
				}
			} else
				setValue(InetSocketAddress.createUnresolved(addOrHost, port));
		}
	}

	private void updateAdress() {
		InetSocketAddress newIsa;
		if (this.byteEditorIp != null) {
			byte[] adresse = new byte[this.byteEditorIp.length];
			for (int i = 0; i < this.byteEditorIp.length; i++)
				adresse[i] = ((Number) this.byteEditorIp[i].getValue()).byteValue();
			try {
				newIsa = new InetSocketAddress(InetAddress.getByAddress(adresse), getValue().getPort());
			} catch (UnknownHostException e) {
				e.printStackTrace();
				newIsa = null;
			}
		} else
			newIsa = InetSocketAddress.createUnresolved(this.stringEditorHostName.getAsText(), getValue().getPort());
		if (!newIsa.equals(getValue()))
			setValue(newIsa);
	}

	@Override
	public void updateCustomEditor() {
		updateIsaBox((HBox) customEditor(), false);
	}

	private void updateIsaBox(HBox isaBox, boolean resetValue) {
		ObservableList<Node> childs = isaBox.getChildren();
		childs.clear();
		// childs.add(new ComboBox<>(FXCollections.observableArrayList(new ArrayList<String>(Arrays.asList("WC", "V4", "V6", "HN")))));
		InetSocketAddress isa = getValue();
		InetAddress add = isa.getAddress();
		if (this.hostName.isSelected()) {
			this.stringEditorHostName = new StringEditor();
			this.stringEditorHostName.setValue(resetValue ? "" : isa.getHostName());
			this.stringEditorHostName.setStyle("-fx-background-color: transparent;");
			TextField editor = (TextField) this.stringEditorHostName.getCustomEditor();
			editor.setAlignment(Pos.CENTER);
			editor.setPadding(new Insets(0));
			editor.setContextMenu(this.contextMenu);
			editor.setPrefWidth(60);
			editor.minWidth(20);
			childs.add(editor);
			HBox.setHgrow(editor, Priority.ALWAYS);
			this.stringEditorHostName.addPropertyChangeListener(() -> propertyChange(this.stringEditorHostName));
			this.byteEditorIp = null;
			this.customEditorIp = null;
		} else {
			int nbBytes = this.ipV6.isSelected() ? 16 : 4;
			byte[] bytes = add == null || add.getAddress().length != nbBytes ? new byte[nbBytes] : add.getAddress();
			this.byteEditorIp = new ShortEditor[bytes.length];
			this.customEditorIp = new TextField[bytes.length];
			for (int i = 0; i < bytes.length; i++) {
				ShortEditor shortEditor = new ShortEditor();
				shortEditor.setMin((short) 0);
				shortEditor.setMax((short) 255);
				shortEditor.setValue((short) (resetValue ? 0 : bytes[i] & 0xFF));
				shortEditor.setStyle("-fx-background-color: transparent;");
				shortEditor.addPropertyChangeListener(() -> propertyChange(shortEditor));
				TextField editor = (TextField) ((HBox) shortEditor.getNoSelectionEditor()).getChildren().get(0);
				editor.setPadding(new Insets(0));
				editor.setPrefWidth(30);
				editor.setMinWidth(15);
				editor.setAlignment(Pos.CENTER);
				editor.setContextMenu(this.contextMenu);
				HBox.setHgrow(editor, Priority.ALWAYS);
				boolean isEditorDisable = !(this.ipV4.isSelected() || this.ipV6.isSelected());
				editor.setDisable(isEditorDisable); // TODO Bug: if disable, no context menu
				editor.setOnKeyTyped(e -> {
					if (e.getCharacter().equals(".")) {
						FxUtils.traverse();
						e.consume();
					}
				});
				this.customEditorIp[i] = editor;
				childs.add(editor);
				if (i != bytes.length - 1) {
					Label l = new Label(".");
					l.setContextMenu(this.contextMenu);
					l.setTextFill(Color.BLACK); // Bug devient blanc sinon dans array editor sinon
					l.setDisable(isEditorDisable);
					childs.add(l);
				}
				this.byteEditorIp[i] = shortEditor;
				this.stringEditorHostName = null;
			}
			for (ShortEditor se : this.byteEditorIp)
				se.addPropertyChangeListener(() -> propertyChange(se));
		}
		Label l = new Label(":");
		l.setTextFill(Color.BLACK);
		l.setContextMenu(this.contextMenu);
		childs.add(l);
		this.integerEditorPort = new IntegerEditor();
		this.integerEditorPort.setValue(isa.getPort());
		this.integerEditorPort.setMin(0);
		this.integerEditorPort.setMax(0xFFFF);
		this.integerEditorPort.setStyle("-fx-background-color: transparent;");
		this.integerEditorPort.addPropertyChangeListener(() -> propertyChange(this.integerEditorPort));
		TextField editor = (TextField) ((HBox) this.integerEditorPort.getNoSelectionEditor()).getChildren().get(0);
		HBox.setHgrow(editor, Priority.ALWAYS);
		editor.setPrefWidth(30);
		editor.setMinWidth(20);
		editor.setPadding(new Insets(0));
		editor.setAlignment(Pos.CENTER);
		editor.setContextMenu(this.contextMenu);
		childs.add(editor);
		isaBox.setOnContextMenuRequested(e -> {
			this.contextMenu.show(isaBox.getScene().getWindow(), e.getScreenX(), e.getScreenY());
		});
		updateAdress();
	}
}
