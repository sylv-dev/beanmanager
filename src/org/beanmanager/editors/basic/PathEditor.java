/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.basic;

import java.io.File;
import java.nio.file.Path;

public class PathEditor extends AbstractPathEditor<Path> {

	public PathEditor() {}

	public PathEditor(boolean isDirectory) {
		super(isDirectory);
	}

	@Override
	public String getAsText() {
		Path value = getValue();
		return value != null ? value.toString() : null;
	}

	@Override
	public void setAsText(String text) {
		setValue(text == null ? null : Path.of(text));
	}

	@Override
	protected File getValueAsFile() {
		Path value = getValue();
		return value == null ? null : value.toFile();
	}

	@Override
	protected void setValueAsFile(File value) {
		setValue(value == null ? null : value.toPath());
	}
}
