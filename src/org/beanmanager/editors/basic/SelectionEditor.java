/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.basic;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

import org.beanmanager.editors.ChoiceEditor;
import org.beanmanager.editors.GenericPropertyEditor;
import org.beanmanager.editors.container.ArrayEditor;
import org.beanmanager.ihmtest.FxTest;
import org.beanmanager.struct.Selection;
import org.beanmanager.tools.FxUtils;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;
import javafx.event.EventHandler;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tooltip;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.DragEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

public class SelectionEditor<T> extends GenericPropertyEditor<Selection<T>> implements ChoiceEditor<T> {
	private static final DataFormat SELECTION_FORMAT = new DataFormat(SelectionEditor.class.getSimpleName());

	private static <T> Class<?>[] getTypes(T[] choices) {
		if (choices == null || choices.length != 0)
			throw new IllegalArgumentException("choices must be non null and contains at least one element");
		return new Class<?>[] { choices[0].getClass() };
	}

	public static void main(String[] args) {
		new FxTest().launchIHM(args,
				s -> new VBox(new SelectionEditor<>(new Integer[] { 7, 5, 8, 3, 9 }).getCustomEditor(), new SelectionEditor<>(new String[] { "Cool", "Patate", "Dauphin" }).getCustomEditor()));
	}

	private ListView<T> unselectedList;
	private ListView<T> selectedList;

	private T[] choices;

	private ArrayEditor<T[]> arrayEditor;

	public SelectionEditor(Class<T> type) {
		super(new Class<?>[] { type });
	}

	public SelectionEditor(Selection<T> selection) {
		super(new Class<?>[] { selection.getComponentType() });
	}

	public SelectionEditor(T[] choices) {
		super(getTypes(choices));
		setChoices(choices);
	}

	@Override
	public boolean canGrow() {
		return true;
	}

	@SuppressWarnings("unchecked")
	private ArrayEditor<T[]> getArrayEditor() {
		if (this.arrayEditor == null)
			try {
				this.arrayEditor = (ArrayEditor<T[]>) new ArrayEditor<>(getType().arrayType());
			} catch (NullPointerException e) {
				e.printStackTrace();
				// arrayEditor = (ArrayEditor<T[]>) new ArrayEditor<>(Array.newInstance(getType(), 0).getClass());
			}
		return this.arrayEditor;
	}

	private Callback<ListView<T>, ListCell<T>> getCellFactory(EventHandler<DragEvent> evDO, EventHandler<MouseEvent> evDDe, EventHandler<DragEvent> evDDr) {
		return lv -> {
			ListCell<T> lc = new ListCell<>() {
				@Override
				protected void updateItem(T item, boolean empty) {
					super.updateItem(item, empty);
					setText(item == null || empty ? null : item.toString());
					if (item instanceof DescriptionProvider) {
						Tooltip toolTip = getTooltip();
						if (toolTip == null) {
							toolTip = new Tooltip();
							setTooltip(toolTip);
						}
						toolTip.setText(((DescriptionProvider) item).getDescription());
					}
				}
			};
			lc.setOnDragOver(evDO);
			lc.setOnDragDetected(evDDe);
			lc.setOnDragDropped(evDDr);
			return lc;
		};
	}

	@Override
	protected HBox getCustomEditor() {
		if (getValue() == null)
			setValue(new Selection<>(new HashSet<>(), getType()));
		@SuppressWarnings("unchecked")
		Comparator<T> comparator = getType() != null && Comparable.class.isAssignableFrom(getType()) ? (o1, o2) -> ((Comparable<T>) o1).compareTo(o2)
				: (o1, o2) -> o1.toString().compareTo(o2.toString());
		ObservableList<T> unSelectedItems = FXCollections.observableArrayList();
		if (this.choices != null)
			unSelectedItems.addAll(Arrays.asList(this.choices));
		SortedList<T> sl = new SortedList<>(unSelectedItems);
		sl.setComparator(comparator);
		this.unselectedList = new ListView<>(sl);
		sl = new SortedList<>(FXCollections.observableArrayList());
		sl.setComparator(comparator);
		this.selectedList = new ListView<>(sl);
		initList(this.unselectedList, this.selectedList, KeyCode.RIGHT);
		initList(this.selectedList, this.unselectedList, KeyCode.LEFT);
		updateCustomEditor();
		HBox selectionBox = new HBox(this.unselectedList, this.selectedList);
		HBox.setHgrow(this.unselectedList, Priority.ALWAYS);
		HBox.setHgrow(this.selectedList, Priority.ALWAYS);
		selectionBox.setPrefWidth(300);
		selectionBox.setMinHeight(150);
		return selectionBox;
	}

	private EventHandler<MouseEvent> getDragDetected(ListView<T> source) {
		return ev -> {
			ClipboardContent content = new ClipboardContent();
			content.put(SELECTION_FORMAT, source.getSelectionModel().getSelectedIndices().toArray(new Integer[0]));
			source.startDragAndDrop(TransferMode.MOVE).setContent(content);
			ev.consume();
		};
	}

	@SuppressWarnings("unchecked")
	private EventHandler<DragEvent> getDragDropped(ListView<T> source, ListView<T> target) {
		return ev -> {
			if (ev.getGestureSource() != source && ev.getDragboard().hasContent(SELECTION_FORMAT)) {
				Integer[] indices = (Integer[]) ev.getDragboard().getContent(SELECTION_FORMAT);
				SortedList<T> targetSortedList = (SortedList<T>) target.getItems();
				ArrayList<T> toRemove = new ArrayList<>();
				ObservableList<T> sourceItems = (ObservableList<T>) ((SortedList<T>) source.getItems()).getSource();
				for (Integer index : indices) {
					T obj = targetSortedList.get(index);
					toRemove.add(obj);
					sourceItems.add(obj);
				}
				targetSortedList.getSource().removeAll(toRemove);
				updateValue();
				target.getSelectionModel().clearSelection();
				ev.setDropCompleted(true);
				ev.consume();
			}
		};
	}

	private EventHandler<DragEvent> getDragOver(ListView<T> source, ListView<T> target) {
		return ev -> {
			if (ev.getGestureSource() != source && target == ev.getGestureSource() && ev.getDragboard().hasContent(SELECTION_FORMAT))
				ev.acceptTransferModes(TransferMode.MOVE);
			ev.consume();
		};
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getGenericAsText() {
		Selection<T> selection = getValue();
		if (selection == null)
			return "";
		ArrayEditor<T[]> ae = getArrayEditor();
		ae.setValue((T[]) selection.getSelected().toArray());
		return ae.getAsText();
	}

	@SuppressWarnings("unchecked")
	private Class<T> getType() {
		return (Class<T>) getTypes()[0];
	}

	@Override
	public boolean hasCustomEditor() {
		return true;
	}

	private void initList(ListView<T> source, ListView<T> destination, KeyCode moveCode) {
		source.setFixedCellSize(24);
		source.setPrefHeight(source.getFixedCellSize() * 4 + 2);
		source.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		source.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.ENTER) {
				FxUtils.traverse();
				e.consume();
			} else if (e.getCode() == moveCode) {
				ObservableList<T> items = source.getSelectionModel().getSelectedItems();
				@SuppressWarnings("unchecked")
				ObservableList<T> srcItems = (ObservableList<T>) ((SortedList<T>) source.getItems()).getSource();
				@SuppressWarnings("unchecked")
				ObservableList<T> destItems = (ObservableList<T>) ((SortedList<T>) destination.getItems()).getSource();
				destItems.addAll(items);
				srcItems.removeAll(items);
				updateValue();
				e.consume();
			}
		});
		source.focusedProperty().addListener((ov, oldValue, newValue) -> {
			if (!newValue)
				source.getSelectionModel().clearSelection();
		});
		EventHandler<DragEvent> evDO = getDragOver(source, destination);
		EventHandler<MouseEvent> evDDe = getDragDetected(source);
		EventHandler<DragEvent> evDDr = getDragDropped(source, destination);
		source.setOnDragOver(evDO);
		source.setOnDragDetected(evDDe);
		source.setOnDragDropped(evDDr);
		source.setCellFactory(getCellFactory(evDO, evDDe, evDDr));
	}

	@SuppressWarnings("unchecked")
	@Override
	public Selection<T> readGenericValue(DataInput raf) throws IOException {
		Object val = getArrayEditor().readValue(raf);
		HashSet<T> selectedH = new HashSet<>();
		int length = Array.getLength(val);
		for (int i = 0; i < length; i++)
			selectedH.add((T) Array.get(val, i));
		return new Selection<>(selectedH, getType());
	}

	@SuppressWarnings("unchecked")
	@Override
	public void setGenericAsText(String text) {
		if (text == null || text.isEmpty()) {
			setValue(null);
			return;
		}
		ArrayEditor<T[]> ae = getArrayEditor();
		ae.setAsText(text);
		HashSet<T> selectedH = new HashSet<>();
		Object val = ae.getValue();
		int length = Array.getLength(val);
		for (int i = 0; i < length; i++)
			selectedH.add((T) Array.get(val, i));
		setValue(new Selection<>(selectedH, getType()));
	}

	public T[] getChoices() {
		return this.choices;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void setChoices(T[] choices) {
		for (T choice : choices)
			if(choice == null)
				throw new IllegalArgumentException("Choices cannot contains null values");
		this.choices = choices;
		if (this.unselectedList != null) {
			ObservableList<Object> unSelectedItems = (ObservableList<Object>) ((SortedList<Object>) this.unselectedList.getItems()).getSource();
			ObservableList<Object> selectedItems = (ObservableList<Object>) ((SortedList<Object>) this.selectedList.getItems()).getSource();
			if (choices == null) {
				if (!unSelectedItems.isEmpty())
					unSelectedItems.clear();
				if (!selectedItems.isEmpty())
					selectedItems.clear();
			} else {
				List<T> poss = Arrays.asList(choices);
				unSelectedItems.removeIf(o -> !poss.contains(o));
				selectedItems.removeIf(o -> !poss.contains(o));
				for (Object newObj : choices)
					if (!unSelectedItems.contains(newObj) && !selectedItems.contains(newObj))
						unSelectedItems.add(newObj);
			}
		}
	}

	@Override
	public void setValue(Selection<T> value) {
		if (value == null)
			super.setValue(value);
		else {
			Selection<?> selection = value;
			if (this.choices == null)
				super.setValue(value);
			else {
				List<T> poss = Arrays.asList(this.choices);
				HashSet<T> validateSelection = new HashSet<>();
				for (Object element : selection.getSelected()) {
					int i = poss.indexOf(element);
					if (i >= 0)
						validateSelection.add(poss.get(i));
				}
				super.setValue(new Selection<>(validateSelection, getType()));
				// List<Object> poss = Arrays.asList(choices);
				// for (Object element : selection.getSelected())
				// if (!poss.contains(element))
				// throw new IllegalArgumentException("Cannot set selection, element: " + element + " are not included in the choices");
				//
				// super.setValue(value);
			}
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void updateCustomEditor() {
		ObservableList<T> unSelectedItems = (ObservableList<T>) ((SortedList<T>) this.unselectedList.getItems()).getSource();
		ObservableList<T> selectedItems = (ObservableList<T>) ((SortedList<T>) this.selectedList.getItems()).getSource();
		if (this.choices == null) {
			if (!unSelectedItems.isEmpty())
				unSelectedItems.clear();
			if (!selectedItems.isEmpty())
				selectedItems.clear();
		} else {
			Selection<T> value = getValue();
			if (value == null)
				value = new Selection<>(new HashSet<T>(), getType());
			HashSet<T> selected = value.getSelected();
			HashSet<T> unselected = new HashSet<>(Arrays.asList(this.choices));
			selected.removeIf(sele -> !unselected.contains(sele));
			if (!selected.equals(new HashSet<>(selectedItems))) {
				selectedItems.clear();
				selectedItems.addAll(selected);
			}
			unselected.removeAll(selected);
			if (!unselected.equals(new HashSet<>(unSelectedItems))) {
				unSelectedItems.clear();
				unSelectedItems.addAll(unselected);
			}
		}
	}

	private void updateValue() {
		Selection<T> selection = new Selection<>(new HashSet<T>(((SortedList<T>) this.selectedList.getItems()).getSource()), getType());
		if (!selection.equals(getValue()))
			setValue(selection);
	}

	@Override
	@SuppressWarnings("unchecked")
	public void writeGenericValue(DataOutput raf, Selection<T> value) throws IOException {
		getArrayEditor().writeValue(raf, (T[]) value.getSelected().toArray());
	}

	// @Override
	// public boolean canContainForbiddenCharacter() { //Il contient un tableau donc oui...
	// return true;
	// }
}
