/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.basic;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.StringTokenizer;

import org.beanmanager.editors.PropertyEditor;
import org.beanmanager.editors.primitive.number.ControlType;
import org.beanmanager.editors.primitive.number.ShortEditor;
import org.beanmanager.ihmtest.FxTest;
import org.beanmanager.tools.FxUtils;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class ColorEditor extends PropertyEditor<Color> {
	public static void main(String[] args) {
		new FxTest().launchIHM(args, s -> new VBox(new ColorEditor().getEditor(), new ColorEditor().getEditor()));
	}

	private ShortEditor[] shortEditor;

	private ColorPicker colorPicker;

	@Override
	public String getAsText() {
		Color value = getValue();
		if (value == null)
			return null;
		return Double.toString(value.getRed()) + " " + Double.toString(value.getGreen()) + " " + Double.toString(value.getBlue()) + " " + Double.toString(value.getOpacity());
	}

	@Override
	protected HBox getCustomEditor() {
		if (getValue() == null)
			setValue(Color.WHITE);
		this.shortEditor = new ShortEditor[4];
		HBox hbox = new HBox();
		hbox.setStyle("-fx-background-color: white;" + "-fx-border-width: 1px;" + "-fx-border-color: #969696 #DCDCDC #DCDCDC #969696;");
		hbox.setAlignment(Pos.BOTTOM_CENTER);
		this.colorPicker = new ColorPicker();
		this.colorPicker.setMinWidth(120);
		this.colorPicker.setMinHeight(24);
		this.colorPicker.setValue(getValue());
		this.colorPicker.setOnAction(e -> {
			setValue(this.colorPicker.getValue());
			updateValue();
		});
		this.colorPicker.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.ENTER) {
				updateValue();
				FxUtils.traverse();
				e.consume();
			}
		});
		hbox.setSpacing(0);
		hbox.getChildren().add(this.colorPicker);
		hbox.setFillHeight(true);
		HBox.setHgrow(this.colorPicker, Priority.ALWAYS);
		for (int i = 0; i < 4; i++) {
			ShortEditor se = new ShortEditor((short) 0, (short) 255, ControlType.TEXTFIELD);
			se.setStyle("-fx-background-color: transparent;");
			TextField c = (TextField) ((HBox) se.getNoSelectionEditor()).getChildren().get(0);
			c.setPrefWidth(30);
			HBox.setHgrow(c, Priority.ALWAYS);
			c.setPadding(new Insets(0, 1, 0, 1));
			c.setAlignment(Pos.CENTER);
			c.setMinWidth(30);
			hbox.getChildren().add(c);
			if (i != 3) {
				Label l = new Label(",");
				l.setPadding(new Insets(0));
				l.setStyle("-fx-background-color: transparent");
				hbox.getChildren().add(l);
			}
			this.shortEditor[i] = se;
		}
		updateValue();
		for (ShortEditor se : this.shortEditor)
			se.addPropertyChangeListener(() -> {
				int[] values = new int[4];
				for (int i = 0; i < values.length; i++)
					values[i] = ((Number) this.shortEditor[i].getValue()).intValue();
				setValue(new Color(values[0] / 255.0, values[1] / 255.0, values[2] / 255.0, values[3] / 255.0));
			});
		addPropertyChangeListener(() -> {
			if (this.colorPicker != null)
				this.colorPicker.setValue(getValue());
		});
		TextField tf = new TextField();
		tf.setStyle("-fx-background-color: transparent;");
		hbox.setPrefWidth(230);
		return hbox;
	}

	@Override
	public int getPitch() {
		return Double.BYTES * 4;
	}

	@Override
	public boolean hasCustomEditor() {
		return true;
	}

	@Override
	public boolean isFixedControlSized() {
		return true;
	}

	@Override
	public Color readValue(DataInput raf) throws IOException {
		return new Color(raf.readDouble(), raf.readDouble(), raf.readDouble(), raf.readDouble());
	}

	@Override
	public void setAsText(String text) {
		if (text == null || text.isEmpty())
			setValue(null);
		else {
			StringTokenizer st = new StringTokenizer(text, " ");
			setValue(new Color(Double.parseDouble(st.nextToken()), Double.parseDouble(st.nextToken()), Double.parseDouble(st.nextToken()), Double.parseDouble(st.nextToken())));
		}
	}

	@Override
	public void updateCustomEditor() {
		this.colorPicker.setValue(getValue());
		updateValue();
	}

	private void updateValue() {
		Color color = getValue();
		this.shortEditor[0].setValue((short) (color.getRed() * 255));
		this.shortEditor[1].setValue((short) (color.getGreen() * 255));
		this.shortEditor[2].setValue((short) (color.getBlue() * 255));
		this.shortEditor[3].setValue((short) (color.getOpacity() * 255));
	}

	@Override
	public void writeValue(DataOutput raf, Color value) throws IOException {
		Color c = value;
		raf.writeDouble(c.getRed());
		raf.writeDouble(c.getGreen());
		raf.writeDouble(c.getBlue());
		raf.writeDouble(c.getOpacity());
	}
}
