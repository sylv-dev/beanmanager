/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors;

import java.util.EventListener;
import java.util.WeakHashMap;

import javax.swing.event.EventListenerList;

import org.beanmanager.tools.FxUtils;

public interface DynamicSizeEditor extends EventListener {
	static WeakHashMap<Object, EventListenerList> LISTENERS = new WeakHashMap<>();

	public default void addSizeChangeListener(DynamicSizeEditorChangeListener listener) {
		EventListenerList list = LISTENERS.get(this);
		if (list == null) {
			list = new EventListenerList();
			LISTENERS.put(this, list);
		}
		list.add(DynamicSizeEditorChangeListener.class, listener);
	}

	default void fireSizeChanged() {
		FxUtils.runLaterIfNeeded(() -> {
			EventListenerList list = LISTENERS.get(this);
			if (list == null)
				return;
			for (DynamicSizeEditorChangeListener listener : list.getListeners(DynamicSizeEditorChangeListener.class))
				listener.sizeChanged();
		});
	}

	public default void removeSizeChangeListener(DynamicSizeEditorChangeListener listener) {
		EventListenerList list = LISTENERS.get(this);
		if (list == null)
			return;
		list.remove(DynamicSizeEditorChangeListener.class, listener);
	}
}
