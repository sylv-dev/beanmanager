/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.primitive;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.beanmanager.ihmtest.FxTest;
import org.beanmanager.tools.FxUtils;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

public class CharacterEditor extends PrimitiveEditor<Character> {
	private Character[] possibilities;
	private final boolean updatingItems = false;

	public static void main(String[] args) {
		CharacterEditor e = new CharacterEditor(false, new Character[] { 'c', 'g' });
		new Thread(() -> {
			try {
				Thread.sleep(3000);
				Platform.runLater(() -> e.setPossibilities(null));
				Thread.sleep(3000);
				Platform.runLater(() -> e.setPossibilities(new Character[] { 't', 'y' }));
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}
		}).start();

		new FxTest().launchIHM(args,
				s -> new VBox(new CharacterEditor(true).getEditor(), new CharacterEditor(false).getEditor(), e.getEditor(), new CharacterEditor(true, new Character[] { 'x', 'v', '\n' }).getEditor()));

	}

	public CharacterEditor() {}

	public CharacterEditor(boolean primitive) {
		this.primitive = primitive;
	}

	public CharacterEditor(boolean primitive, Character[] possibilities) {
		this.primitive = primitive;
		setPossibilities(possibilities);
	}

	@Override
	public boolean canContainForbiddenCharacter() {
		return true;
	}

	@Override
	public String getAsText() {
		return getValue() == null ? null : Character.toString(getValue());
	}

	@Override
	protected TextField getCustomEditor() {
		if (getValue() == null && this.primitive)
			setValue('_');
		Object value = getValue();
		TextField textField = new TextField(value == null ? "" : getTextWithoutEscapeSequence(((Character) value).toString()));
		textField.setPrefWidth(50);
		textField.focusedProperty().addListener((ov, oldValue, newValue) -> {
			if (!newValue)
				updateValueFromTextField(textField);
		});
		textField.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.ENTER) {
				updateValueFromTextField(textField);
				FxUtils.traverse();
				e.consume();
			}
		});
		addPropertyChangeListener(() -> updateGUI());
		textField.textProperty().addListener(e -> updateStyle());
		HBox.setHgrow(textField, Priority.ALWAYS);
		return textField;
	}

	@Override
	public int getPitch() {
		return Character.BYTES;
	}

	private static String getTextWithEscapeSequence(String text) {
		return text == null ? ""
				: text.replace("\\t", "\t").replace("\\b", "\b").replace("\\n", "\n").replace("\\r", "\r").replace("\\f", "\f").replace("\\'", "\'").replace("\\\"", "\"").replace("\\\\", "\\");
	}

	private static String getTextWithoutEscapeSequence(String text) {
		return text.replace("\\", "\\\\").replace("\t", "\\t").replace("\b", "\\b").replace("\n", "\\n").replace("\r", "\\r").replace("\f", "\\f").replace("\'", "\\'").replace("\"", "\\\"");
	}

	@Override
	public boolean hasCustomEditor() {
		return true;
	}

	@Override
	public boolean isFixedControlSized() {
		return true;
	}

	// @Override
	@Override
	protected ComboBox<Object> createSelectionComboBox(Character[] possibilities) {
		Object[] comboBoxChoices = new String[possibilities.length];
		for (int i = 0; i < possibilities.length; i++)
			comboBoxChoices[i] = getTextWithoutEscapeSequence("" + possibilities[i]);
		ComboBox<Object> comboBox = new ComboBox<>(FXCollections.observableArrayList(comboBoxChoices));
		comboBox.setValue(getTextWithEscapeSequence(getValue() + ""));
		comboBox.setOnScroll(e -> {
			if (e.getDeltaY() != 0) {
				int index = comboBox.getSelectionModel().getSelectedIndex() + (e.getDeltaY() > 0 ? -1 : 1);
				comboBox.getSelectionModel().select(index < 0 ? comboBox.getItems().size() - 1 : index >= comboBox.getItems().size() ? 0 : index);
			}
			e.consume();
		});
		comboBox.valueProperty().addListener((ov, oldValue, newValue) -> {
			if (!this.updatingItems)
				setValue(getTextWithEscapeSequence((String) newValue).charAt(0));
		});
		comboBox.setMaxWidth(Double.MAX_VALUE);
		return comboBox;
	}

	/** @param textField */
	private void updateValueFromTextField(TextField textField) {
		textField.setStyle("-fx-text-fill: black;");
		String text = getTextWithEscapeSequence(textField.getText());
		if (text.isEmpty() && !this.primitive)
			setValue(null);
		else if (text.length() == 1)
			setValue(text.charAt(0));
		else
			updateGUI();
	}

	@Override
	public Character readValue(DataInput raf) throws IOException {
		return raf.readChar();
	}

	@Override
	public void setAsText(String text) {
		setValue(text == null || text.isEmpty() ? null : text.charAt(0));
	}

	@Override
	public Character[] getPossibilities() {
		return this.possibilities;
	}

	@Override
	public void updateCustomEditor() {
		Object value = getValue();
		((TextField) customEditor()).setText(value == null ? "" : getTextWithoutEscapeSequence(((Character) value).toString()));
		updateStyle();
	}

	@Override
	protected void updateComboBox(ComboBox<Object> comboBox) {
		comboBox.setValue(getTextWithoutEscapeSequence(getValue() + ""));
	}

	protected void updateStyle() {
		TextField textField = (TextField) customEditor();
		if (textField == null)
			return;
		String t = getTextWithEscapeSequence(textField.getText());
		int tl = t.length();
		String style = "-fx-text-fill: " + (!this.primitive && getValue() == null && tl == 0 || tl == 1 && Character.valueOf(t.charAt(0)).equals(getValue()) ? "black"
				: !this.primitive && tl > 1 || this.primitive && tl != 1 ? "red" : "green") + ";";
		if (!style.equals(textField.getStyle()))
			textField.setStyle(style);
	}

	@Override
	public void writeValue(DataOutput raf, Character value) throws IOException {
		raf.writeChar(value);
	}
}
