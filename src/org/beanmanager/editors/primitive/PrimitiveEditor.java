/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.primitive;

import org.beanmanager.editors.PropertyEditor;

public abstract class PrimitiveEditor<T> extends PropertyEditor<T> {
	protected boolean primitive = true;

	public void setPrimitive(boolean primitive) {
		this.primitive = primitive;
	}
}
