/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.primitive.number;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD, ElementType.METHOD })
public @interface DynamicNumberInfo {
	/** Method name to get the max value. The prototype of this method must be: {@link ControlType} incrementModeMethodName()
	 * @return the method name. */
	String controlTypeMethodName() default "";

	/** Method name to get the max value. The prototype of this method must be: {@link Double} incrementMethodName()
	 * @return the method name. */
	String incrementMethodName() default "";

	/** Method name to get the max value. The prototype of this method must be: {@link IncrementMode} incrementModeMethodName()
	 * @return the method name. */
	String incrementModeMethodName() default "";

	/** Method name to get the min value. The prototype of this method must be: {@link Integer} minMethodName()
	 * @return the method name. */
	String minMethodName() default "";

	/** Method name to get the max value. The prototype of this method must be: {@link Integer} maxModeMethodName()
	 * @return the method name. */
	String maxModeMethodName() default "";
	// String boundsMethodName();
	//
	// ControlType controlType() default ControlType.TEXTFIELD;
}
