/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.primitive.number;

import static org.beanmanager.editors.primitive.number.ControlType.SLIDER;
import static org.beanmanager.editors.primitive.number.ControlType.SPINNER;
import static org.beanmanager.editors.primitive.number.ControlType.SPINNER_AND_SLIDER;
import static org.beanmanager.editors.primitive.number.ControlType.TEXTFIELD;
import static org.beanmanager.editors.primitive.number.ControlType.TEXTFIELD_AND_SLIDER;

import java.text.DecimalFormat;

import org.beanmanager.editors.primitive.PrimitiveEditor;
import org.beanmanager.tools.FxUtils;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Slider;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.util.StringConverter;

public abstract class NumberEditor<T extends Number> extends PrimitiveEditor<T> {
	// TODO affichage de la valeur du slider en dynamique au dessus de la valeur comme dans XR3Player/Tools
	protected TextField textField;
	private Slider slider;
	private Spinner<T> spinner;
	private ControlType controlType = TEXTFIELD;
	private IncrementMode incrementMode = IncrementMode.LINEAR;
	private double increment = 1;
	protected String style;
	private T[] possibilities;

	public NumberEditor() {}

	public abstract T getMax();

	public abstract T getMaxTypeValue();

	public abstract void setMax(T max);

	public abstract T getMin();

	public abstract T getMinTypeValue();

	public abstract void setMin(T min);

	public abstract T getNumberAsGeneric(Number a);

	protected abstract String getNumberAsText(T number);

	protected abstract T changeValue(T incrFactor, boolean positive, boolean ctrl, boolean shift);

	protected abstract T defaultValue();

	protected abstract int isInBound(T val);

	protected abstract boolean isIntegerNumber();

	protected abstract T parseNumber(String text);

	public static void main(String[] args) {
		for (double ratio = 0; ratio <= 1.01; ratio += 0.01) {
			System.out.println("ratio: " + ratio);
			double minv = Math.log(1);
			double value = Math.exp(minv + (Math.log(100) - minv) * ratio);
			System.out.println(value);
			System.out.println("ratio2: " + (Math.log(value) - minv) / (Math.log(100) - minv));
		}
	}

	// protected abstract void setNumberValue( value);

	@Override
	protected HBox getCustomEditor() {
		if (getValue() == null && this.primitive)
			setValue(defaultValue());
		this.textField = null;
		this.slider = null;
		this.spinner = null;
		if (this.controlType == SLIDER || this.controlType == TEXTFIELD_AND_SLIDER || this.controlType == SPINNER_AND_SLIDER) {
			if (getValue() == null)
				setValue(defaultValue());
			this.slider = new Slider(getMin().doubleValue(), getMax().doubleValue(), ((Number) getValue()).doubleValue());
			HBox.setHgrow(this.slider, Priority.ALWAYS);
			this.slider.setMaxWidth(Double.MAX_VALUE);
			if (getMax().doubleValue() > getMin().doubleValue())
				this.slider.setMajorTickUnit(getMax().doubleValue() - getMin().doubleValue());
			if (isIntegerNumber()) {
				if (this.slider.getMajorTickUnit() < 1000) // warning sinon
					this.slider.setMinorTickCount((int) (this.slider.getMajorTickUnit() - 1));
				this.slider.setShowTickMarks(this.slider.getMajorTickUnit() <= 50);
				this.slider.setSnapToTicks(true);
			} else {
				double nbMinorTick = this.slider.getMajorTickUnit();
				while (nbMinorTick < 5 || nbMinorTick > 50)
					nbMinorTick = nbMinorTick <= 5 ? nbMinorTick * 10 : nbMinorTick / 10;
				// incrFactor = slider.getMajorTickUnit() / nbMinorTick; //PK??? suppression le 6/10/2018
				this.slider.setMinorTickCount((int) nbMinorTick - 1);
				this.slider.setShowTickMarks(true);
			}
			this.slider.setShowTickLabels(true);
			StringConverter<Double> sc = new StringConverter<>() {
				private final DecimalFormat formatter = new DecimalFormat("####.##E0");

				@Override
				public Double fromString(String arg0) {
					return Double.parseDouble(arg0);
				}

				@Override
				public String toString(Double arg0) {
					if (arg0.toString().length() <= 8)
						return arg0.toString();
					String out = this.formatter.format(arg0);
					if (out.endsWith("E0"))
						out = out.substring(0, out.length() - 2);
					return out;
				}
			};
			this.slider.setLabelFormatter(sc);
			this.slider.setMinWidth(200);
			this.slider.setPadding(new Insets(0, sc.toString(getMax().doubleValue()).length() * 2.5, 0, sc.toString(getMin().doubleValue()).length() * 2.5));// Font mesure... RT-8060
			this.slider.setOnScroll(e -> {
				if (e.getDeltaY() != 0)
					incrValue(e.getDeltaX() + e.getDeltaY() >= 0, e.isControlDown(), e.isShiftDown());
				e.consume();
			});
			this.slider.setOnKeyPressed(e -> {
				if (e.getCode() == KeyCode.ENTER)
					FxUtils.traverse();
			});
			this.slider.valueProperty().addListener((ov, oldValue, newValue) -> {
				if (this.incrementMode == IncrementMode.LOGARITHMIQUE && getMin().doubleValue() != newValue.doubleValue() && getMax().doubleValue() != newValue.doubleValue()) {
					if (getMin().doubleValue() != 0 && getMin().doubleValue() * getMax().doubleValue() > 0)
						setValue(getNumberAsGeneric(getValueFromLogarithmiqueSliderPos(newValue)));
					else {
						System.err.println("Cannot use logarithmique scale, min must be different to zero and min and max must have the same sign");
						setValue(getNumberAsGeneric(newValue));
					}
				} else
					setValue(getNumberAsGeneric(newValue));
			});
			this.slider.setOnKeyPressed(e -> {
				KeyCode c = e.getCode();
				if (c == KeyCode.ENTER)
					FxUtils.traverse();
				else if (c == KeyCode.UP || c == KeyCode.DOWN || (c == KeyCode.RIGHT || c == KeyCode.LEFT) && this.slider != null) {
					incrValue(c == KeyCode.UP || c == KeyCode.RIGHT, e.isControlDown(), e.isShiftDown());
					e.consume();
				}
			});
			this.slider.setMaxWidth(Double.MAX_VALUE);
		}
		if (this.controlType == TEXTFIELD || this.controlType == TEXTFIELD_AND_SLIDER) {
			this.textField = new TextField();
			HBox.setHgrow(this.textField, Priority.ALWAYS);
			this.textField.setMaxWidth(Double.MAX_VALUE);
			this.textField.setMinWidth(50);
			this.textField.setPrefWidth(this.slider == null ? 150 : 100);
			this.textField.setContextMenu(getMinMaxContextMenu());
			updateCustomEditor();
			this.textField.setOnScroll(e -> {
				if (e.getDeltaY() != 0)
					incrValue(e.getDeltaX() + e.getDeltaY() >= 0, e.isControlDown(), e.isShiftDown());
				e.consume();
			});
			this.textField.focusedProperty().addListener((ov, oldValue, newValue) -> {
				if (this.textField != null && !newValue)
					updateValueFromTextField(this.textField);
				updateCustomEditor();
			});
			this.textField.textProperty().addListener(e -> updateStyle());
			if (this.style != null)
				this.textField.setStyle(this.style);

			this.textField.setOnKeyPressed(e -> {
				KeyCode c = e.getCode();
				if (c == KeyCode.ENTER) {
					updateValueFromTextField(this.textField);
					FxUtils.traverse();
					e.consume();
				} else if (c == KeyCode.UP || c == KeyCode.DOWN || (c == KeyCode.RIGHT || c == KeyCode.LEFT) && this.slider != null) {
					incrValue(c == KeyCode.UP || c == KeyCode.RIGHT, e.isControlDown(), e.isShiftDown());
					e.consume();
				}
			});
			this.textField.setMaxWidth(Double.MAX_VALUE);
		} else if (this.controlType == SPINNER || this.controlType == SPINNER_AND_SLIDER) {
			if (getValue() == null)
				setValue(defaultValue());
			if (isIntegerNumber()) {
				long min = getMin().longValue();
				if (min > Integer.MAX_VALUE) // PK???
					min = Integer.MAX_VALUE;
				else if (min < Integer.MIN_VALUE)
					min = Integer.MIN_VALUE;
				long max = getMax().longValue();
				if (max > Integer.MAX_VALUE)
					max = Integer.MAX_VALUE;
				else if (max < Integer.MIN_VALUE)
					max = Integer.MIN_VALUE;
				this.spinner = new Spinner<>(min, max, ((Number) getValue()).longValue());
			} else
				this.spinner = new Spinner<>(getMin().doubleValue(), getMax().doubleValue(), ((Number) getValue()).doubleValue());
			HBox.setHgrow(this.spinner, Priority.ALWAYS);
			this.spinner.setMaxWidth(Double.MAX_VALUE);
			this.spinner.setValueFactory(new SpinnerValueFactory<T>() {

				@Override
				public void decrement(int steps) {
					for (int i = 0; i < steps; i++)
						incrValue(false, false, false);
				}

				@Override
				public void increment(int steps) {
					for (int i = 0; i < steps; i++)
						incrValue(true, false, false);
				}

			});
			this.spinner.getValueFactory().setConverter(new StringConverter<T>() {
				@Override
				public T fromString(String value) {
					return parseNumber(value);
				};

				@Override
				public String toString(T value) {
					return getNumberAsText(value);
				};
			});
			this.spinner.setContextMenu(getMinMaxContextMenu());
			this.spinner.setMinWidth(50);
			this.spinner.setPrefWidth(150);
			this.spinner.setEditable(true);
			updateCustomEditor();
			this.spinner.setOnScroll(e -> {
				if (e.getDeltaY() != 0)
					incrValue(e.getDeltaX() + e.getDeltaY() >= 0, e.isControlDown(), e.isShiftDown());
				e.consume();
			});
			this.spinner.focusedProperty().addListener((ov, oldValue, newValue) -> {
				if (!newValue)
					try {
						String text = this.spinner.getEditor().getText();
						updateValue(text.isEmpty() && !this.primitive ? null : parseNumber(text));
					} catch (NumberFormatException ex) {
						updateCustomEditor();
					}
			});
			this.spinner.getEditor().setOnKeyPressed(e -> {
				KeyCode c = e.getCode();
				if (c == KeyCode.UP || c == KeyCode.DOWN || (c == KeyCode.RIGHT || c == KeyCode.LEFT) && this.slider != null) {
					incrValue(c == KeyCode.UP || c == KeyCode.RIGHT, e.isControlDown(), e.isShiftDown());
					e.consume();
				}
			});
			this.spinner.getEditor().setOnAction(e -> {
				try {
					String text = this.spinner.getEditor().getText();
					updateValue(text.isEmpty() && !this.primitive ? null : parseNumber(text));
				} catch (NumberFormatException ex) {
					updateCustomEditor();
				}
			});
			this.spinner.getEditor().textProperty().addListener(e -> updateStyle());
			this.spinner.valueProperty().addListener((a, b, c) -> {
				try {
					String text = this.spinner.getEditor().getText();
					updateValue(text.isEmpty() && !this.primitive ? null : parseNumber(text));
				} catch (NumberFormatException ex) {
					updateCustomEditor();
				}
			});
			if (this.style != null)
				this.spinner.setStyle(this.style);
			this.spinner.setOnKeyPressed(e -> {
				KeyCode c = e.getCode();
				if (c == KeyCode.ENTER)
					FxUtils.traverse();
				else if (c == KeyCode.UP || c == KeyCode.DOWN || (c == KeyCode.RIGHT || c == KeyCode.LEFT) && this.slider != null) {
					incrValue(c == KeyCode.UP || c == KeyCode.RIGHT, e.isControlDown(), e.isShiftDown());
					e.consume();
				}
			});
			this.spinner.setMaxWidth(Double.MAX_VALUE);
		}
		HBox hBox = new HBox();
		if (this.slider == null)
			hBox.getChildren().setAll(this.textField != null ? this.textField : this.spinner);
		else if (this.textField == null && this.spinner == null)
			hBox.getChildren().setAll(this.slider);
		else
			hBox.getChildren().setAll(this.slider, this.textField != null ? this.textField : this.spinner);
		hBox.setAlignment(Pos.BASELINE_CENTER);
		return hBox;
	}

	private T getIncrValue(Boolean positive) {
		T incrValue;
		if (this.incrementMode == IncrementMode.LINEAR)
			incrValue = getNumberAsGeneric(this.increment);
		else if (this.incrementMode == IncrementMode.LOGARITHMIQUE) {
			if (positive == null)
				incrValue = getNumberAsGeneric(this.increment);
			else if (getMin().doubleValue() != 0 && getMin().doubleValue() * getMax().doubleValue() > 0) {
				double minv = Math.log(getMin().doubleValue());
				double ratio = (Math.log(((Number) getValue()).doubleValue()) - minv) / (Math.log(getMax().doubleValue()) - minv);
				if (positive)
					incrValue = getNumberAsGeneric(Math.exp(minv + (Math.log(getMax().doubleValue()) - minv) * (ratio + this.increment)) - ((Number) getValue()).doubleValue());
				else
					incrValue = getNumberAsGeneric(((Number) getValue()).doubleValue() - Math.exp(minv + (Math.log(getMax().doubleValue()) - minv) * (ratio - this.increment)));
			} else {
				System.err.println("Cannot use logarithmique scale, min must be different to zero and min and max must have the same sign");
				incrValue = getNumberAsGeneric(this.increment);
			}
		} else if (this.incrementMode == IncrementMode.LOGARITHMIQUE)
			incrValue = getNumberAsGeneric(Math.abs((isIntegerNumber() ? getMax().longValue() - getMin().longValue() : getMax().doubleValue() - getMin().doubleValue()) * this.increment));
		else if (this.increment <= 1) {
			System.err.println("Cannot use geometric progression, increment must be greater than 1");
			incrValue = getNumberAsGeneric(this.increment);
		} else
			incrValue = getNumberAsGeneric(Math.abs(((Number) getValue()).doubleValue()) * this.increment - Math.abs(((Number) getValue()).doubleValue()));
		if (isIntegerNumber()) {
			if (incrValue.longValue() == 0)
				incrValue = getNumberAsGeneric(1);
		} else if (incrValue.doubleValue() == 0)
			incrValue = getNumberAsGeneric(1);
		return incrValue;
	}

	private double getLogarithmiqueSliderPosFromValue() {
		double minv = Math.log(getMin().doubleValue());
		return (Math.log(((Number) getValue()).doubleValue()) - minv) / (Math.log(getMax().doubleValue()) - minv) * (getMax().doubleValue() - getMin().doubleValue()) + getMin().doubleValue();
	}

	private double getValueFromLogarithmiqueSliderPos(Number newValue) {
		double minv = Math.log(getMin().doubleValue());
		return Math.exp(minv + (Math.log(getMax().doubleValue()) - minv) * (newValue.doubleValue() - getMin().doubleValue()) / (getMax().doubleValue() - getMin().doubleValue()));
	}

	@Override
	public boolean hasCustomEditor() {
		return true;
	}

	private void incrValue(boolean positive, boolean controlDown, boolean shiftDown) {
		if (getValue() == null)
			setValue(defaultValue());
		updateValue(changeValue(getIncrValue(positive), positive, controlDown, shiftDown));
	}

	public void initWithDefaultValue() {
		setValue(this.possibilities == null ? defaultValue() : this.possibilities[0]);
	}

	@Override
	public boolean isFixedControlSized() {
		return true;
	}

	/** @param textField2 */
	private void updateValueFromTextField(TextField textField2) {
		try {
			String text = this.textField.getText();
			updateValue(text.isEmpty() && !this.primitive ? null : parseNumber(text));
		} catch (NumberFormatException ex) {}
	}

	private ContextMenu getMinMaxContextMenu() {
		MenuItem minMenuItem = new MenuItem("set To min");
		minMenuItem.setOnAction(e -> setValue(getMin()));
		MenuItem maxMenuItem = new MenuItem("set to max");
		maxMenuItem.setOnAction(e -> setValue(getMax()));
		return new ContextMenu(minMenuItem, maxMenuItem);
	}

	public void setControleType(ControlType controlType) {
		if (controlType == null)
			throw new IllegalArgumentException("argument cannot be null");
		if (this.controlType != controlType) {
			this.controlType = controlType;
			resetEditor();
		}
	}

	public void setIncrement(double increment) {
		this.increment = increment;
	}

	public void setIncrementMode(IncrementMode incrementMode) {
		if (incrementMode == null)
			throw new IllegalArgumentException("argument cannot be null");
		this.incrementMode = incrementMode;
	}

	@Override
	public void setPossibilities(T[] possibilities) {
		super.setPossibilities(possibilities);
		resetEditor();
	}

	@Override
	public T[] getPossibilities() {
		return this.possibilities;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	@Override
	public void setValue(T value) {
		if (value == null || isInBound(value) == 0)
			super.setValue(value);
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " Value: " + getValue();
	}

	@Override
	public void updateCustomEditor() {
		T val = getValue();
		if (this.textField != null) {
			String stringVal = val == null ? "" : getNumberAsText(val);
			if (!stringVal.equals(this.textField.getText()))
				this.textField.setText(stringVal);
		}
		if (this.spinner != null) {
			if (val == null)
				setValue(defaultValue());
			val = getValue();
			// if (isIntegerNumber())
			this.spinner.getValueFactory().setValue(val);
			// else
			// spinner.getValueFactory().setValue(val.doubleValue());
			this.spinner.getEditor().setText(this.spinner.getValueFactory().getConverter().toString(this.spinner.getValueFactory().getValue()));
		}
		if (this.slider != null) {
			if (val == null)
				setValue(defaultValue());
			val = getValue();
			if (this.incrementMode == IncrementMode.LOGARITHMIQUE) {
				if (getMin().doubleValue() != 0 && getMin().doubleValue() * getMax().doubleValue() > 0)
					this.slider.setValue(getLogarithmiqueSliderPosFromValue());
				else {
					System.err.println("Cannot use logarithmique scale, min must be different to zero and min and max must have the same sign");
					this.slider.setValue(val.doubleValue());
				}
			} else
				this.slider.setValue(val.doubleValue());
		}
		// if (comboBox != null)
		// comboBox.setValue(getValue());
		updateStyle();
	}

	protected void updateStyle() {
		TextField tf;
		if (this.textField != null)
			tf = this.textField;
		else if (this.spinner != null)
			tf = this.spinner.getEditor();
		else
			return;
		T val = null;
		boolean badFormatted = false;
		String tfText = tf.getText();
		try {
			val = parseNumber(tfText);
		} catch (NumberFormatException ex) {
			badFormatted = !tfText.isEmpty();
		}
		String style = "-fx-text-fill: " + (badFormatted || val == null && this.primitive || val != null && isInBound(val) != 0 ? "red"
				: tfText.equals(getValue() == null ? "" : getNumberAsText(getValue())) ? "black" : "green") + ";";
		if (this.style != null)
			style = this.style + style;
		if (!style.equals(tf.getStyle()))
			tf.setStyle(style);
	}

	protected void updateValue(T newVal) {
		if (newVal != null) {
			int comp = isInBound(newVal);
			if (comp != 0)
				newVal = comp == -1 ? getMin() : getMax();
		}
		if (newVal != getValue())
			if (newVal == null) {
				if (!this.primitive)
					setValue(null);
			} else
				setValue(newVal);
	}

	protected void updateValueWithBounds() {
		T val = getValue();
		if (val != null) {
			int comp = isInBound(val);
			if (comp != 0)
				setValue(comp == -1 ? getMin() : getMax());
		}
	}

	@Override
	public String getDescription() {
		if (getMin().equals(getMinTypeValue()) && getMax().equals(getMaxTypeValue()))
			return "";
		return "[" + (getMin().equals(getMinTypeValue()) ? "min" : getMin().toString()) + ", " + (getMax().equals(getMaxTypeValue()) ? "max" : getMax().toString()) + "]";
	}
}
