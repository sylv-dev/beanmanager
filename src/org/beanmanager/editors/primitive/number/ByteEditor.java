/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.primitive.number;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.beanmanager.ihmtest.FxTest;

import javafx.scene.layout.VBox;

public class ByteEditor extends NumberEditor<Byte> {
	private byte min = getMinTypeValue();
	private byte max = getMaxTypeValue();

	public static void main(String[] args) {
		new FxTest().launchIHM(args,
				s -> new VBox(new ByteEditor((byte) 10, (byte) 20, ControlType.TEXTFIELD).getEditor(), new ByteEditor((byte) 0, (byte) 50, ControlType.SPINNER_AND_SLIDER).getEditor()));
	}

	public ByteEditor() {}

	public ByteEditor(byte min, byte max, ControlType controlType) {
		setMin(min);
		setMax(max);
		setControleType(controlType);
	}

	public ByteEditor(byte min, byte max, ControlType controlType, byte increment, IncrementMode incrementMode) {
		setMin(min);
		setMax(max);
		setIncrement(increment);
		setIncrementMode(incrementMode);
		setControleType(controlType);
	}

	public ByteEditor(Byte[] numbers) {
		super();
		setPossibilities(numbers);
	}

	@Override
	protected Byte changeValue(Byte incrFactor, boolean positive, boolean ctrl, boolean shift) {
		byte inc = incrFactor;
		if (shift && ctrl)
			inc *= 100;
		else if (shift)
			inc *= 1000;
		else if (ctrl)
			inc *= 10;
		return (byte) (getValue() + (positive ? inc : -inc));
	}

	private void checkBounds() {
		// if (max == Byte.MIN_VALUE)
		// max++;
		// if (min == Byte.MAX_VALUE)
		// min--;
		// if (max <= min)
		// max = (byte) (min + 1);
		if (this.max < this.min)
			this.max = this.min;
		updateValueWithBounds();
	}

	@Override
	protected Byte defaultValue() {
		return this.min > 0 || this.max < 0 ? this.min : 0;
	}

	@Override
	public String getAsText() {
		return getValue() == null ? null : Byte.toString(getValue());
	}

	@Override
	public Byte getMax() {
		return this.max;
	}

	@Override
	public Byte getMin() {
		return this.min;
	}

	@Override
	public Byte getNumberAsGeneric(Number number) {
		return number.byteValue();
	}

	@Override
	protected String getNumberAsText(Byte number) {
		return Byte.toString(number);
	}

	@Override
	public int getPitch() {
		return Byte.BYTES;
	}

	@Override
	protected int isInBound(Byte val) {
		return val < this.min ? -1 : val > this.max ? 1 : 0;
	}

	@Override
	protected boolean isIntegerNumber() {
		return true;
	}

	@Override
	protected Byte parseNumber(String text) {
		return Byte.parseByte(text);
	}

	@Override
	public Byte readValue(DataInput raf) throws IOException {
		return raf.readByte();
	}

	@Override
	public void setAsText(String text) {
		setValue(text == null || text.isEmpty() || text.equals(String.valueOf((Object) null)) ? null : Byte.parseByte(text));
	}

	@Override
	public void setMax(Byte max) {
		this.max = max == null ? Byte.MAX_VALUE : max.byteValue();
		checkBounds();
		resetEditor();
	}

	@Override
	public void setMin(Byte min) {
		this.min = min == null ? Byte.MIN_VALUE : min.byteValue();
		checkBounds();
		resetEditor();
	}

	@Override
	public void writeValue(DataOutput raf, Byte value) throws IOException {
		raf.writeByte(value);
	}

	@Override
	public Byte getMaxTypeValue() {
		return Byte.MAX_VALUE;
	}

	@Override
	public Byte getMinTypeValue() {
		return Byte.MIN_VALUE;
	}
}
