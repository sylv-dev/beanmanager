/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.primitive.number;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.beanmanager.ihmtest.FxTest;

import javafx.scene.layout.VBox;

public class DoubleEditor extends NumberEditor<Double> {
	private double min = getMinTypeValue();
	private double max = getMaxTypeValue();

	public static void main(String[] args) {
		DoubleEditor de = new DoubleEditor(0.001, 100, 0.2, IncrementMode.LOGARITHMIQUE, ControlType.SPINNER_AND_SLIDER);
		de.setPossibilities(new Double[] { 6.25, 16.12, 2155985.2386764 });
		new FxTest().launchIHM(args,
				s -> new VBox(new DoubleEditor().getCustomEditor(), new DoubleEditor(0, 0.0015916f, 0.1, IncrementMode.PROPORTIONAL, ControlType.SLIDER).getEditor(),
						new DoubleEditor(0, 100, 0.75, IncrementMode.LINEAR, ControlType.SPINNER_AND_SLIDER).getEditor(),
						new DoubleEditor(0, 100, 0.75, IncrementMode.PROPORTIONAL, ControlType.SPINNER_AND_SLIDER).getEditor(),
						new DoubleEditor(0.001, 100, 0.2, IncrementMode.LOGARITHMIQUE, ControlType.SPINNER_AND_SLIDER).getEditor(), de.getEditor()));
	}

	public DoubleEditor() {}

	public DoubleEditor(double min, double max, double increment, IncrementMode incrementMode, ControlType controlType) {
		super();
		setMin(min);
		setMax(max);
		setIncrement(increment);
		setIncrementMode(incrementMode);
		setControleType(controlType);
	}

	public DoubleEditor(Double[] numbers) {
		super();
		setPossibilities(numbers);
	}

	@Override
	protected Double changeValue(Double incrFactor, boolean positive, boolean ctrl, boolean shift) {
		double inc = incrFactor;
		if (shift && ctrl)
			inc /= 100;
		else if (shift)
			inc /= 1000;
		else if (ctrl)
			inc /= 10;
		return getValue() + (positive ? inc : -inc);
	}

	private void checkBounds() {
		// if (max == -Double.MAX_VALUE)
		// max++;
		// if (min == Double.MAX_VALUE)
		// min--;
		// if (max <= min)
		// max = min + 1;
		if (this.max < this.min)
			this.max = this.min;
		updateValueWithBounds();
	}

	@Override
	protected Double defaultValue() {
		return this.min > 0 || this.max < 0 ? this.min : 0;
	}

	@Override
	public String getAsText() {
		return getValue() == null ? null : Double.toString(getValue());
	}

	@Override
	public Double getMax() {
		return this.max;
	}

	@Override
	public Double getMin() {
		return this.min;
	}

	@Override
	public Double getNumberAsGeneric(Number number) {
		return number.doubleValue();
	}

	@Override
	protected String getNumberAsText(Double number) {
		return Double.toString(number);
	}

	@Override
	public int getPitch() {
		return Double.BYTES;
	}

	@Override
	protected int isInBound(Double val) {
		return val.doubleValue() < this.min ? -1 : val.doubleValue() > this.max ? 1 : 0;
	}

	@Override
	protected boolean isIntegerNumber() {
		return false;
	}

	@Override
	protected Double parseNumber(String text) {
		return Double.parseDouble(text);
	}

	@Override
	public Double readValue(DataInput raf) throws IOException {
		return raf.readDouble();
	}

	@Override
	public void setAsText(String text) {
		setValue(text == null || text.isEmpty() || text.equals(String.valueOf((Object) null)) ? null : Double.parseDouble(text));
	}

	@Override
	public void setMax(Double max) {
		if (max != null && Double.isInfinite(max))
			throw new IllegalArgumentException("argument must be finite");
		this.max = max == null || Double.isNaN(max) ? Double.MAX_VALUE : max;
		checkBounds();
		resetEditor();
	}

	@Override
	public void setMin(Double min) {
		if (min != null && Double.isInfinite(min))
			throw new IllegalArgumentException("argument must be finite");
		this.min = min == null || Double.isNaN(min) ? -Double.MAX_VALUE : min;
		checkBounds();
		resetEditor();
	}

	@Override
	public void writeValue(DataOutput raf, Double value) throws IOException {
		raf.writeDouble(value);
	}

	@Override
	public Double getMaxTypeValue() {
		return Double.MAX_VALUE;
	}

	@Override
	public Double getMinTypeValue() {
		return -Double.MAX_VALUE;
	}
}
