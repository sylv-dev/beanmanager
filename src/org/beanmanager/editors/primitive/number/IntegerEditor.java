/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.primitive.number;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.beanmanager.ihmtest.FxTest;

import javafx.application.Platform;
import javafx.scene.layout.VBox;

public class IntegerEditor extends NumberEditor<Integer> {
	private int min = getMinTypeValue();
	private int max = getMaxTypeValue();

	public static void main(String[] args) {
		IntegerEditor e1 = new IntegerEditor(0, 10, ControlType.TEXTFIELD);
		e1.setValue(-5);
		e1.setPrimitive(false);
		IntegerEditor e2 = new IntegerEditor(new Integer[] { 1, 6, 15 });
		e2.setPrimitive(false);
		IntegerEditor e3 = new IntegerEditor();
		e3.setPrimitive(false);
		new Thread(() -> {
			try {
				Thread.sleep(3000);
				Platform.runLater(() -> {
					e1.setMin(5);
					e1.setMax(15);
					// e1.setControleType(ControlType.SLIDER);
					e2.setPossibilities(null);
					e3.setPossibilities(new Integer[] { 12, 24, 32 });
				});
				Thread.sleep(3000);
				Platform.runLater(() -> e1.setPossibilities(new Integer[] { 8, 12 }));
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}
		}).start();

		new FxTest().launchIHM(args, s -> new VBox(e1.getEditor(), e2.getEditor(), e3.getEditor()));
	}

	public IntegerEditor() {}

	public IntegerEditor(int min, int max, ControlType controlType) {
		setMin(min);
		setMax(max);
		setControleType(controlType);
	}

	public IntegerEditor(int min, int max, ControlType controlType, int increment, IncrementMode incrementMode) {
		setMin(min);
		setMax(max);
		setIncrement(increment);
		setIncrementMode(incrementMode);
		setControleType(controlType);
	}

	public IntegerEditor(Integer[] numbers) {
		super();
		setPossibilities(numbers);
	}

	@Override
	protected Integer changeValue(Integer incrFactor, boolean positive, boolean ctrl, boolean shift) {
		int inc = incrFactor;
		if (shift && ctrl)
			inc *= 100;
		else if (shift)
			inc *= 1000;
		else if (ctrl)
			inc *= 10;
		return getValue() + (positive ? inc : -inc);
	}

	private void checkBounds() {
		// if (max == Integer.MIN_VALUE)
		// max++;
		// if (min == Integer.MAX_VALUE)
		// min--;
		// if (max <= min)
		// max = min + 1;
		if (this.max < this.min)
			this.max = this.min;
		updateValueWithBounds();
	}

	@Override
	protected Integer defaultValue() {
		return this.min > 0 || this.max < 0 ? this.min : 0;
	}

	@Override
	public String getAsText() {
		return getValue() == null ? null : Integer.toString(getValue());
	}

	@Override
	public Integer getMax() {
		return this.max;
	}

	@Override
	public Integer getMin() {
		return this.min;
	}

	@Override
	public Integer getNumberAsGeneric(Number number) {
		return number.intValue();
	}

	@Override
	protected String getNumberAsText(Integer number) {
		return Integer.toString(number.intValue());
	}

	@Override
	public int getPitch() {
		return Integer.BYTES;
	}

	@Override
	protected int isInBound(Integer val) {
		return val < this.min ? -1 : val > this.max ? 1 : 0;
	}

	@Override
	protected boolean isIntegerNumber() {
		return true;
	}

	@Override
	protected Integer parseNumber(String text) {
		return Integer.parseInt(text);
	}

	@Override
	public Integer readValue(DataInput raf) throws IOException {
		return raf.readInt();
	}

	@Override
	public void setAsText(String text) {
		setValue(text == null || text.isEmpty() || text.equals(String.valueOf((Object) null)) ? null : Integer.parseInt(text));
	}

	@Override
	public void setMax(Integer max) {
		this.max = max == null ? Integer.MAX_VALUE : max;
		checkBounds();
		resetEditor();
	}

	@Override
	public void setMin(Integer min) {
		this.min = min == null ? Integer.MIN_VALUE : min;
		checkBounds();
		resetEditor();
	}

	@Override
	public void writeValue(DataOutput raf, Integer value) throws IOException {
		raf.writeInt(value);
	}

	@Override
	public Integer getMaxTypeValue() {
		return Integer.MAX_VALUE;
	}

	@Override
	public Integer getMinTypeValue() {
		return Integer.MIN_VALUE;
	}
}
