/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.multiNumber.point;

import java.util.List;
import java.util.function.Supplier;

import javax.vecmath.Point2i;

import org.beanmanager.editors.multiNumber.StaticSizeMonoDimensionEditor;
import org.beanmanager.editors.primitive.number.IntegerEditor;
import org.beanmanager.editors.primitive.number.NumberEditor;

public class Point2iEditor extends StaticSizeMonoDimensionEditor<Point2i, Integer> {
	public Point2iEditor() {
		super(2);
	}

	@Override
	protected List<Integer> getArrayFromValue(Point2i value) {
		return List.of(value.x, value.y);
	}

	@Override
	protected Supplier<NumberEditor<Integer>> getEditorConstructor() {
		return IntegerEditor::new;
	}

	@Override
	protected Point2i getValueFromArray(List<Integer> datas) {
		return new Point2i(datas.get(0), datas.get(1));
	}
}
