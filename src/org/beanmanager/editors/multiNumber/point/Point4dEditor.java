/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.multiNumber.point;

import java.util.List;
import java.util.function.Supplier;

import javax.vecmath.Point4d;

import org.beanmanager.editors.multiNumber.StaticSizeMonoDimensionEditor;
import org.beanmanager.editors.primitive.number.DoubleEditor;
import org.beanmanager.editors.primitive.number.NumberEditor;

public class Point4dEditor extends StaticSizeMonoDimensionEditor<Point4d, Double> {
	public Point4dEditor() {
		super(4);
	}

	@Override
	protected List<Double> getArrayFromValue(Point4d value) {
		return List.of(value.x, value.y, value.z, value.w);
	}

	@Override
	protected Supplier<NumberEditor<Double>> getEditorConstructor() {
		return DoubleEditor::new;
	}

	@Override
	protected Point4d getValueFromArray(List<Double> datas) {
		return new Point4d(datas.get(0), datas.get(1), datas.get(2), datas.get(3));
	}
}
