/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.multiNumber.point;

import java.util.List;
import java.util.function.Supplier;

import javax.vecmath.Point2d;

import org.beanmanager.editors.multiNumber.StaticSizeMonoDimensionEditor;
import org.beanmanager.editors.primitive.number.DoubleEditor;
import org.beanmanager.editors.primitive.number.NumberEditor;

public class Point2dEditor extends StaticSizeMonoDimensionEditor<Point2d, Double> {
	public Point2dEditor() {
		super(2);
	}

	@Override
	protected List<Double> getArrayFromValue(Point2d value) {
		return List.of(value.x, value.y);
	}

	@Override
	protected Supplier<NumberEditor<Double>> getEditorConstructor() {
		return DoubleEditor::new;
	}

	@Override
	protected Point2d getValueFromArray(List<Double> datas) {
		return new Point2d(datas.get(0), datas.get(1));
	}
}
