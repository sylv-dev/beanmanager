/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.multiNumber.point;

import java.util.List;
import java.util.function.Supplier;

import javax.vecmath.Point2f;

import org.beanmanager.editors.multiNumber.StaticSizeMonoDimensionEditor;
import org.beanmanager.editors.primitive.number.FloatEditor;
import org.beanmanager.editors.primitive.number.NumberEditor;

public class Point2fEditor extends StaticSizeMonoDimensionEditor<Point2f, Float> {
	public Point2fEditor() {
		super(2);
	}

	@Override
	protected List<Float> getArrayFromValue(Point2f value) {
		return List.of(value.x, value.y);
	}

	@Override
	protected Supplier<NumberEditor<Float>> getEditorConstructor() {
		return FloatEditor::new;
	}

	@Override
	protected Point2f getValueFromArray(List<Float> datas) {
		return new Point2f(datas.get(0), datas.get(1));
	}
}
