/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.multiNumber;

public abstract class StaticSizeMonoDimensionEditor<T, U extends Number> extends MonoDimensionEditor<T, U> {

	public StaticSizeMonoDimensionEditor(int n) {
		super(n);
	}

	@Override
	protected int[] getDimensions(T object) {
		return new int[] { this.editors.size() };
	}

	@Override
	protected int getDimension(T object) {
		return this.editors.size();
	}

	@Override
	protected boolean hasFixedSize() {
		return true;
	}
}
