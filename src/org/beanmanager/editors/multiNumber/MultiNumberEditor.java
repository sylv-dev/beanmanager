/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.multiNumber;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import org.beanmanager.editors.PropertyEditor;
import org.beanmanager.editors.primitive.number.NumberEditor;

public abstract class MultiNumberEditor<T, U extends Number> extends PropertyEditor<T> {
	private static final String SEPARATOR = " ";
	ArrayList<NumberEditor<U>> editors;
	protected String[] names;

	public MultiNumberEditor(int numberOfValue) {
		if (numberOfValue < 1)
			throw new IllegalArgumentException("The number of value must be at least 1");
		updateNumberOfValue(numberOfValue);
	}

	protected abstract List<U> getArrayFromValue(T value);

	protected abstract int[] getDimensions();

	protected abstract int[] getDimensions(T object);

	protected abstract void setDimensions(int[] dimensions);

	protected abstract boolean hasFixedSize();

	// protected abstract void updateCustomEditor();

	protected abstract Supplier<NumberEditor<U>> getEditorConstructor();

	protected int getNumberOfValues() {
		return this.editors.size();
	}

	@Override
	public int getPitch() {
		return this.editors.get(0).getPitch() * this.editors.size();
	}

	protected abstract T getValueFromArray(List<U> datas);

	protected T getValueFromEditors() {
		int nbElement = this.editors.size();
		ArrayList<U> values = new ArrayList<>(nbElement);
		for (int i = 0; i < nbElement; i++) {
			U val = this.editors.get(i).getValue();
			if (val == null)
				return null;
			values.add(val);
		}
		return getValueFromArray(values);
	}

	@Override
	public boolean hasCustomEditor() {
		return true;
	}

	@Override
	public boolean isFixedControlSized() {
		return this.editors.get(0).isFixedControlSized();
	}

	public void setNames(String[] names) {
		this.names = names;
	}

	@Override
	public void setValue(T value) {
		super.setValue(value);
		if (value != null) {
			super.setValue(value);
			if (!hasFixedSize())
				setDimensions(getDimensions(value));
			updateEditorFromValues();
		}
	}

	@Override
	public void updateCustomEditor() {
		for (NumberEditor<U> propertyEditor : this.editors)
			propertyEditor.updateCustomEditor();
	}

	private void updateEditorFromValues() {
		List<U> array = getArrayFromValue(getValue());
		if (array.size() != this.editors.size())
			updateNumberOfValue(array.size());
		for (int i = 0; i < this.editors.size(); i++)
			if (!this.editors.get(i).getValue().equals(array.get(i)))
				this.editors.get(i).setValue(array.get(i));
	}

	void updateNumberOfValue(int numberOfValue) {
		if (this.editors != null && this.editors.size() == numberOfValue)
			return;
		this.editors = new ArrayList<>(numberOfValue);
		Supplier<NumberEditor<U>> editorConstructor = getEditorConstructor();
		for (int i = 0; i < numberOfValue; i++) {
			NumberEditor<U> editor = editorConstructor.get();
			editor.initWithDefaultValue();
			editor.addPropertyChangeListener(() -> super.setValue(getValueFromEditors()));
			this.editors.add(editor);
		}
		updateCustomEditor();
	}

	@Override
	public String getAsText() {
		StringBuilder t;
		int j;
		if (!hasFixedSize()) {
			int[] dims = getDimensions();
			t = new StringBuilder(Integer.toString(dims[0]));
			for (int i = 1; i < dims.length; i++)
				t.append(SEPARATOR + Integer.toString(dims[i]));
			j = 0;
		} else {
			t = new StringBuilder(this.editors.get(0).getAsText());
			j = 1;
		}
		for (int i = j; i < this.editors.size(); i++)
			t.append(SEPARATOR + this.editors.get(i).getAsText());
		return t.toString();
	}

	@Override
	public void setAsText(String text) {
		if (text == null || text.isEmpty() || text.equals(String.valueOf((Object) null))) {
			if (hasFixedSize())
				for (int i = 0; i < this.editors.size(); i++)
					this.editors.get(i).setAsText(null);
			else
				setValue(null);
		} else {
			String[] values = text.split(SEPARATOR);
			int i = 0;
			if (!hasFixedSize()) {
				int[] dimensions = getDimensions();
				hasFixedSize();
				while (i < dimensions.length)
					dimensions[i] = Integer.parseInt(values[i++]);
				setDimensions(dimensions);
			}
			int max = Math.min(values.length, this.editors.size()) + i;
			int editorIndex = 0;
			if (i == max)
				setValue(getValueFromArray(List.of()));
			else
				while (i < max)
					this.editors.get(editorIndex++).setAsText(values[i++]);
		}
	}

	@Override
	public T readValue(DataInput raf) throws IOException {
		if (!hasFixedSize()) {
			int[] dimensions = getDimensions();
			for (int i = 0; i < dimensions.length; i++)
				dimensions[i] = raf.readInt();
			setDimensions(dimensions);
		}
		int nbElement = this.editors.size();
		ArrayList<U> datas = new ArrayList<>(nbElement);
		for (int i = 0; i < nbElement; i++)
			datas.add(this.editors.get(i).readValue(raf));
		return getValueFromArray(datas);
	}

	@Override
	public void writeValue(DataOutput raf, T value) throws IOException {
		PropertyEditor<U> propertyEditor = getEditorConstructor().get();
		if (!hasFixedSize()) {
			int[] dimensions = getDimensions(value);
			for (int dim : dimensions)
				raf.writeInt(dim);
			setDimensions(dimensions);
		}
		for (U subValue : getArrayFromValue(value))
			propertyEditor.writeValue(raf, subValue);
	}

}
