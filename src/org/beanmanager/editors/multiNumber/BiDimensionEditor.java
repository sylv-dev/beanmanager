/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.multiNumber;

import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;

public abstract class BiDimensionEditor<T, U extends Number> extends MultiNumberEditor<T, U> {
	protected int nRow;
	protected int nCol;
	// private GridPane gp;

	public BiDimensionEditor(int nRow, int nCol) {
		super(nRow * nCol);
		this.nRow = nRow;
		this.nCol = nCol;
	}

	@Override
	protected GridPane getCustomEditor() {
		GridPane gp = new GridPane();
		updateCustomEditor(gp);
		return gp;
	}

	@Override
	public void updateCustomEditor() {
		updateCustomEditor((GridPane) customEditor());
	}

	public void updateCustomEditor(GridPane gp) {
		if (gp == null)
			return;
		gp.getChildren().clear();
		double prefWidth = -2;
		boolean useNames = this.names != null;
		if (useNames && this.names.length != this.editors.size()) {
			System.err.println(getClass().getSimpleName() + ": editor sub element names array size (" + this.names.length + ") must be equal to the number of sub elements (" + this.editors.size()
					+ "), so names are ignored");
			useNames = false;
		}
		for (int i = 0; i < this.nRow; i++)
			for (int j = 0; j < this.nCol; j++) {
				Region ce = this.editors.get(i * this.nCol + j).getNoSelectionEditor();
				if (prefWidth == -2)
					prefWidth = ce.getPrefWidth() / 2.0 + Math.max(0, 10 - this.nCol) * ce.getPrefWidth() / 20.0;
				ce.setPrefWidth(prefWidth);
				Tooltip.install(ce, new Tooltip(i + "-" + j));
				GridPane.setHgrow(ce, Priority.ALWAYS);
				if (useNames)
					gp.getChildren().add(new Label(this.names[i] + ":"));
				gp.add(ce, j, i);
			}
	}

	@Override
	protected boolean hasFixedSize() {
		return true;
	}

	@Override
	protected int[] getDimensions() {
		return new int[] { this.nRow, this.nCol };
	}

	@Override
	protected void setDimensions(int[] dimensions) {
		this.nRow = dimensions[0];
		this.nCol = dimensions[1];
		updateNumberOfValue(this.nRow * this.nCol);
	}
}
