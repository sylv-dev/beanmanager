/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.multiNumber;

public abstract class DynamicSizeMonoDimensionEditor<T, U extends Number> extends MonoDimensionEditor<T, U> {

	public DynamicSizeMonoDimensionEditor(int size) {
		super(size);
	}

	@Override
	protected boolean hasFixedSize() {
		return false;
	}

	@Override
	public void setValue(T value) {
		if (value != null) {
			int size = getDimension(value);
			if (size != this.editors.size())
				updateNumberOfValue(size);
		}
		super.setValue(value);
	}

	@Override
	public void updateSize(int size) {
		updateNumberOfValue(size);
	}

	@Override
	protected int[] getDimensions(T gVector) {
		return new int[] { getDimension(gVector) };
	}

	@Override
	public String getDescription() {
		return "Size:" + this.editors.size();
	}
}
