/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.multiNumber.vector;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import javax.vecmath.GVector;

import org.beanmanager.editors.multiNumber.DynamicSizeMonoDimensionEditor;
import org.beanmanager.editors.primitive.number.DoubleEditor;
import org.beanmanager.editors.primitive.number.NumberEditor;

public class GVectorEditor extends DynamicSizeMonoDimensionEditor<GVector, Double> {
	public GVectorEditor() {
		super(5);
	}

	@Override
	protected ArrayList<Double> getArrayFromValue(GVector value) {
		int nbValue = getNumberOfValues();
		ArrayList<Double> array = new ArrayList<>(nbValue);
		for (int i = 0; i < nbValue; i++)
			array.add(value.getElement(i));
		return array;
	}

	@Override
	protected Supplier<NumberEditor<Double>> getEditorConstructor() {
		return DoubleEditor::new;
	}

	@Override
	protected GVector getValueFromArray(List<Double> datas) {
		double[] data = new double[datas.size()];
		for (int i = 0; i < datas.size(); i++)
			data[i] = datas.get(i);
		return new GVector(data);
	}

	@Override
	protected int getDimension(GVector gVector) {
		return gVector.getSize();
	}
}
