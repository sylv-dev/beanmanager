/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.multiNumber;

public abstract class DynamicSizeBiDimensionEditor<T, U extends Number> extends BiDimensionEditor<T, U> {

	public DynamicSizeBiDimensionEditor(int nRow, int nCol) {
		super(nRow, nCol);
	}

	@Override
	protected boolean hasFixedSize() {
		return false;
	}

	public void updateSizes(int... sizes) {
		this.nRow = sizes[0];
		this.nCol = sizes[1];
		updateNumberOfValue(this.nRow * this.nCol);
	}

	@Override
	public String getDescription() {
		return "Size:" + this.nRow + "x" + this.nCol;
	}
}
