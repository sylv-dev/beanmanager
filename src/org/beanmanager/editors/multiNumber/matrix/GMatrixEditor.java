/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.multiNumber.matrix;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import javax.vecmath.GMatrix;

import org.beanmanager.editors.multiNumber.DynamicSizeBiDimensionEditor;
import org.beanmanager.editors.primitive.number.DoubleEditor;
import org.beanmanager.editors.primitive.number.NumberEditor;

public class GMatrixEditor extends DynamicSizeBiDimensionEditor<GMatrix, Double> {
	public GMatrixEditor() {
		super(5, 5);
	}

	@Override
	protected List<Double> getArrayFromValue(GMatrix value) {
		GMatrix mat = value;
		ArrayList<Double> array = new ArrayList<>(mat.getNumRow() * mat.getNumCol());
		for (int row = 0; row < mat.getNumRow(); row++)
			for (int col = 0; col < mat.getNumCol(); col++)
				array.add(mat.getElement(row, col));
		return array;
	}

	@Override
	protected Supplier<NumberEditor<Double>> getEditorConstructor() {
		return DoubleEditor::new;
	}

	@Override
	protected GMatrix getValueFromArray(List<Double> datas) {
		double[] data = new double[datas.size()];
		for (int i = 0; i < datas.size(); i++)
			data[i] = datas.get(i);
		return new GMatrix(this.nRow, this.nCol, data);
	}

	@Override
	protected int[] getDimensions(GMatrix object) {
		return new int[] { object.getNumRow(), object.getNumCol() };
	}
}
