/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.multiNumber.matrix;

import java.util.List;
import java.util.function.Supplier;

import javax.vecmath.Matrix4d;

import org.beanmanager.editors.multiNumber.StaticSizeBiDimensionEditor;
import org.beanmanager.editors.primitive.number.DoubleEditor;
import org.beanmanager.editors.primitive.number.NumberEditor;

public class Matrix4dEditor extends StaticSizeBiDimensionEditor<Matrix4d, Double> {
	public Matrix4dEditor() {
		super(4, 4);
	}

	@Override
	protected List<Double> getArrayFromValue(Matrix4d value) {
		return List.of(value.m00, value.m01, value.m02, value.m03, value.m10, value.m11, value.m12, value.m13, value.m20, value.m21, value.m22, value.m23, value.m30, value.m31, value.m32, value.m33);
	}

	@Override
	protected Supplier<NumberEditor<Double>> getEditorConstructor() {
		return DoubleEditor::new;
	}

	@Override
	protected Matrix4d getValueFromArray(List<Double> datas) {
		return new Matrix4d(datas.get(0), datas.get(1), datas.get(2), datas.get(3), datas.get(4), datas.get(5), datas.get(6), datas.get(7), datas.get(8), datas.get(9), datas.get(10), datas.get(11),
				datas.get(12), datas.get(13), datas.get(14), datas.get(15));
	}
}
