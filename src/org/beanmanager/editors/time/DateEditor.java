/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.time;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.beanmanager.editors.PropertyEditor;
import org.beanmanager.editors.primitive.number.ControlType;
import org.beanmanager.editors.primitive.number.LongEditor;
import org.beanmanager.ihmtest.FxTest;
import org.beanmanager.tools.FxUtils;

import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.util.converter.TimeStringConverter;

public class DateEditor extends PropertyEditor<Date> {
	private SimpleDateFormat dateFormat;
	private TextField textField;
	private long baseTime;
	private String style;
	private LongEditor longEditor;
	private long min = Long.MIN_VALUE;
	private long max = Long.MAX_VALUE;
	private String timePattern;

	boolean ignoreTimePattern = false;

	public static void main(String[] args) {
		System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
		System.out.println(System.currentTimeMillis());
		DateEditor te = new DateEditor();
		te.setValue(new Date(System.currentTimeMillis()));
		te.setMax(System.currentTimeMillis() + 1000);
		te.setTimePattern("HH:mm:ss.SSS");
		// new Thread(() -> {
		// try {
		// Thread.sleep(3000);
		// Platform.runLater(() -> {
		// te.setMin(System.currentTimeMillis() - 5000);
		// te.setMax(System.currentTimeMillis() + 5000);
		// te.setTimePattern(null);
		// });
		// Thread.sleep(3000);
		// Platform.runLater(() -> {
		//// te.setMin(5);
		//// te.setMax(15);
		// te.setTimePattern("HH:mm:ss.SSS");
		// });
		// } catch (InterruptedException ex) {
		// ex.printStackTrace();
		// }
		// }).start();
		new FxTest().launchIHM(args, s -> new VBox(te.getEditor(), new DateEditor().getEditor()));
	}

	public DateEditor() {}

	public DateEditor(long min, long max, String timePattern) {
		this.min = min;
		this.max = max;
		setTimePattern(timePattern);
		this.timePattern = timePattern;
	}

	@Override
	public String getAsText() {
		return getValue() == null ? null : Long.toString(getValue().getTime());
	}

	@Override
	protected HBox getCustomEditor() {
		HBox hBox = new HBox();
		populateHBox(hBox);
		return hBox;
	}

	private void populateHBox(HBox hBox) {
		this.longEditor = null;
		this.textField = null;
		MenuItem timeContexItem = new MenuItem(this.ignoreTimePattern ? "As Time" : "As Integer");
		timeContexItem.setOnAction(e -> setIgnoreTimePattern(!this.ignoreTimePattern));
		if (this.timePattern == null || this.ignoreTimePattern) {
			this.longEditor = new LongEditor(this.min, this.max, ControlType.TEXTFIELD);
			this.longEditor.setPrimitive(false);
			this.longEditor.setValue(getValue() != null ? getValue().getTime() : null);
			this.longEditor.addPropertyChangeListener(() -> setValue(this.longEditor.getValue() == null ? null : new Date(this.longEditor.getValue())));
			this.longEditor.setStyle(this.style);
			Region le = this.longEditor.getNoSelectionEditor();
			TextField control = (TextField) le.getChildrenUnmodifiable().get(0);
			ContextMenu contextMenu = control.getContextMenu();
			if (contextMenu == null) {
				contextMenu = new ContextMenu();
				control.setContextMenu(contextMenu);
			}
			contextMenu.getItems().add(0, timeContexItem);
			HBox.setHgrow(le, Priority.ALWAYS);
			hBox.getChildren().setAll(le);
		} else {
			this.textField = new TextField();
			TextFormatter<Date> form = new TextFormatter<>(new TimeStringConverter(this.dateFormat));
			this.textField.setTextFormatter(form);
			this.textField.focusedProperty().addListener((ov, oldValue, newValue) -> {
				if (!newValue && this.textField != null)
					updateValueFromTextField(this.textField);
				updateGUI();
			});
			this.textField.setOnScroll(e -> {
				if (e.getDeltaY() != 0)
					incrValue(e.getDeltaX() + e.getDeltaY() >= 0, e.isControlDown(), e.isShiftDown());
				e.consume();
			});
			MenuItem minMenuItem = new MenuItem("set To min");
			minMenuItem.setOnAction(e -> setValue(new Date(getMin())));
			MenuItem maxMenuItem = new MenuItem("set to max");
			maxMenuItem.setOnAction(e -> setValue(new Date(getMax())));
			this.textField.setContextMenu(new ContextMenu(timeContexItem, minMenuItem, maxMenuItem));
			this.textField.textProperty().addListener(e -> updateStyle());
			this.textField.setOnKeyPressed(e -> {
				KeyCode c = e.getCode();
				if (c == KeyCode.ENTER) {
					updateValueFromTextField(this.textField);
					FxUtils.traverse();
					e.consume();
				} else if (c == KeyCode.UP || c == KeyCode.DOWN) {
					incrValue(c == KeyCode.UP, e.isControlDown(), e.isShiftDown());
					e.consume();
				}
			});
			if (this.style != null)
				this.textField.setStyle(this.style);
			updateCustomEditor();
			// updateBase();
			HBox.setHgrow(this.textField, Priority.ALWAYS);
			hBox.getChildren().setAll(this.textField);
		}
	}

	private void updateValueFromTextField(TextField textField2) {
		try {
			String text = this.textField.getText();
			updateValue(text.isEmpty() ? null : this.dateFormat.parse(text));
		} catch (ParseException ex) {}
	}

	private void incrValue(boolean positive, boolean controlDown, boolean shiftDown) {
		Date value = getValue();
		if (value == null)
			return;
		int inc = shiftDown && controlDown ? 100 : shiftDown ? 1000 : controlDown ? 10 : 1;
		Date newVal = new Date(value.getTime() + (positive ? inc : -inc));
		if (isInBounds(newVal) == 0)
			setValue(newVal);
	}

	@Override
	public void setAsText(String text) {
		setValue(text == null || text.isEmpty() || text.equals(String.valueOf((Object) null)) ? null : new Date(Long.parseLong(text))); // null sinon player timeB pas bon
	}

	public long getMin() {
		return this.min;
	}

	public void setMin(long min) {
		this.min = min;
		checkBounds();
		updateBaseTime();
		if (this.longEditor != null)
			this.longEditor.setMin(min);
	}

	public long getMax() {
		return this.max;
	}

	public void setMax(long max) {
		this.max = max;
		checkBounds();
		if (this.longEditor != null)
			this.longEditor.setMax(max);
	}

	private void checkBounds() {
		if (this.max == Long.MIN_VALUE)
			this.max++;
		if (this.min == Long.MAX_VALUE)
			this.min--;
		if (this.max <= this.min)
			this.max = this.min;
		updateValueWithBounds();
	}

	protected void updateValueWithBounds() {
		Date val = getValue();
		if (val != null) {
			int comp = isInBounds(val);
			if (comp != 0)
				setValue(comp == -1 ? new Date(this.min) : new Date(this.max));
		}
	}

	private int isInBounds(Date value) {
		long time = value.getTime();
		return time < this.min ? -1 : time > this.max ? 1 : 0;
	}

	public void setTimePattern(String timePattern) {
		this.timePattern = timePattern;
		updateBaseTime();
		HBox hBox = (HBox) customEditor();
		if (hBox != null)
			FxUtils.runLaterIfNeeded(() -> populateHBox(hBox));
	}

	private void updateBaseTime() {
		long baseTime = 0;
		if (this.timePattern != null) {
			this.dateFormat = new SimpleDateFormat(this.timePattern);
			try {
				baseTime = this.min - this.dateFormat.parse(this.dateFormat.format(this.min)).getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		this.baseTime = baseTime;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	@Override
	public boolean hasCustomEditor() {
		return true;
	}

	@Override
	public void setValue(Date value) {
		if (value != null) {
			int comp = isInBounds(value);
			if (comp != 0)
				value = comp == -1 ? new Date(this.min) : new Date(this.max);
		}
		super.setValue(value);
		// updateBase();
	}

	@Override
	public void updateCustomEditor() {
		Date time = getValue();
		if (this.longEditor != null)
			this.longEditor.setValue(time == null ? null : time.getTime());
		else if (this.textField != null)
			this.textField.setText(time == null ? "" : this.dateFormat.format(time));
	}

	protected void updateStyle() {
		if (this.textField == null)
			return;
		Date valFromText = null;
		boolean badFormatted = false;
		long time = 0;
		String tfText = this.textField.getText();
		try {
			valFromText = this.dateFormat.parse(tfText);
			badFormatted = !this.dateFormat.format(valFromText).equals(tfText);
			time = this.baseTime + valFromText.getTime();
		} catch (ParseException ex) {
			badFormatted = !tfText.isEmpty();
		}
		String style = "-fx-text-fill: " + (badFormatted || valFromText != null && (time < this.min || time > this.max) ? "red"
				: tfText.equals(getValue() == null ? "" : this.dateFormat.format(getValue())) ? "black" : "green") + ";";
		if (this.style != null)
			style = this.style + style;
		if (!style.equals(this.textField.getStyle()))
			this.textField.setStyle(style);
	}

	private boolean updateValue(Date date) {
		if (date == null) {
			setValue(null);
			return true;
		}
		if (this.timePattern != null)
			date = new Date(this.baseTime + date.getTime());
		if (isInBounds(date) == 0 && !date.equals(getValue())) {
			setValue(date);
			return true;
		}
		return false;
	}

	public void setIgnoreTimePattern(boolean ignoreTimePattern) {
		this.ignoreTimePattern = ignoreTimePattern;
		populateHBox((HBox) customEditor());
	}

	public boolean isIgnoreTimePattern() {
		return this.ignoreTimePattern;
	}

	public SimpleDateFormat getDateFormat() {
		return this.ignoreTimePattern ? null : this.dateFormat;
	}

	@Override
	public String getDescription() {
		SimpleDateFormat formatter = getDateFormat();
		return formatter == null ? getMin() + " -> " + getMax() : formatter.format(getMin()) + " -> " + formatter.format(getMax());
	}
}
