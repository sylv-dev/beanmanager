/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.time;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.time.LocalDate;

import org.beanmanager.editors.PropertyEditor;
import org.beanmanager.ihmtest.FxTest;
import org.beanmanager.tools.FxUtils;

import javafx.geometry.Pos;
import javafx.scene.control.DatePicker;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

public class LocalDateEditor extends PropertyEditor<LocalDate> {

	public static void main(String[] args) {
		new FxTest().launchIHM(args, s -> {
			VBox vbox = new VBox(new LocalDateEditor().getCustomEditor(), new LocalDateEditor().getCustomEditor());
			vbox.setAlignment(Pos.CENTER);
			return vbox;
		});
	}

	@Override
	public String getAsText() {
		return getValue() == null ? "" : getValue().toString();
	}

	@Override
	protected Region getCustomEditor() {
		DatePicker dp = new DatePicker();
		dp.setValue(getValue());
		dp.setOnAction(e -> setValue(dp.getValue()));
		dp.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.ENTER) {
				FxUtils.traverse();
				e.consume();
			}
		});
		dp.setPrefWidth(120);
		dp.setMaxWidth(Double.MAX_VALUE);
		return dp;
	}

	@Override
	public int getPitch() {
		return Integer.BYTES + Byte.BYTES * 2;
	}

	@Override
	public boolean hasCustomEditor() {
		return true;
	}

	@Override
	public boolean isFixedControlSized() {
		return true;
	}

	@Override
	public LocalDate readValue(DataInput raf) throws IOException {
		return LocalDate.of(raf.readInt(), raf.readByte(), raf.readByte());
	}

	@Override
	public void setAsText(String text) {
		setValue(text == null || text.isEmpty() ? null : LocalDate.parse(text));
	}

	@Override
	public void updateCustomEditor() {
		((DatePicker) customEditor()).setValue(getValue());
	}

	@Override
	public void writeValue(DataOutput raf, LocalDate value) throws IOException {
		raf.writeInt(value.getYear());
		raf.writeByte(value.getMonthValue());
		raf.writeByte(value.getDayOfMonth());
	}

}
