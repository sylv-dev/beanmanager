/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors;

import java.util.WeakHashMap;

import javax.swing.event.EventListenerList;

import org.beanmanager.tools.FxUtils;

public interface DynamicAnnotationBean {
	static WeakHashMap<Object, EventListenerList> LISTENERS = new WeakHashMap<>();

	public static void addAnnotationChangeListener(Object bean, DynamicAnnotationBeanChangeListener listener) {
		EventListenerList list = LISTENERS.get(bean);
		if (list == null) {
			list = new EventListenerList();
			LISTENERS.put(bean, list);
		}
		list.add(DynamicAnnotationBeanChangeListener.class, listener);
	}

	public static void removeAnnotationChangeListener(Object bean, DynamicAnnotationBeanChangeListener listener) {
		EventListenerList list = LISTENERS.get(bean);
		if (list == null)
			return;
		list.remove(DynamicAnnotationBeanChangeListener.class, listener);
	}

	default void fireAnnotationChanged(Object bean, String propertyName) {
		FxUtils.runLaterIfNeeded(() -> {
			EventListenerList list = LISTENERS.get(bean);
			if (list == null)
				return;
			for (DynamicAnnotationBeanChangeListener listener : list.getListeners(DynamicAnnotationBeanChangeListener.class))
				listener.dynamicAnnotationChange(propertyName);
		});
	}
}
