/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.container;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.lang.reflect.Array;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.vecmath.Point2i;

import org.beanmanager.BeanListChangeListener;
import org.beanmanager.BeanManager;
import org.beanmanager.editors.GenericPropertyEditor;
import org.beanmanager.editors.PropertyEditor;
import org.beanmanager.editors.PropertyEditorManager;
import org.beanmanager.editors.primitive.number.ControlType;
import org.beanmanager.editors.primitive.number.IntegerEditor;
import org.beanmanager.ihmtest.FxTest;

import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.control.skin.TreeCellSkin;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

//TODO purge des bean à partir d'un module
public class ArrayEditor<T> extends PropertyContainerEditor<T> implements BeanListChangeListener {
	private static final String REMOVE = "remove";
	private static final String INSERT_AN_ELEMENT_INSIDE = "insert an element inside";
	private static final String INSERT_MULTIPLE_ELEMENT_BELOW = "insert multiple element below";
	private static final String INSERT_AN_ELEMENT_BELOW = "insert an element below";
	private static final String INSERT_MULTIPLE_ELEMENT_ABOVE = "insert multiple element above";
	private static final String INSERT_AN_ELEMENT_ABOVE = "insert an element above";
	public static final String SIZE_TO_DATA_SEPARATOR = " ";
	private static final String SUB_ARRAY = "[]";
	private static final String EMPTY = "-";
	private static final String EMPTY_ARRAY = "[" + EMPTY + "]";
	private static final String RANGE = "...";
	private static final int DELETE = 0;
	private static final int INSERT_ABOVE = 2;
	private static final int INSERT_BELOW = 3;
	private static final int INSERT_MULTIPLE_ABOVE = 4;
	private static final int INSERT_MULTIPLE_BELOW = 5;
	private static final int INSERT_INSIDE = 6;
	private static final double BASE_OFFSET = 18;
	private static final Insets LIST_INSETS = new Insets(0, 0, 0, -BASE_OFFSET);
	private static final Insets TREE_INSETS = new Insets(0, 0, 0, -3);
	static final Insets NULL_INSETS = new Insets(0);

	private final int arrayLevel;
	private final int nbGenericParameters;
	// private final EventListenerList listeners = new EventListenerList();
	private Boolean isSubEditorOnlyContainsAllowedCharacter;
	private int prefHeight = 26 * 5 + 2;

	public ArrayEditor(Class<T> type) {
		super(getComponentType(type));
		this.arrayLevel = getArrayLevel(type);
		this.nbGenericParameters = getComponentType().getTypeParameters().length;
	}

	public ArrayEditor(Class<T> type, Function<Class<?>, PropertyEditor<?>> patternEditorFactory) {
		this(type);
		initPatternEditorFactory(patternEditorFactory);
	}

	@Override
	public void beanListChanged() {
		// fireBeanChange();
	}

	@Override
	public String getAsText() {
		return getAsText(this.arrayLevel, this.nbGenericParameters != 0);
	}

	private String getAsText(int arrayLevel, boolean hasParameters) {
		Object value = super.getValue();
		if (value == null)
			return null;
		StringBuilder stringValue = new StringBuilder(PropertyEditor.OPEN_GROUP);
		PropertyEditor<?> editor = null;
		int size = Array.getLength(value);
		for (int i = 0; i < size; i++) {
			Object tabValue = Array.get(value, i);
			if (tabValue != null) {
				PropertyEditor<?> currentEditor;
				if (hasParameters)
					currentEditor = PropertyEditorManager.findEditorWithValue(tabValue, this.local); // Gére bien si tableau vide
				else {
					if (editor == null)
						editor = arrayLevel != 1 ? new ArrayEditor<>(getTypeArray(getComponentType(), arrayLevel - 1), this.patternEditorFactory) : getPropertyEditor();
					currentEditor = editor;
				}
				currentEditor.setValueFromObj(tabValue);
				String subStringValue = currentEditor.getAsText();
				if (currentEditor.canContainForbiddenCharacter())
					subStringValue = subStringValue.length() + SIZE_TO_DATA_SEPARATOR + subStringValue;
				stringValue.append((subStringValue == null ? String.valueOf((Object) null) : subStringValue) + PropertyEditor.GROUP_SEPARATOR);
			} else if (arrayLevel != 1)
				stringValue.append("_" + PropertyEditor.GROUP_SEPARATOR);
			else
				stringValue.append(PropertyEditor.GROUP_SEPARATOR);
		}
		stringValue.append(CLOSE_GROUP);
		return stringValue.toString();
	}

	private Class<?> getTypeArray(Class<?> componentType, int i) {
		Class<?> type = getComponentType();
		for (int k = 0; k < i; k++)
			type = Array.newInstance(type, 0).getClass();
		return type;
	}

	@Override
	protected Region getCustomEditor() {
		return getTree(true);
	}

	@Override
	protected Region getCustomView() {
		return getTree(false);
	}

	@Override
	public boolean canGrow() {
		return true;
	}

	private static int getIndexesFromTreeItem(ArrayTreeItem item, int indexFromEnd) {
		String l = (String) item.getValue();
		while (l.endsWith(SUB_ARRAY))
			l = l.substring(0, l.length() - SUB_ARRAY.length());
		String[] indexes = l.replace(SUB_ARRAY, "[_]").replace("[", " ").replace("]", "").split(" ");
		while (indexes[indexes.length - indexFromEnd].contains(RANGE) || indexes[indexes.length - indexFromEnd].contains(EMPTY))
			indexFromEnd++;
		return Integer.parseInt(indexes[indexes.length - indexFromEnd]);
	}

	private static int getIndexOfTreeItem(ArrayTreeItem item) {
		return getIndexesFromTreeItem(item, 1);
	}

	private static int getIndexOfTreeItemInParent(ArrayTreeItem item) {
		return getIndexesFromTreeItem(item, 2);
	}

	private void getMaxLabelSize(Object tab, int maxLevel, int level, MutableInteger maxSize) {
		if (tab == null)
			return;
		int length = Array.getLength(tab);
		if (maxLevel == level) {
			if (length > maxSize.value)
				maxSize.value = length;
		} else
			for (int i = 0; i < length; i++)
				getMaxLabelSize(Array.get(tab, i), maxLevel, level + 1, maxSize);
	}

	protected Object getObjValue() {
		return super.getValue();
	}

	PropertyEditor<?> getPropertyEditor() {
		return super.getPropertyEditorClone(null);
	}

	private HashSet<Object> getSubArray(Object value) {
		HashSet<Object> arrayCandidateForSuppression = new HashSet<>();
		int arrayLength = Array.getLength(value);
		for (int i = 0; i < arrayLength; i++) {
			Object val = Array.get(value, i);
			if (val != null)
				if (val.getClass().getComponentType().isArray())
					arrayCandidateForSuppression.addAll(getSubArray(val));
			arrayCandidateForSuppression.add(val);
		}
		arrayCandidateForSuppression.add(value);
		return arrayCandidateForSuppression;
	}

	private Region getTree(boolean editable) {
		Object tab = getValue();
		ArrayTreeItem rootItem = new ArrayTreeItem(this, "", 0, tab == null ? -1 : Array.getLength(tab), tab, editable);
		TreeView<Object> treeView = new TreeView<>(rootItem);
		treeView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		treeView.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.DELETE)
				modifyArrayStruct(treeView, DELETE);
		});
		ContextMenu elementContextMenu;
		ContextMenu lastLevelElementContextMenu;
		ContextMenu rangeContextMenu;
		if (editable) {
			MenuItem miea = new MenuItem(INSERT_AN_ELEMENT_ABOVE);
			miea.setAccelerator(new KeyCodeCombination(KeyCode.ADD, KeyCombination.SHIFT_DOWN));
			miea.setOnAction(e -> modifyArrayStruct(treeView, INSERT_ABOVE));
			MenuItem mimea = new MenuItem(INSERT_MULTIPLE_ELEMENT_ABOVE);
			mimea.setOnAction(e -> modifyArrayStruct(treeView, INSERT_MULTIPLE_ABOVE));
			MenuItem mieb = new MenuItem(INSERT_AN_ELEMENT_BELOW);
			mieb.setAccelerator(new KeyCodeCombination(KeyCode.ADD, KeyCombination.CONTROL_DOWN));
			mieb.setOnAction(e -> modifyArrayStruct(treeView, INSERT_BELOW));
			MenuItem mimeb = new MenuItem(INSERT_MULTIPLE_ELEMENT_BELOW);
			mimeb.setOnAction(e -> modifyArrayStruct(treeView, INSERT_MULTIPLE_BELOW));
			MenuItem miiei = new MenuItem(INSERT_AN_ELEMENT_INSIDE);
			miiei.setOnAction(e -> modifyArrayStruct(treeView, INSERT_INSIDE));
			miiei.setAccelerator(new KeyCodeCombination(KeyCode.ADD, KeyCombination.ALT_DOWN));
			MenuItem mir = new MenuItem(REMOVE);
			mir.setOnAction(e -> modifyArrayStruct(treeView, DELETE));
			mir.setAccelerator(new KeyCodeCombination(KeyCode.SUBTRACT, KeyCombination.CONTROL_DOWN));
			elementContextMenu = new ContextMenu(miea, mimea, mieb, mimeb, miiei, mir);

			miea = new MenuItem(INSERT_AN_ELEMENT_ABOVE);
			miea.setAccelerator(new KeyCodeCombination(KeyCode.ADD, KeyCombination.SHIFT_DOWN));
			miea.setOnAction(e -> modifyArrayStruct(treeView, INSERT_ABOVE));
			mimea = new MenuItem(INSERT_MULTIPLE_ELEMENT_ABOVE);
			mimea.setOnAction(e -> modifyArrayStruct(treeView, INSERT_MULTIPLE_ABOVE));
			mieb = new MenuItem(INSERT_AN_ELEMENT_BELOW);
			mieb.setAccelerator(new KeyCodeCombination(KeyCode.ADD, KeyCombination.CONTROL_DOWN));
			mieb.setOnAction(e -> modifyArrayStruct(treeView, INSERT_BELOW));
			mimeb = new MenuItem(INSERT_MULTIPLE_ELEMENT_BELOW);
			mimeb.setOnAction(e -> modifyArrayStruct(treeView, INSERT_MULTIPLE_BELOW));
			mir = new MenuItem(REMOVE);
			mir.setOnAction(e -> modifyArrayStruct(treeView, DELETE));
			mir.setAccelerator(new KeyCodeCombination(KeyCode.SUBTRACT, KeyCombination.CONTROL_DOWN));
			lastLevelElementContextMenu = new ContextMenu(miea, mimea, mieb, mimeb, mir);

			MenuItem mris = new MenuItem(REMOVE);
			mris.setOnAction(e -> modifyArrayStruct(treeView, DELETE));
			rangeContextMenu = new ContextMenu(mris);
		} else {
			elementContextMenu = null;
			lastLevelElementContextMenu = null;
			rangeContextMenu = null;
		}

		MutableInteger maxLabel = new MutableInteger();
		getMaxLabelSize(tab, this.arrayLevel - 1, 0, maxLabel);
		Text lab = new Text("[" + (maxLabel.value - 1) + "] = ");
		lab.setFont(new Label().getFont());
		double maxLabelSize = lab.getBoundsInLocal().getWidth();
		double compSize = editable ? getDefaultPatternEditor(null).getNoSelectionEditor().getPrefWidth() : getDefaultPatternEditor(null).getView().getPrefWidth();
		if (compSize == -1)
			compSize = 150;
		double maxHboxSize = maxLabelSize + compSize + 3;
		var treeIndentSize = new Object() {
			double value = -1;
		};
		double scrollBarSize = 16;
		treeView.setCenterShape(true);

		treeView.setCellFactory(p -> new TreeCell<>() {
			protected javafx.scene.control.Skin<?> createDefaultSkin() {
				TreeCellSkin<?> skin = new TreeCellSkin<>(this);
				skin.indentProperty().addListener(a -> {
					if (treeIndentSize.value != skin.getIndent()) {
						treeIndentSize.value = skin.getIndent();
						ArrayTreeItem rootNode = (ArrayTreeItem) treeView.getRoot();
						int nbLevel = -1;
						if (rootNode != null) {
							int[] maxTabSize = new int[ArrayEditor.this.arrayLevel];
							populateMaxSubTabLength(ArrayEditor.this.arrayLevel - 1, tab, maxTabSize, 0);
							for (int i = 0; i < maxTabSize.length; i++)
								nbLevel += rootNode.getLevel(0, maxTabSize[i]) + 1;
						}
						treeView.setPrefWidth(BASE_OFFSET + treeIndentSize.value * nbLevel + maxHboxSize + scrollBarSize);
					}
				});
				return skin;
			};

			private void populateMaxSubTabLength(int maxLevel, Object tab, int[] maxTabSize, int level) {
				if (tab == null)
					return;
				int length = Array.getLength(tab);
				if (maxTabSize[level] < length)
					maxTabSize[level] = length;
				if (level != maxLevel)
					for (int i = 0; i < length; i++) {
						Object val = Array.get(tab, i);
						if (val != null)
							populateMaxSubTabLength(maxLevel, val, maxTabSize, level + 1);
					}
			};

			protected void updateItem(Object item, boolean empty) {
				super.updateItem(item, empty);
				setCenterShape(true);
				setPadding(NULL_INSETS);
				if (empty) {
					setText(null);
					setGraphic(null);
					if (editable)
						setContextMenu(null);
				} else {
					TreeItem<Object> ti = getTreeItem();
					if (ti.getValue() instanceof String) {
						setText((String) ti.getValue());
						setGraphic(ti.getGraphic());
						if (editable)
							setContextMenu(ti instanceof ArrayTreeItem && !((ArrayTreeItem) ti).isRangeItem() ? elementContextMenu : rangeContextMenu);
					} else {
						HBox box = new HBox();
						box.getChildren().addAll(ti.getGraphic(), (Region) ti.getValue());
						box.setAlignment(Pos.CENTER_LEFT);
						box.setPadding(ArrayEditor.this.arrayLevel == 1 && Array.getLength(getValue()) < 100 ? LIST_INSETS : TREE_INSETS);
						setText(null);
						setGraphic(box);
						if (editable)
							setContextMenu(lastLevelElementContextMenu);
					}
				}
			}
		});
		treeView.setShowRoot(false);
		if (editable) {
			StackPane bp = new StackPane();
			bp.getChildren().add(treeView);
			bp.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			treeView.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			treeView.setPrefHeight(this.prefHeight); // TODO
			updateCustomEditor(bp.getChildren());
			return bp;
		}
		updateCustomView(treeView);
		return treeView;
	}

	@Override
	public boolean hasCustomEditor() {
		return true;
	}

	@Override
	public boolean hasCustomView() {
		return true;
	}

	private boolean isSubEditorOnlyContainsAllowedCharacter() {
		if (this.isSubEditorOnlyContainsAllowedCharacter == null) {
			PropertyEditor<?> ed = getDefaultPatternEditor(this.nbGenericParameters == 0 ? null : () -> new Class<?>[] { Void.class });
			this.isSubEditorOnlyContainsAllowedCharacter = !ed.canContainForbiddenCharacter();
		}
		return this.isSubEditorOnlyContainsAllowedCharacter;
	}

	private static void localizeArray(ArrayTreeItem parent, Object tab, BiConsumer<Object, Integer> biConsumer) {
		ArrayTreeItem root = parent;
		while (root.getParent() != null && ((ArrayTreeItem) root.getParent()).tab == tab)
			root = (ArrayTreeItem) root.getParent();
		if (root.getParent() == null)
			biConsumer.accept(null, -1);
		else
			biConsumer.accept(((ArrayTreeItem) root.getParent()).tab, getIndexOfTreeItemInParent(root));
	}

	private boolean localizeTreeItem(ArrayTreeItem rootTreeItem, Object tab, int index, Consumer<TreeItem<Object>> consumer) {
		if (rootTreeItem == null)
			return false;
		ObservableList<TreeItem<Object>> children = rootTreeItem.getChildren();
		if (rootTreeItem.tab == tab && !children.isEmpty()/* && !(children.get(0) instanceof ArrayTreeItem) */) {
			int indexOfChildren = index - rootTreeItem.begin;
			if (indexOfChildren < children.size())
				consumer.accept(children.get(indexOfChildren));
			return true;
		}
		for (TreeItem<Object> treeItem : children)
			if (treeItem instanceof ArrayTreeItem) {
				ArrayTreeItem ati = (ArrayTreeItem) treeItem;
				if (localizeTreeItem(ati, tab, index, consumer))
					return true;
			}
		return false;
	}

	private void modifyArrayStruct(TreeView<Object> treeView, int modificationType) {
		if (modificationType == DELETE) {
			HashSet<Object> arrayCandidateForSuppression = new HashSet<>();
			HashMap<Object, ArraySuppressionInfo> modifications = new HashMap<>();
			for (TreeItem<Object> item : treeView.getSelectionModel().getSelectedItems()) {
				ArrayTreeItem parent = (ArrayTreeItem) item.getParent();
				Object tab = parent.tab;
				if (arrayCandidateForSuppression.contains(tab))
					continue;
				ArraySuppressionInfo modifs = modifications.get(tab);
				if (modifs == null) {
					var cell = new Object() {
						private ArraySuppressionInfo arraySuppression;
					};
					localizeArray(parent, tab, (parentTab, parentTabIndex) -> cell.arraySuppression = new ArraySuppressionInfo(parentTab, parentTabIndex));
					modifs = cell.arraySuppression;
					modifications.put(tab, modifs);
				}

				Point2i range;
				Node graphic = item.getGraphic();
				if (graphic == null) {
					ArrayTreeItem arrayTreeItem = (ArrayTreeItem) item;
					if (tab.getClass().getComponentType().isArray()) {
						HashSet<Object> newArrayCandidateForSuppression = new HashSet<>();
						int begin;
						int end;
						if (arrayTreeItem.isRangeItem()) {
							begin = arrayTreeItem.begin;
							end = arrayTreeItem.end;
						} else {
							begin = getIndexOfTreeItemInParent(arrayTreeItem);
							end = begin + 1;
						}
						for (int i = begin; i < end; i++)
							if (i < Array.getLength(arrayTreeItem.tab)) {
								Object val = Array.get(arrayTreeItem.tab, i);
								if (val != null && val.getClass().getComponentType().isArray())
									newArrayCandidateForSuppression.addAll(getSubArray(Array.get(arrayTreeItem.tab, i)));
							}
						for (Object tabToChange : modifications.keySet())
							if (newArrayCandidateForSuppression.contains(tabToChange))
								modifications.remove(tabToChange);
						arrayCandidateForSuppression.addAll(newArrayCandidateForSuppression);
						range = new Point2i(begin, end);
					} else
						range = new Point2i(arrayTreeItem.begin, arrayTreeItem.end);
				} else {
					String indexText = ((Label) graphic).getText();
					int index = Integer.parseInt(indexText.substring(1, indexText.indexOf("]")));
					range = new Point2i(index, index + 1);
				}
				modifs.addRangeToBeDeleted(range);
			}
			Object newwRootTab = null;
			for (Object oriTab : modifications.keySet()) {
				ArraySuppressionInfo arraySuppression = modifications.get(oriTab);
				ArrayList<Point2i> ranges = arraySuppression.ranges;
				Object tab = oriTab;
				for (int i = ranges.size(); i-- != 0;) {
					int tabLength = Array.getLength(tab);
					Point2i range = ranges.get(i);
					int rangeSize = range.y - range.x;
					Object newTab = Array.newInstance(tab.getClass().getComponentType(), tabLength - rangeSize);
					System.arraycopy(tab, 0, newTab, 0, range.x);
					System.arraycopy(tab, range.x + rangeSize, newTab, range.x, tabLength - range.y);
					tab = newTab;
				}
				if (arraySuppression.rootTab == null)
					newwRootTab = tab;
				else {
					Array.set(arraySuppression.rootTab, arraySuppression.rootTabIndex, tab);
					valueChanged();
				}
			}
			if (newwRootTab != null)
				setValueFromObj(newwRootTab);
			else {
				updateGUI();
				firePropertyChangeListener();
			}
			treeView.getSelectionModel().clearSelection();
		} else {
			TreeItem<Object> item = treeView.getSelectionModel().getSelectedItem();
			if (item == null || item instanceof ArrayTreeItem && ((ArrayTreeItem) item).isRangeItem())
				return;
			ArrayTreeItem parent = modificationType == INSERT_INSIDE ? (ArrayTreeItem) item : (ArrayTreeItem) item.getParent();
			Object tab = parent.tab;
			int tabLength = Array.getLength(tab);
			Node graphic = item.getGraphic();
			int indexOfChange;
			if (graphic == null)
				indexOfChange = modificationType == INSERT_INSIDE ? tabLength : getIndexOfTreeItem((ArrayTreeItem) item);
			else {
				String indexText = ((Label) graphic).getText();
				indexOfChange = Integer.parseInt(indexText.substring(1, indexText.indexOf("]")));
			}

			Consumer<Integer> insertionTask = nbElement -> {
				if (nbElement == 0)
					return;
				Object newTab = Array.newInstance(tab.getClass().getComponentType(), tabLength + nbElement);
				int rowToInsertIndex = indexOfChange + (modificationType == INSERT_BELOW || modificationType == INSERT_MULTIPLE_BELOW ? 1 : 0);
				System.arraycopy(tab, 0, newTab, 0, rowToInsertIndex);
				System.arraycopy(tab, rowToInsertIndex, newTab, rowToInsertIndex + nbElement, tabLength - rowToInsertIndex);
				localizeArray(parent, tab, (parentTab, parentTabIndex) -> {
					if (parentTab == null)
						setValueFromObj(newTab);
					else {
						Array.set(parentTab, parentTabIndex, newTab);
						updateGUI();
						firePropertyChangeListener();
						treeView.getSelectionModel().clearSelection();
						if (modificationType == INSERT_INSIDE)
							localizeTreeItem((ArrayTreeItem) treeView.getRoot(), parentTab, parentTabIndex, ti -> treeView.getSelectionModel().select(ti));
						else
							localizeTreeItem((ArrayTreeItem) treeView.getRoot(), newTab,
									modificationType == INSERT_BELOW || modificationType == INSERT_MULTIPLE_BELOW ? rowToInsertIndex - 1 : rowToInsertIndex + 1 * nbElement,
									ti -> treeView.getSelectionModel().select(ti));
					}
				});
			};

			if (modificationType == INSERT_MULTIPLE_ABOVE || modificationType == INSERT_MULTIPLE_BELOW) {
				Stage dialog = new Stage(StageStyle.UTILITY);
				dialog.initModality(Modality.WINDOW_MODAL);
				dialog.initOwner(treeView.getScene().getWindow());
				dialog.setTitle("Number of element");
				IntegerEditor ie = new IntegerEditor(1, Integer.MAX_VALUE - tabLength, ControlType.SPINNER);
				ie.setMin(0);
				ie.setValue(1);
				Button okButton = new Button("Ok");
				okButton.setOnAction(e -> {
					insertionTask.accept((int) ie.getValue());
					dialog.close();
				});
				VBox box = new VBox(ie.getNoSelectionEditor(), okButton);
				box.setAlignment(Pos.CENTER);
				dialog.setScene(new Scene(box, -1, -1));
				dialog.show();
			} else
				insertionTask.accept(1);
		}
	}

	private Object readObjectArray(DataInput raf, int arrayLevel) throws IOException, ClassNotFoundException {
		if (arrayLevel != 1) {
			Class<?> type = getComponentType();
			for (int i = 0; i < arrayLevel - 1; i++)
				type = type.arrayType();
			int length = raf.readInt();
			Object[] tab = (Object[]) Array.newInstance(type, length);
			for (int i = 0; i < length; i++)
				if (raf.readBoolean())
					tab[i] = readObjectArray(raf, arrayLevel - 1);
			return tab;
		}
		if (this.nbGenericParameters == 0) {
			PropertyEditor<?> editor = getDefaultPatternEditor(null);
			Object[] tab = (Object[]) Array.newInstance(getComponentType(), raf.readInt());
			for (int i = 0; i < tab.length; i++)
				if (raf.readBoolean())
					tab[i] = editor.readValue(raf);
			return tab;
		}
		Object[] tab = (Object[]) Array.newInstance(getComponentType(), raf.readInt());
		for (int i = 0; i < tab.length; i++)
			if (raf.readBoolean()) {
				Class<?>[] types = new Class<?>[raf.readInt()];
				for (int k = 0; k < types.length; k++)
					types[k] = BeanManager.getClassFromDescriptor(raf.readUTF());
				PropertyEditor<?> editor = getDefaultPatternEditor(() -> types);
				tab[i] = editor.readValue(raf);
			}
		return tab;
	}

	private Object readPrimitiveArray(DataInput raf, int arrayLevel) throws IOException {
		Class<?> componentType = getComponentType();
		if (arrayLevel <= 1) {
			if (componentType == byte.class)
				return readByteArray(raf);
			else if (componentType == double.class)
				return readDoubleArray(raf);
			else if (componentType == float.class) // 10000000 en 50 ms -> 38mo/50ms good!!!
				return readFloatArray(raf);
			else if (componentType == int.class)
				return readIntArray(raf);
			else if (componentType == long.class)
				return readLongArray(raf);
			else if (componentType == boolean.class)
				return readBooleanArray(raf);
			else if (componentType == short.class)
				return readShortArray(raf);
			else if (componentType == char.class)
				return readCharacterArray(raf);
		} else if (arrayLevel == 2) {
			if (componentType == byte.class)
				return readByteBiArray(raf);
			else if (componentType == double.class)
				return readDoubleBiArray(raf);
			else if (componentType == float.class) // 10000000 en 50 ms -> 38mo/50ms good!!!
				return readFloatBiArray(raf);
			else if (componentType == int.class)
				return readIntBiArray(raf);
			else if (componentType == long.class)
				return readLongBiArray(raf);
			else if (componentType == boolean.class)
				return readBooleanBiArray(raf);
			else if (componentType == short.class)
				return readShortBiArray(raf);
			else if (componentType == char.class)
				return readCharacterBiArray(raf);
		} else {
			Class<?> type = componentType;
			for (int i = 0; i < arrayLevel - 1; i++)
				type = type.arrayType();
			int length = raf.readInt();
			Object tab = Array.newInstance(type, length);
			for (int i = 0; i < length; i++)
				if (raf.readBoolean())
					Array.set(tab, i, readPrimitiveArray(raf, arrayLevel - 1));
			return tab;
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T readValue(DataInput raf) throws IOException {
		try {
			return (T) (getComponentType().isPrimitive() ? readPrimitiveArray(raf, this.arrayLevel) : readObjectArray(raf, this.arrayLevel));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<Object> getSubBeans() {
		if (getComponentType().isPrimitive())
			return null;
		if (!(getDefaultPatternEditor(null) instanceof BeanEditor))
			return null;
		Object[] array = (Object[]) getValue();
		if (array == null || array.length == 0)
			return null;
		ArrayList<Object> subBeans = new ArrayList<>();
		LinkedList<Object[]> todoArray = new LinkedList<>();
		todoArray.add(array);
		while (!todoArray.isEmpty()) {
			array = todoArray.pop();
			for (Object object : array)
				if (object != null)
					if (object instanceof Object[])
						todoArray.push((Object[]) object);
					else
						subBeans.add(object);
		}
		return subBeans;
	}

	@Override
	public void removeSubBeans(Object beanToRemove) {
		if (getComponentType().isPrimitive())
			return;
		Object[] val = (Object[]) getValue();
		if (val == null || val.length == 0)
			return;
		boolean removedValue = false;
		if (val instanceof Object[][]) {
			LinkedList<Object[][]> todoArray = new LinkedList<>();
			todoArray.add((Object[][]) val);
			while (!todoArray.isEmpty()) {
				Object[][] array2D = todoArray.pop();
				for (Object[] array : array2D)
					if (array != null)
						if (array instanceof Object[][])
							todoArray.push((Object[][]) array);
						else
							removedValue |= removeSubBeansIn1DArray(val, beanToRemove);
			}
		} else
			removedValue = removeSubBeansIn1DArray(val, beanToRemove);
		if (removedValue)
			updateGUI();
	}

	private static boolean removeSubBeansIn1DArray(Object[] array, Object beanToRemove) {
		boolean removedValue = false;
		for (int i = 0; i < array.length; i++)
			if (array[i] == beanToRemove) {
				array[i] = null;
				removedValue = true;
			}
		return removedValue;
	}

	@Override
	public void setAsText(String text) {
		super.setValueFromObj(stringToValue(text, this.arrayLevel, new MutableInteger()));
	}

	public void setToDefaultValue() {
		setToDefaultValue(0);
	}

	private void setToDefaultValue(int arrayLength) {
		Class<?> type = getComponentType();
		for (int i = 0; i < this.arrayLevel - 1; i++)
			type = type.arrayType();
		setValueFromObj(Array.newInstance(type, arrayLength));
	}

	private Object stringToValue(String stringValue, int level, MutableInteger index) {
		if (stringValue == null || stringValue.isEmpty()) {
			setValue(null);
			return null;
		}
		if (stringValue.charAt(index.get()) == '_') {
			index.increment();
			return null;
		}
		index.increment();
		Object tab = null;
		if (level != 1) {
			ArrayList<Object> subObj = new ArrayList<>();
			while (true) {
				if (stringValue.charAt(index.get()) == '}') {
					index.increment();
					break;
				}
				subObj.add(stringToValue(stringValue, level - 1, index));
				index.increment();
			}
			Class<?> type = getComponentType();
			for (int i = 0; i < level - 1; i++)
				type = type.arrayType();
			tab = Array.newInstance(type, subObj.size());
			int i = 0;
			for (Object object : subObj)
				Array.set(tab, i++, object);
		} else {
			ArrayList<Object> subObj = new ArrayList<>();
			Class<?> type = getComponentType();
			int beginIndex = index.get();
			int l = 0;
			if (this.nbGenericParameters == 0) {
				PropertyEditor<?> editor = getDefaultPatternEditor(null);
				if (editor.canContainForbiddenCharacter())
					while (true) {
						char charValue = stringValue.charAt(beginIndex);
						if (charValue == ',') {
							subObj.add(null);
							beginIndex++;
						} else if (charValue == '}') {
							index.set(beginIndex + 1);
							break;
						} else {
							int spaceIndex = stringValue.indexOf(" ", beginIndex);
							int nbChar = Integer.parseInt(stringValue.substring(beginIndex, spaceIndex++));
							int endIndex = spaceIndex + nbChar;
							editor.setAsText(stringValue.substring(spaceIndex, endIndex));
							subObj.add(editor.getValue());
							beginIndex = endIndex + 1;
						}
					}
				else
					for (int i = index.get(); i < stringValue.length(); i++) {
						char charValue = stringValue.charAt(i);
						if (charValue == ',' && l == 0) {
							editor.setAsText(stringValue.substring(beginIndex, i));
							subObj.add(editor.getValue());
							beginIndex = i + 1;
						} else if (charValue == '{')
							l++;
						else if (charValue == '}') {
							if (l == 0) {
								index.set(i + 1);
								break;
							}
							l--;
						}
					}
			} else if (isSubEditorOnlyContainsAllowedCharacter())
				for (int i = index.get(); i < stringValue.length(); i++) {
					char charValue = stringValue.charAt(i);
					if (charValue == ',' && l == 0) {
						String text = stringValue.substring(beginIndex, i);
						PropertyEditor<?> editor = getDefaultPatternEditor(() -> {
							String t = text;
							Class<?>[] types = new Class<?>[this.nbGenericParameters];
							for (int k = 0; k < types.length; k++) {
								int splitIndex = t.indexOf(GenericPropertyEditor.TYPE_SEPARATOR);
								try {
									types[k] = BeanManager.getClassFromDescriptor(t.substring(0, splitIndex));
								} catch (ClassNotFoundException | StringIndexOutOfBoundsException e) {
									System.err.println("Cannot find the generic class: " + t.substring(splitIndex));
								}
								t = text.substring(splitIndex + 1);
							}
							return types;
						});
						editor.setAsText(stringValue.substring(beginIndex, i));
						subObj.add(editor.getValue());
						beginIndex = i + 1;
					} else if (charValue == '{')
						l++;
					else if (charValue == '}') {
						if (l == 0) {
							index.set(i + 1);
							break;
						}
						l--;
					}
				}
			else
				while (true) {
					char charValue = stringValue.charAt(beginIndex);
					if (charValue == ',') {
						subObj.add(null);
						beginIndex++;
					} else if (charValue == '}') {
						index.set(beginIndex + 1);
						break;
					} else {
						int spaceIndex = stringValue.indexOf(" ", beginIndex);
						int nbChar = Integer.parseInt(stringValue.substring(beginIndex, spaceIndex++));
						int endIndex = spaceIndex + nbChar;
						String text = stringValue.substring(spaceIndex, endIndex);
						PropertyEditor<?> editor = getDefaultPatternEditor(() -> {
							String t = text;// .substring(text.indexOf(" ") + 1);
							Class<?>[] types = new Class<?>[this.nbGenericParameters];
							for (int k = 0; k < types.length; k++) {
								int splitIndex = t.indexOf(GenericPropertyEditor.TYPE_SEPARATOR);
								try {
									types[k] = BeanManager.getClassFromDescriptor(t.substring(0, splitIndex));
								} catch (ClassNotFoundException | StringIndexOutOfBoundsException e) {
									System.err.println("Cannot find the generic class: " + t.substring(splitIndex));
								}
								t = t.substring(splitIndex + 1);
							}
							return types;
						});
						editor.setAsText(stringValue.substring(spaceIndex, endIndex));
						subObj.add(editor.getValue());
						beginIndex = endIndex + 1;
					}
				}
			for (int i = 0; i < level - 1; i++)
				type = type.arrayType();
			tab = Array.newInstance(type, subObj.size());
			for (int i = 0; i < subObj.size(); i++)
				Array.set(tab, i, subObj.get(i));
		}
		return tab;
	}

	@Override
	public void updateCustomEditor() {
		ObservableList<Node> spChildren = ((StackPane) customEditor()).getChildren();
		updateCustomEditor(spChildren);
	}

	private void updateCustomEditor(ObservableList<Node> spChildren) {
		ArrayTreeItem root = (ArrayTreeItem) ((TreeView<?>) spChildren.get(0)).getRoot();
		Object value = getValue();
		if (root != null) {
			if (root.isLeaf() && value != null && Array.getLength(value) != 0) // Patch in order to solves the problem of not refreshing when changing from 0 to 1 elements
				root.getChildren();
			root.resetRoot(value);
		}
		if (value == null || Array.getLength(value) == 0) {
			ObservableList<Node> vBoxChildren;
			if (spChildren.size() == 1) {
				VBox vBox = new VBox(new Button(), new Button());
				spChildren.add(vBox);
				vBox.setAlignment(Pos.CENTER);
				vBox.setSpacing(10);
				vBoxChildren = vBox.getChildren();
			}
			vBoxChildren = ((VBox) spChildren.get(1)).getChildren();
			if (value == null) {
				Button newInstanceButton = (Button) vBoxChildren.get(0);
				newInstanceButton.setText("New instance");
				newInstanceButton.setOnAction(e -> setToDefaultValue());
				Button newInstAndAddFirstButton = (Button) vBoxChildren.get(1);
				newInstAndAddFirstButton.setText("New instance and add first item");
				newInstAndAddFirstButton.setOnAction(e -> setToDefaultValue(1));
				vBoxChildren.setAll(newInstanceButton, newInstAndAddFirstButton);
			} else {
				Button addButton = (Button) vBoxChildren.get(0);
				addButton.setText("Add first item");
				addButton.setOnAction(e -> setToDefaultValue(1));
				Button setToNullButton = (Button) vBoxChildren.get(1);
				setToNullButton.setText("Set array to null");
				setToNullButton.setOnAction(e -> setValue(null));
				vBoxChildren.setAll(addButton, setToNullButton);
			}
		} else if (spChildren.size() == 2)
			spChildren.remove(1);
	}

	@Override
	@SuppressWarnings("unchecked")
	public void updateCustomView() {
		updateCustomView((TreeView<Object>) this.view);
	}

	public void updateCustomView(TreeView<Object> treeView) {
		ArrayTreeItem root = (ArrayTreeItem) treeView.getRoot();
		if (root != null)
			root.resetRoot(getValue());
	}

	void valueChanged() {
		firePropertyChangeListener();
	}

	private void writeObjectArray(DataOutput raf, int arrayLevel, Object[] value, boolean hasParameters) throws IOException {
		raf.writeInt(value.length);
		PropertyEditor<?> editor = null;
		for (Object tabValue : value)
			if (tabValue == null)
				raf.writeBoolean(false);
			else {
				raf.writeBoolean(true);
				PropertyEditor<?> currentEditor;
				if (hasParameters) {
					currentEditor = PropertyEditorManager.findEditorWithValue(tabValue, this.local); // Changement, un seul editeur je pense
					if (arrayLevel == 1) {
						Class<?>[] types = ((GenericPropertyEditor<?>) currentEditor).getTypes();
						raf.writeInt(types.length);
						for (Class<?> type : types)
							raf.writeUTF(BeanManager.getDescriptorFromClass(type));
					}
				} else {
					if (editor == null)
						editor = PropertyEditorManager.findEditor(tabValue.getClass(), this.local); // Changement, un seul editeur je pense
					currentEditor = editor; // Changement, un seul editeur je pense
				}
				try {
					currentEditor.writeValueFromObj(raf, tabValue);
				} catch (NullPointerException e) {
					PropertyEditorManager.findEditor(tabValue.getClass(), this.local);
					e.printStackTrace();
				}
			}
	}

	private void writePrimitiveArray(DataOutput raf, int arrayLevel, Object value) throws IOException {
		Class<?> componentType = getComponentType();
		if (arrayLevel <= 1) {
			if (componentType == byte.class)
				writeByteArray(raf, (byte[]) value);
			else if (componentType == double.class)
				writeDoubleArray(raf, (double[]) value);
			else if (componentType == float.class) // 10000000 en 50 ms -> 38mo/50ms good!!!
				writeFloatArray(raf, (float[]) value);
			else if (componentType == int.class)
				writeIntArray(raf, (int[]) value);
			else if (componentType == long.class)
				writeLongArray(raf, (long[]) value);
			else if (componentType == boolean.class)
				writeBooleanArray(raf, (boolean[]) value);
			else if (componentType == short.class)
				writeShortArray(raf, (short[]) value);
			else if (componentType == char.class)
				writeCharArray(raf, (char[]) value);
		} else if (arrayLevel == 2) {
			if (componentType == byte.class)
				writeByteArray(raf, (byte[][]) value);
			else if (componentType == double.class)
				writeDoubleArray(raf, (double[][]) value);
			else if (componentType == float.class) // 10000000 en 50 ms -> 38mo/50ms good!!!
				writeFloatArray(raf, (float[][]) value);
			else if (componentType == int.class)
				writeIntArray(raf, (int[][]) value);
			else if (componentType == long.class)
				writeLongArray(raf, (long[][]) value);
			else if (componentType == boolean.class)
				writeBooleanArray(raf, (boolean[][]) value);
			else if (componentType == short.class)
				writeShortArray(raf, (short[][]) value);
			else if (componentType == char.class)
				writeCharArray(raf, (char[][]) value);
		} else {
			int length = Array.getLength(value);
			raf.writeInt(length);
			for (int i = 0; i < length; i++) {
				Object val = Array.get(value, i);
				if (val != null) {
					raf.writeBoolean(true);
					writePrimitiveArray(raf, arrayLevel - 1, Array.get(value, i));
				} else
					raf.writeBoolean(false);
			}
		}
	}

	@Override
	public void writeValue(DataOutput raf, T value) throws IOException {
		if (getComponentType().isPrimitive())
			writePrimitiveArray(raf, this.arrayLevel, value);
		else
			writeObjectArray(raf, this.arrayLevel, (Object[]) value, this.nbGenericParameters != 0);
	}

	class ArraySuppressionInfo {
		private final Object rootTab;
		private final int rootTabIndex;
		private final ArrayList<Point2i> ranges;

		public ArraySuppressionInfo(Object rootTab, int rootTabIndex) {
			this.rootTab = rootTab;
			this.rootTabIndex = rootTabIndex;
			this.ranges = new ArrayList<>();
		}

		private void addRangeToBeDeleted(Point2i toRemove) {
			if (this.ranges.isEmpty()) {
				this.ranges.add(toRemove);
				return;
			}
			int begin = toRemove.x;
			int end = toRemove.y;
			int index = 0;
			while (index < this.ranges.size() && begin > this.ranges.get(index).x)
				index++;

			int nextIndex = index;
			// Test d'inclusion
			while (nextIndex < this.ranges.size()) {
				Point2i nextElement = this.ranges.get(nextIndex);
				if (nextElement.x >= begin && nextElement.y <= end)
					this.ranges.remove(nextElement);
				else if (begin >= nextElement.x && end <= nextElement.y)
					return;
				else
					break;
			}
			int previousIndex = index - 1;
			while (previousIndex >= 0) {
				Point2i previousElement = this.ranges.get(previousIndex);
				if (previousElement.x >= begin && previousElement.y <= end)
					this.ranges.remove(previousElement);
				else if (begin >= previousElement.x && end <= previousElement.y)
					return;
				else
					break;
			}
			// Test de recouvrement
			if (nextIndex < this.ranges.size()) {
				Point2i nextElement = this.ranges.get(nextIndex);
				if (nextElement.x >= begin && nextElement.x <= end) {
					nextElement.x = begin;
					if (previousIndex >= 0) {
						Point2i previousElement = this.ranges.get(previousIndex);
						if (previousElement.y >= nextElement.x && previousElement.y <= nextElement.y) {
							nextElement.x = previousElement.x;
							this.ranges.remove(previousIndex);
						}
					}
					return;
				}
			}
			if (previousIndex >= 0) {
				Point2i previousElement = this.ranges.get(previousIndex);
				if (previousElement.y >= begin && previousElement.y <= end) {
					previousElement.y = end;
					return;
				}
			}
			this.ranges.add(index, toRemove);
		}
	}

	class MutableInteger {
		private int value;

		public int get() {
			return this.value;
		}

		public void increment() {
			this.value++;
		}

		public void set(int value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return Integer.toString(this.value);
		}
	}

	private static int getArrayLevel(Class<?> type) {
		int arrayLevel = 0;
		while (type.isArray()) {
			type = type.getComponentType();
			arrayLevel++;
		}
		return arrayLevel;
	}

	static String getArrayTreeItemPrefix(String prefix, int begin, int end, Object tab) {
		return prefix + (end - begin == 0 ? EMPTY_ARRAY + String.join("", Collections.nCopies(getArrayLevel(tab.getClass()) - 1, SUB_ARRAY))
				: "[" + begin + RANGE + (end - 1) + "]" + String.join("", Collections.nCopies(getArrayLevel(tab.getClass()) - 1, SUB_ARRAY)));// change Collections.nCopies to repeat in Java11
	}

	public static Class<?> getComponentType(Class<?> type) {
		while (type.isArray())
			type = type.getComponentType();
		return type;
	}

	public static void main(String[] args) {
		ArrayEditor<Double[][][]> ae = new ArrayEditor<>(Double[][][].class);
		// double[][][] tab = new double[100][7][495];
		// int index = 0;
		// for (int i = 0; i < tab.length; i++)
		// for (int j = 0; j < tab[i].length; j++)
		// for (int k = 0; k < tab[i][j].length; k++)
		// tab[i][j][k] = index++ / 100.0;

		// double[][] tab = new double[101][7];
		// int index = 0;
		// for (int i = 0; i < tab.length; i++)
		// for (int j = 0; j < tab[i].length; j++)
		// tab[i][j] = index++ / 100.0;

		// double[] tab = new double[0];
		// int index = 0;
		// for (int i = 0; i < tab.length; i++)
		// tab[i] = index++ / 100.0;

		Double[][][] tab = new Double[3][0][0];
		int index = 0;
		for (int i = 0; i < tab.length; i++)
			for (int j = 0; j < tab[i].length; j++)
				for (int k = 0; k < tab[i][j].length; k++)
					tab[i][j][k] = index++ / 100.0;

		tab[1] = new Double[][] { new Double[0] };
		tab[2] = new Double[][] { new Double[3] };
		// tab[0][0] = null;
		// tab[0][1] = new Double[] { (double) 7 };
		ae.setValue(tab);// new Selection[] {new Selection<String>(new HashSet<>(), String.class)}
		ArrayEditor<InetSocketAddress[]> ae2 = new ArrayEditor<>(InetSocketAddress[].class);
		ae2.setValue(new InetSocketAddress[] { new InetSocketAddress(90), new InetSocketAddress(94) });
		new FxTest().launchIHM(args, s -> {
			VBox vbox = new VBox(ae.getNoSelectionEditor()/* , ae2.getEditor() */);
			return vbox;
		}, false);
	}

	public static boolean[] readBooleanArray(DataInput raf) throws IOException {
		int length = raf.readInt();
		if (length == 0)
			return new boolean[0];
		byte[] data = new byte[length / Byte.SIZE + (length % Byte.SIZE != 0 ? 1 : 0)];
		raf.readFully(data);
		boolean[] array = new boolean[length];
		int index = 0;
		int i = 0;
		int baseSize = data.length - 1;
		while (i < baseSize) {
			byte b = data[i++];
			for (int j = 0; j < Byte.SIZE; j++)
				array[index++] = (b & 1 << j) != 0;
		}
		byte lastByte = data[i];
		int dec = 0;
		while (index < length)
			array[index++] = (lastByte & 1 << dec++) != 0;
		return array;
	}

	public static boolean[][] readBooleanBiArray(DataInput raf) throws IOException {
		int size = raf.readInt();
		boolean[][] data = new boolean[size][];
		for (int i = 0; i < data.length; i++)
			if (raf.readBoolean())
				data[i] = readBooleanArray(raf);
		return data;
	}

	public static byte[] readByteArray(DataInput raf) throws IOException {
		byte[] data = new byte[raf.readInt()];
		raf.readFully(data);
		return data;
	}

	public static byte[][] readByteBiArray(DataInput raf) throws IOException {
		int size = raf.readInt();
		byte[][] data = new byte[size][];
		for (int i = 0; i < data.length; i++)
			if (raf.readBoolean())
				data[i] = readByteArray(raf);
		return data;
	}

	public static char[] readCharacterArray(DataInput raf) throws IOException {
		byte[] data = new byte[raf.readInt() * Character.BYTES];
		raf.readFully(data);
		int times = Character.BYTES;
		char[] arrays = new char[data.length / times];
		for (int i = 0; i < arrays.length; i++)
			arrays[i] = ByteBuffer.wrap(data, i * times, times).getChar();
		return arrays;
	}

	public static char[][] readCharacterBiArray(DataInput raf) throws IOException {
		int size = raf.readInt();
		char[][] data = new char[size][];
		for (int i = 0; i < data.length; i++)
			if (raf.readBoolean())
				data[i] = readCharacterArray(raf);
		return data;
	}

	public static char[] readCharArray(DataInput raf) throws IOException {
		byte[] data = new byte[raf.readInt() * Character.BYTES];
		raf.readFully(data);
		int times = Character.BYTES;
		char[] arrays = new char[data.length / times];
		for (int i = 0; i < arrays.length; i++)
			arrays[i] = ByteBuffer.wrap(data, i * times, times).getChar();
		return arrays;
	}

	public static char[][] readCharBiArray(DataInput raf) throws IOException {
		int size = raf.readInt();
		char[][] data = new char[size][];
		for (int i = 0; i < data.length; i++)
			if (raf.readBoolean())
				data[i] = readCharArray(raf);
		return data;
	}

	public static double[] readDoubleArray(DataInput raf) throws IOException {
		byte[] data = new byte[raf.readInt() * Double.BYTES];
		raf.readFully(data);
		int times = Double.BYTES;
		double[] array = new double[data.length / times];
		for (int i = 0; i < array.length; i++)
			array[i] = ByteBuffer.wrap(data, i * times, times).getDouble();
		return array;
	}

	public static double[][] readDoubleBiArray(DataInput raf) throws IOException {
		int size = raf.readInt();
		double[][] data = new double[size][];
		for (int i = 0; i < data.length; i++)
			if (raf.readBoolean())
				data[i] = readDoubleArray(raf);
		return data;
	}

	public static float[] readFloatArray(DataInput raf) throws IOException {
		byte[] data = new byte[raf.readInt() * Float.BYTES];
		raf.readFully(data);
		int times = Float.BYTES;
		float[] array = new float[data.length / times];
		for (int i = 0; i < array.length; i++)
			array[i] = ByteBuffer.wrap(data, i * times, times).getFloat();
		return array;
	}

	public static float[][] readFloatBiArray(DataInput raf) throws IOException {
		int size = raf.readInt();
		float[][] data = new float[size][];
		for (int i = 0; i < data.length; i++)
			if (raf.readBoolean())
				data[i] = readFloatArray(raf);
		return data;
	}

	public static int[] readIntArray(DataInput raf) throws IOException {
		byte[] data = new byte[raf.readInt() * Integer.BYTES];
		raf.readFully(data);
		int times = Integer.BYTES;
		int[] array = new int[data.length / times];
		for (int i = 0; i < array.length; i++)
			array[i] = ByteBuffer.wrap(data, i * times, times).getInt();
		return array;
	}

	public static int[][] readIntBiArray(DataInput raf) throws IOException {
		int size = raf.readInt();
		int[][] data = new int[size][];
		for (int i = 0; i < data.length; i++)
			if (raf.readBoolean())
				data[i] = readIntArray(raf);
		return data;
	}

	public static long[] readLongArray(DataInput raf) throws IOException {
		byte[] data = new byte[raf.readInt() * Long.BYTES];
		raf.readFully(data);
		int times = Long.BYTES;
		long[] arrays = new long[data.length / times];
		for (int i = 0; i < arrays.length; i++)
			arrays[i] = ByteBuffer.wrap(data, i * times, times).getLong();
		return arrays;
	}

	public static long[][] readLongBiArray(DataInput raf) throws IOException {
		int size = raf.readInt();
		long[][] data = new long[size][];
		for (int i = 0; i < data.length; i++)
			if (raf.readBoolean())
				data[i] = readLongArray(raf);
		return data;
	}

	public static short[] readShortArray(DataInput raf) throws IOException {
		byte[] data = new byte[raf.readInt() * Short.BYTES];
		raf.readFully(data);
		int times = Short.BYTES;
		short[] arrays = new short[data.length / times];
		for (int i = 0; i < arrays.length; i++)
			arrays[i] = ByteBuffer.wrap(data, i * times, times).getShort();
		return arrays;
	}

	public static short[][] readShortBiArray(DataInput raf) throws IOException {
		int size = raf.readInt();
		short[][] data = new short[size][];
		for (int i = 0; i < data.length; i++)
			if (raf.readBoolean())
				data[i] = readShortArray(raf);
		return data;
	}

	public static void writeBooleanArray(DataOutput raf, boolean[] values) throws IOException {
		int size = values.length;
		raf.writeInt(size);
		if (size == 0)
			return;
		int baseSize = values.length / Byte.SIZE;
		byte[] data = new byte[baseSize + (size % Byte.SIZE == 0 ? 0 : 1)];
		int index = 0;
		for (int i = 0; i < baseSize; i++) {
			byte b = 0;
			for (int j = 0; j < Byte.SIZE; j++)
				if (values[index++])
					b += 1 << j;
			data[i] = b;
		}
		if (index != values.length) {
			byte lastB = 0;
			int dec = 0;
			for (; index < values.length; index++) {
				if (values[index])
					lastB += 1 << dec;
				dec++;
			}
			data[data.length - 1] = lastB;
		}
		raf.write(data);
	}

	public static void writeBooleanArray(DataOutput raf, boolean[][] values) throws IOException {
		raf.writeInt(values.length);
		for (boolean[] array : values)
			if (array == null)
				raf.writeBoolean(false);
			else {
				raf.writeBoolean(true);
				writeBooleanArray(raf, array);
			}
	}

	public static void writeByteArray(DataOutput raf, byte[] values) throws IOException {
		raf.writeInt(values.length);
		raf.write(values);
	}

	public static void writeByteArray(DataOutput raf, byte[][] values) throws IOException {
		raf.writeInt(values.length);
		for (byte[] array : values)
			if (array == null)
				raf.writeBoolean(false);
			else {
				raf.writeBoolean(true);
				writeByteArray(raf, array);
			}
	}

	public static void writeCharArray(DataOutput raf, char[] values) throws IOException {
		ByteBuffer buff = ByteBuffer.allocate(values.length * Character.BYTES);
		for (char val : values)
			buff.putChar(val);
		raf.writeInt(values.length);
		raf.write(buff.array());
	}

	// @Override
	// public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
	// if (newValue.doubleValue() > 0) {
	// Platform.runLater(() -> list.setFixedCellSize(newValue.doubleValue() + 2)); // Bug de taille sinon... pas de
	// // mise a jour...
	// fixedHeightRegion.heightProperty().removeListener(this);
	// }
	// }

	public static void writeCharArray(DataOutput raf, char[][] values) throws IOException {
		raf.writeInt(values.length);
		for (char[] array : values)
			if (array == null)
				raf.writeBoolean(false);
			else {
				raf.writeBoolean(true);
				writeCharArray(raf, array);
			}
	}

	public static void writeDoubleArray(DataOutput raf, double[] values) throws IOException {
		ByteBuffer buff = ByteBuffer.allocate(values.length * Double.BYTES);
		for (double val : values)
			buff.putDouble(val);
		raf.writeInt(values.length);
		raf.write(buff.array());
	}

	public static void writeDoubleArray(DataOutput raf, double[][] values) throws IOException {
		raf.writeInt(values.length);
		for (double[] array : values)
			if (array == null)
				raf.writeBoolean(false);
			else {
				raf.writeBoolean(true);
				writeDoubleArray(raf, array);
			}
	}

	public static void writeFloatArray(DataOutput raf, float[] values) throws IOException {
		ByteBuffer buff = ByteBuffer.allocate(values.length * Float.BYTES);
		for (float val : values)
			buff.putFloat(val);
		raf.writeInt(values.length);
		raf.write(buff.array());
	}

	public static void writeFloatArray(DataOutput raf, float[][] values) throws IOException {
		raf.writeInt(values.length);
		for (float[] array : values)
			if (array == null)
				raf.writeBoolean(false);
			else {
				raf.writeBoolean(true);
				writeFloatArray(raf, array);
			}
	}

	public static void writeIntArray(DataOutput raf, int[] values) throws IOException {
		ByteBuffer buff = ByteBuffer.allocate(values.length * Integer.BYTES);
		for (int val : values)
			buff.putInt(val);
		raf.writeInt(values.length);
		raf.write(buff.array());
	}

	public static void writeIntArray(DataOutput raf, int[][] values) throws IOException {
		raf.writeInt(values.length);
		for (int[] array : values)
			if (array == null)
				raf.writeBoolean(false);
			else {
				raf.writeBoolean(true);
				writeIntArray(raf, array);
			}
	}

	public static void writeLongArray(DataOutput raf, long[] values) throws IOException {
		ByteBuffer buff = ByteBuffer.allocate(values.length * Long.BYTES);
		for (long val : values)
			buff.putLong(val);
		raf.writeInt(values.length);
		raf.write(buff.array());
	}

	public static void writeLongArray(DataOutput raf, long[][] values) throws IOException {
		raf.writeInt(values.length);
		for (long[] array : values)
			if (array == null)
				raf.writeBoolean(false);
			else {
				raf.writeBoolean(true);
				writeLongArray(raf, array);
			}
	}

	public static void writeShortArray(DataOutput raf, short[] values) throws IOException {
		ByteBuffer buff = ByteBuffer.allocate(values.length * Short.BYTES);
		for (short val : values)
			buff.putShort(val);
		raf.writeInt(values.length);
		raf.write(buff.array());
	}

	public static void writeShortArray(DataOutput raf, short[][] values) throws IOException {
		raf.writeInt(values.length);
		for (short[] array : values)
			if (array == null)
				raf.writeBoolean(false);
			else {
				raf.writeBoolean(true);
				writeShortArray(raf, array);
			}
	}

	public int getPrefHeight() {
		return this.prefHeight;
	}

	public void setPrefHeight(int prefHeight) {
		this.prefHeight = prefHeight;
	}

	@Override
	public boolean isInline() {
		return false;
	}
}

class ArrayTreeItem extends TreeItem<Object> {
	private final boolean editable;
	private final ArrayEditor<?> editor;
	private final HashMap<TreeItem<?>, PropertyEditor<?>> editorMap = new HashMap<>();
	private String prefix;
	private boolean isChildrenInitialize = false;
	int begin;
	int end;
	Object tab;
	private int tabLength;

	public ArrayTreeItem(ArrayEditor<?> editor, String prefix, int begin, int end, Object tab, boolean editable) {
		super(tab == null ? "" : ArrayEditor.getArrayTreeItemPrefix(prefix, begin, end, tab), null);
		this.editor = editor;
		this.prefix = prefix;
		this.begin = begin;
		this.end = end;
		this.tab = tab;
		this.tabLength = tab == null ? -1 : Array.getLength(tab);
		this.editable = editable;
	}

	private PropertyEditor<?> createEditor(Object val, int index) {
		PropertyEditor<?> newEditor = this.editor.getPropertyEditor();
		newEditor.setValueFromObj(val);
		newEditor.addPropertyChangeListener(() -> {
			Array.set(this.tab, index, newEditor.getValue());
			this.editor.valueChanged();
		});
		return newEditor;
	}

	private Button createNullButton(int index) {
		Button nullButton = new Button("New Instance");
		nullButton.setOnAction(e -> {
			Class<?> compType = this.tab.getClass().getComponentType();
			Array.set(this.tab, index, Array.newInstance(compType.getComponentType(), 0));
			this.editor.updateGUI();
			this.editor.valueChanged();
		});
		nullButton.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(nullButton, Priority.ALWAYS);
		return nullButton;
	}

	@Override
	public ObservableList<TreeItem<Object>> getChildren() {
		if (!this.isChildrenInitialize)
			populate();
		return super.getChildren();
	}

	private int getLevel() {
		return getLevel(this.begin, this.end);
	}

	int getLevel(int begin, int end) {
		int size = end - begin;
		int divider = 0;
		while (size > 100) {
			size /= 100;
			divider++;
		}
		return divider;
	}

	private Region getRegionFromEditor(PropertyEditor<?> editor) {
		Region reg;
		if (this.editable) {
			reg = editor.getNoSelectionEditor();
			HBox.setHgrow(reg, Priority.ALWAYS);
		} else {
			reg = editor.getView();
			if (reg == null)
				reg = new Label(editor.getValue().toString());
			reg.setPadding(ArrayEditor.NULL_INSETS);
		}
		return reg;
	}

	public boolean isLastLevel() {
		return !this.tab.getClass().getComponentType().isArray();
	}

	@Override
	public boolean isLeaf() {
		return this.end == 0;
	}

	public boolean isRangeItem() {
		return this.begin != 0 || this.tabLength != this.end;
	}

	private void populate() {
		this.isChildrenInitialize = true;
		reset(this.tab, this.prefix, this.begin, this.end);
	}

	public void reset(Object tab, String prefix, int begin, int end) {
		this.tab = tab;
		if (tab == null) {
			begin = -1;
			end = -1;
			prefix = "";
			this.tab = tab;
			getChildren().clear();
			return;
		}
		if (this.begin != begin || this.end != end || !this.prefix.equals(prefix)) {
			this.begin = begin;
			this.end = end;
			this.prefix = prefix;
			setValue(ArrayEditor.getArrayTreeItemPrefix(prefix, begin, end, tab));
		}
		this.tabLength = Array.getLength(tab);
		if (!this.isChildrenInitialize)
			return;
		if (!isExpanded()) {
			getChildren().clear();
			this.editorMap.clear();
			this.isChildrenInitialize = false;
			return;
		}
		boolean isStrucModification = false;
		ObservableList<TreeItem<Object>> childrens = super.getChildren();
		int childrensSize = childrens.size();
		ArrayList<TreeItem<Object>> newChildrens = new ArrayList<>();
		int childrenIndex = 0;
		int level = getLevel();
		if (level == 0) {
			boolean isLastLevel = isLastLevel();
			for (int i = begin; i < end; i++) {
				Object val = Array.get(tab, i);
				if (val == null && !isLastLevel) {
					TreeItem<Object> nextChildren = childrenIndex < childrensSize ? childrens.get(childrenIndex++) : null;
					String labelText = (isLastLevel ? "" : prefix) + "[" + i + "] = ";
					if (nextChildren == null || nextChildren instanceof ArrayTreeItem) {
						Label label = new Label(labelText);
						label.setPadding(ArrayEditor.NULL_INSETS);
						newChildrens.add(new TreeItem<Object>(createNullButton(i), label));
						isStrucModification = true;
					} else {
						this.editorMap.remove(nextChildren);
						if (!(nextChildren.getValue() instanceof Button))
							nextChildren.setValue(createNullButton(i));
						if (!((Label) nextChildren.getGraphic()).getText().equals(labelText))
							((Label) nextChildren.getGraphic()).setText(labelText);
						newChildrens.add(nextChildren);
					}
				} else if (!isLastLevel) {
					int tabSize = val == null ? 0 : Array.getLength(val);
					TreeItem<Object> nextChildren = childrenIndex < childrensSize ? childrens.get(childrenIndex++) : null;
					String childrenPrefix = prefix + "[" + i + "]";
					if (nextChildren instanceof ArrayTreeItem) {
						ArrayTreeItem nextArrayTreeItem = (ArrayTreeItem) nextChildren;
						if (nextArrayTreeItem.begin == 0 && nextArrayTreeItem.end == tabSize && nextArrayTreeItem.prefix == childrenPrefix)
							nextArrayTreeItem.tab = val;
						else
							nextArrayTreeItem.reset(val, childrenPrefix, 0, tabSize);
						newChildrens.add(nextArrayTreeItem);
					} else {
						this.editorMap.remove(nextChildren);
						newChildrens.add(new ArrayTreeItem(this.editor, childrenPrefix, 0, tabSize, val, this.editable));
						isStrucModification = true;
					}
				} else {
					TreeItem<Object> nextChildren = childrenIndex < childrensSize ? childrens.get(childrenIndex++) : null;
					String labelText = "[" + i + "] = ";
					if (nextChildren == null || nextChildren instanceof ArrayTreeItem) {
						PropertyEditor<?> subEditor = createEditor(val, i);
						Label label = new Label(labelText);
						label.setPadding(ArrayEditor.NULL_INSETS);
						TreeItem<Object> ti = new TreeItem<>(getRegionFromEditor(subEditor), label);
						this.editorMap.put(ti, subEditor);
						newChildrens.add(ti);
						isStrucModification = true;
					} else {
						if (!((Label) nextChildren.getGraphic()).getText().equals(labelText))
							((Label) nextChildren.getGraphic()).setText(labelText);
						newChildrens.add(nextChildren);
						PropertyEditor<?> subEditor = this.editorMap.get(nextChildren);
						if (subEditor == null) {
							subEditor = createEditor(val, i);
							nextChildren.setValue(getRegionFromEditor(subEditor));
							this.editorMap.put(nextChildren, subEditor);
						} else
							subEditor.setValueFromObj(val);
					}
				}
			}
		} else {
			int divider = (int) Math.pow(100, level);
			int length = end - begin;
			int nbChild = length / divider;
			int i = 0;
			for (; i < nbChild; i++) {
				int subBegin = begin + i * divider;
				int subEnd = begin + (i + 1) * divider;
				TreeItem<Object> nextChildren = childrenIndex < childrensSize ? childrens.get(childrenIndex++) : null;
				if (nextChildren instanceof ArrayTreeItem) {
					ArrayTreeItem nextArrayTreeItem = (ArrayTreeItem) nextChildren;
					if (!nextArrayTreeItem.prefix.equals(prefix) || nextArrayTreeItem.begin != subBegin || nextArrayTreeItem.end != subEnd)
						nextArrayTreeItem.reset(tab, prefix, subBegin, subEnd);
					else
						nextArrayTreeItem.resetSubValue(tab);
					newChildrens.add(nextArrayTreeItem);
				} else {
					this.editorMap.remove(nextChildren);
					newChildrens.add(new ArrayTreeItem(this.editor, prefix, subBegin, subEnd, tab, this.editable));
					isStrucModification = true;
				}
			}
			if (length % divider != 0) {
				int subBegin = begin + i * divider;
				TreeItem<Object> nextChildren = childrenIndex < childrensSize ? childrens.get(childrenIndex++) : null;
				if (nextChildren instanceof ArrayTreeItem) {
					ArrayTreeItem nextArrayTreeItem = (ArrayTreeItem) nextChildren;
					if (!nextArrayTreeItem.prefix.equals(prefix) || nextArrayTreeItem.begin != subBegin || nextArrayTreeItem.end != this.tabLength)
						nextArrayTreeItem.reset(tab, prefix, subBegin, this.tabLength);
					else
						nextArrayTreeItem.resetSubValue(tab);
					newChildrens.add(nextArrayTreeItem);
				} else {
					this.editorMap.remove(nextChildren);
					newChildrens.add(new ArrayTreeItem(this.editor, prefix, subBegin, this.tabLength, tab, this.editable));
					isStrucModification = true;
				}
			}
		}
		for (int i = childrenIndex; i < childrensSize; i++) {
			TreeItem<Object> children = childrens.get(i);
			if (children != null && !(children instanceof ArrayTreeItem))
				this.editorMap.remove(children);
		}
		isStrucModification |= childrenIndex != childrensSize;
		if (isStrucModification)
			childrens.setAll(newChildrens);
	}

	public void resetRoot(Object tab) {
		isChildrenInitialize = true;
		reset(tab, "", 0, tab == null ? -1 : Array.getLength(tab));
	}

	private void resetSubValue(Object tab) {
		this.tab = tab;
		if (!this.isChildrenInitialize)
			return;
		if (!isExpanded()) {
			getChildren().clear();
			this.editorMap.clear();
			this.isChildrenInitialize = false;
			return;
		}
		int level = getLevel();
		if (level == 0) {
			int index = this.begin;
			if (!this.editorMap.isEmpty())
				for (TreeItem<Object> it : getChildren()) {
					PropertyEditor<?> pe = this.editorMap.get(it);
					if (pe != null) // Les null n'ont pas d'éditeur
						pe.setValueFromObj(Array.get(tab, index++));
				}
			else
				for (TreeItem<Object> it : getChildren()) {
					Object val = Array.get(tab, index);
					if (val != null)
						((ArrayTreeItem) it).reset(val, this.prefix + "[" + index++ + "]", 0, Array.getLength(val));
				}
		} else
			for (TreeItem<Object> it : getChildren())
				((ArrayTreeItem) it).resetSubValue(tab);
	}
}
