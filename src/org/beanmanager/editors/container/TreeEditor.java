/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.container;

import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodHandles.Lookup;
import java.lang.invoke.MethodType;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

import org.beanmanager.BeanListChangeListener;
import org.beanmanager.editors.PropertyEditor;
import org.beanmanager.editors.PropertyEditorManager;
import org.beanmanager.struct.TreeNode;
import org.beanmanager.struct.TreeRoot;

import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Region;

public class TreeEditor<T> extends GenericPropertyContainerEditor<TreeRoot<T>, T> implements BeanListChangeListener {
	private static MethodType setValueMethodType;
	private static MethodType setChildrensMethodType;
	private static MethodType addChildMethodType;
	private static MethodType removeChildMethodType;
	private static Lookup lookup;
	static {
		Method method;
		try {
			method = TreeNode.class.getMethod("setValue", Object.class);
			setValueMethodType = MethodType.methodType(method.getReturnType(), method.getParameterTypes());
			method = TreeNode.class.getMethod("setChildren", ArrayList.class);
			setChildrensMethodType = MethodType.methodType(method.getReturnType(), method.getParameterTypes());
			method = TreeNode.class.getMethod("addChild", TreeNode.class);
			addChildMethodType = MethodType.methodType(method.getReturnType(), method.getParameterTypes());
			method = TreeNode.class.getMethod("removeChild", TreeNode.class);
			removeChildMethodType = MethodType.methodType(method.getReturnType(), method.getParameterTypes());
		} catch (NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
		lookup = MethodHandles.lookup();
	}

	public static void main(String[] args) {
		TreeRoot<String> treeRoot = new TreeRoot<>(String.class);
		TreeNode<Integer> tree = new TreeNode<>(9);
		TreeNode<Integer> children1 = new TreeNode<>(0);
		children1.addChild(new TreeNode<>(4));
		children1.addChild(new TreeNode<>(7));
		tree.addChild(children1);
		TreeNode<Integer> children2 = new TreeNode<>(0);
		TreeNode<Integer> childrenChildren2 = new TreeNode<>(24);
		childrenChildren2.addChild(new TreeNode<>(1));
		children1.addChild(childrenChildren2);
		children1.addChild(new TreeNode<>(27));
		tree.addChild(children2);
		TreeEditor<String> te = new TreeEditor<>(String.class);
		te.setValue(treeRoot);
		te.setAsText(te.getAsText());
		PropertyEditor<?> editor = PropertyEditorManager.findEditor(TreeRoot.class, "", null);
		// editor.setValue(treeRoot);
		editor.getAsText();
		editor.setAsText("Cool");
	}

	public TreeEditor(Class<T> type) {
		super(type);
	}

	private static void addChild(TreeNode<?> treeNode, Object child) {
		try {
			lookup.bind(treeNode, "addChild", addChildMethodType).invoke(child);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	private static void expandTreeView(TreeItem<TreeViewNode> treeItem) {
		LinkedList<TreeItem<TreeViewNode>> todo = new LinkedList<>();
		todo.add(treeItem);
		while (!todo.isEmpty()) {
			TreeItem<TreeViewNode> ti = todo.pop();
			ti.setExpanded(true);
			todo.addAll(ti.getChildren());
		}
	}

	@Override
	public String getAsText() {
		TreeRoot<?> tree = getValue();
		if (tree == null)
			return null;
		StringBuilder text = new StringBuilder();
		saveTreeNode(tree, text);
		return text.toString();
	}

	@Override
	protected Region getCustomEditor() {
		if (getComponentType() == null)
			throw new IllegalAccessError("The type of the treeEditor need to be set before");
		else if (getPropertyEditorClone(null) == null)
			throw new IllegalAccessError("The type of the treeEditor: " + getComponentType() + " does not have an editor");
		TreeRoot<?> tree = getValue();
		TreeItem<TreeViewNode> rootNode = null;
		if (tree != null)
			rootNode = getTreeItem(tree);
		TreeView<TreeViewNode> treeView = new TreeView<>(rootNode);
		treeView.setMinHeight(100);
		treeView.setCellFactory(p -> {
			TreeCell<TreeViewNode> cell = new TreeCell<>() {
				@Override
				protected void updateItem(TreeViewNode item, boolean empty) {
					super.updateItem(item, empty);
					setGraphic(item == null ? null : item.propertyEditor.getNoSelectionEditor());
				}
			};
			cell.addEventHandler(MouseEvent.MOUSE_CLICKED, e -> {
				if (e.getButton() == MouseButton.SECONDARY) {
					ArrayList<MenuItem> menus = new ArrayList<>();
					TreeItem<TreeViewNode> treeItem = cell.getTreeItem();
					if (treeItem != null) {
						MenuItem removeMenu = new MenuItem("Remove");
						removeMenu.setOnAction(e11 -> {
							TreeItem<TreeViewNode> parent = treeItem.getParent();
							if (parent == null) {
								setValue(null);
								treeView.setRoot(null);
							} else {
								parent.getChildren().remove(treeItem);
								removeChild(parent.getValue().treeNode, treeItem.getValue().treeNode);
								firePropertyChangeListener();
							}
						});
						menus.add(removeMenu);
					}
					if (treeItem != null || getValue() == null) {
						MenuItem newInstMenu = new MenuItem("Add child");
						newInstMenu.setOnAction(e12 -> {
							PropertyEditor<?> pe = getPropertyEditorClone(null);
							if (treeItem == null) {
								TreeRoot<T> treeNode = new TreeRoot<>(getGenericComponentType());
								pe.addPropertyChangeListener(() -> {
									setValue(treeNode, pe.getValue());
									firePropertyChangeListener();
								});
								pe.setValueFromObj(pe.getValue());
								setValue(treeNode);
								treeView.setRoot(new TreeItem<>(new TreeViewNode(treeNode, pe)));
							} else {
								TreeViewNode tvn = treeItem.getValue();
								TreeNode<?> newTreeNode = new TreeNode<>();
								addChild(tvn.treeNode, newTreeNode);
								pe.addPropertyChangeListener(() -> {
									setValue(newTreeNode, pe.getValue());
									firePropertyChangeListener();
								});
								setValue(newTreeNode, pe.getValue());
								firePropertyChangeListener();
								treeItem.getChildren().add(new TreeItem<>(new TreeViewNode(newTreeNode, pe)));
							}
						});
						menus.add(newInstMenu);
					}
					if (treeItem != null && !treeItem.isLeaf()) {
						MenuItem eaMenu = new MenuItem("Expand All");
						eaMenu.setOnAction(e2 -> expandTreeView(treeItem));
						menus.add(eaMenu);
					}
					if (!menus.isEmpty()) {
						ContextMenu cm = new ContextMenu();
						cm.getItems().addAll(menus);
						cm.show(cell, e.getScreenX(), e.getScreenY());
					}
				}
			});
			return cell;
		});
		return treeView;
	}

	private TreeItem<TreeViewNode> getTreeItem(TreeNode<?> treeNode) {
		TreeItem<TreeViewNode> treeItem = new TreeItem<>();
		ArrayList<TreeItem<TreeViewNode>> nodes = new ArrayList<>();
		if (treeNode.getChildren() != null)
			for (TreeNode<?> child : treeNode.getChildren()) {
				treeItem.getChildren().add(getTreeItem(child));
				nodes.add(treeItem);
			}
		PropertyEditor<?> propertyEditor = getPropertyEditorClone(null);
		propertyEditor.setValueFromObj(treeNode.getValue());
		propertyEditor.addPropertyChangeListener(() -> {
			setValue(treeNode, propertyEditor.getValue());
			firePropertyChangeListener();
		});
		treeItem.setValue(new TreeViewNode(treeNode, propertyEditor));
		return treeItem;
	}

	@Override
	public boolean hasCustomEditor() {
		return true;
	}

	private static int indexOfFirst(String text, char delimiter) {
		int cpt = 0;
		int i = 0;
		for (; i < text.length(); i++) {
			char c = text.charAt(i);
			if (c == delimiter && cpt == 0)
				break;
			if (c == '{')
				cpt++;
			else if (c == '}')
				cpt--;
		}
		return i;
	}

	private void populateNode(String text, TreeNode<?> treeNode) {
		PropertyEditor<?> pe = getDefaultPatternEditor(null);
		int id = indexOfFirst(text, '{');
		String value;
		String children;
		if (id >= 0) {
			value = text.substring(0, id);
			children = text.substring(id);
		} else {
			value = text;
			children = null;
		}
		if (!value.isEmpty()) {
			try {
				getDefaultPatternEditor(null).setAsText(value);
			} catch (Exception e) {
				System.err.println(toString() + " error when set value: " + value + ", cause: " + e.getMessage());
			}
			setValue(treeNode, getDefaultPatternEditor(null).getValue());
		}
		if (!children.isEmpty()) {
			ArrayList<Object> chilren = new ArrayList<>();
			while (!children.isEmpty()) {
				children = children.substring(1);
				int idChild = indexOfFirst(children, '}');
				if (id >= 0) {
					TreeNode<?> tn = new TreeNode<>();
					populateNode(children.substring(0, idChild), tn);
					chilren.add(tn);
					children = children.substring(idChild + 1);
				} else {
					pe.setAsText(value);
					chilren.add(pe.getValue());
				}
			}
			setchildren(treeNode, chilren);
		}
	}

	private static void removeChild(TreeNode<?> treeNode, Object child) {
		try {
			lookup.bind(treeNode, "removeChild", removeChildMethodType).invoke(child);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	@Override
	public void removeSubBeans(Object beanToRemove) {
		TreeRoot<T> tree = getValue();
		if (tree == null || !(getDefaultPatternEditor(null) instanceof BeanEditor))
			return;
		foreach(tree, node -> {
			if (node.getValue() == beanToRemove)
				node.setValue(null);
		});
	}

	@Override
	public void beanListChanged() {}

	@Override
	public List<Object> getSubBeans() {
		TreeRoot<T> tree = getValue();
		if (tree == null || !(getDefaultPatternEditor(null) instanceof BeanEditor))
			return null;
		ArrayList<Object> subBeans = new ArrayList<>();
		foreach(tree, node -> {
			Object value = node.getValue();
			if (value != null)
				subBeans.add(node.getValue());
		});
		return subBeans;
	}

	private void foreach(TreeRoot<T> tree, Consumer<TreeNode<?>> consumer) {
		LinkedList<TreeNode<?>> todoNode = new LinkedList<>();
		todoNode.add(tree);
		while (!todoNode.isEmpty()) {
			TreeNode<?> node = todoNode.pop();
			consumer.accept(node);
			if (node.getChildren() != null)
				for (TreeNode<?> subNode : node.getChildren())
					if (subNode != null)
						todoNode.push(subNode);
		}
	}

	private void saveTreeNode(TreeNode<?> tree, StringBuilder text) {
		PropertyEditor<?> pe = getDefaultPatternEditor(null);
		Object value = tree.getValue();
		if (value != null) {
			pe.setValueFromObj(value);
			text.append(pe.getAsText());
		}
		if (tree.getChildren() != null)
			for (TreeNode<?> child : tree.getChildren()) {
				text.append('{');
				saveTreeNode(child, text);
				text.append('}');
			}
	}

	@Override
	public void setAsText(String text) {
		// if (type == null)
		// throw new IllegalAccessError("The type of the treeEditor need to be set before");
		if (text == null || text.isEmpty()) {
			setValue(null);
			return;
		}
		TreeRoot<T> rootNode = new TreeRoot<>(getGenericComponentType());
		populateNode(text, rootNode);
		setValue(rootNode);
	}

	private static void setValue(TreeNode<?> treeNode, Object value) {
		try {
			lookup.bind(treeNode, "setValue", setValueMethodType).invoke(value);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	private static void setchildren(TreeNode<?> treeNode, ArrayList<Object> children) {
		try {
			lookup.bind(treeNode, "setChildren", setChildrensMethodType).invoke(children);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	@Override
	public void setValue(TreeRoot<T> value) {
		if (value != null)
			// if (componentType == null)
			// componentType = ((TreeRoot<?>) value).getType();
			if (value.getType() != getComponentType())
				throw new IllegalArgumentException("The type of the treeEditor: " + getComponentType() + " do not correponds to the type of the value: " + ((TreeRoot<?>) value).getType());
		super.setValue(value);
	}

	@Override
	public void updateCustomEditor() {
		TreeRoot<?> tree = getValue();
		@SuppressWarnings("unchecked")
		TreeView<TreeViewNode> treeView = (TreeView<TreeViewNode>) customEditor();
		treeView.setRoot(tree != null ? getTreeItem(tree) : null);
	}

	@Override
	public boolean isInline() {
		return false;
	}
}

class TreeViewNode {
	public TreeNode<?> treeNode;
	public PropertyEditor<?> propertyEditor;

	public TreeViewNode(TreeNode<?> treeNode, PropertyEditor<?> propertyEditor) {
		this.treeNode = treeNode;
		this.propertyEditor = propertyEditor;
	}
}
