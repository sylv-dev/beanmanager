/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.container;

// T le type de l'éditeur, U le type qu'il contient
public abstract class GenericPropertyContainerEditor<T, U> extends PropertyContainerEditor<T> {
	private final Class<U> typedComponentType;

	public GenericPropertyContainerEditor(Class<U> componentType) {
		super(componentType);
		this.typedComponentType = componentType;
	}

	public Class<U> getGenericComponentType() {
		return this.typedComponentType;
	}
}
