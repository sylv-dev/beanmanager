/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors;

public interface ChoiceEditor<T> {
	void setChoices(T[] choices);

	@SuppressWarnings("unchecked")
	default void setChoicesFromObjects(Object[] choices) {
		setChoices((T[]) choices);
	}
}
