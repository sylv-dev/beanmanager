/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.beanmanager.BeanManager;

public abstract class GenericPropertyEditor<T> extends PropertyEditor<T> {
	public static final String TYPE_SEPARATOR = "_";

	public static Class<?>[] getTypeFromText(int nbGenericParameters, String text) {
		Class<?>[] types = new Class<?>[nbGenericParameters];
		for (int k = 0; k < types.length; k++) {
			int splitIndex = text.indexOf(GenericPropertyEditor.TYPE_SEPARATOR);
			try {
				types[k] = BeanManager.getClassFromDescriptor(text.substring(0, splitIndex));
			} catch (ClassNotFoundException e) {
				System.err.println("Cannot get the type of the generic from text. The class with name: " + text.substring(0, splitIndex) + " does not exits");
			}
			text = text.substring(splitIndex + 1);
		}
		return types;
	}

	private final Class<?>[] types;

	public GenericPropertyEditor(Class<?>[] types) {
		this.types = types;
	}

	@Override
	public final String getAsText() {
		StringBuilder sb = new StringBuilder();
		for (Class<?> type : this.types)
			sb.append(BeanManager.getDescriptorFromClass(type) + TYPE_SEPARATOR);
		return sb.toString() + getGenericAsText();
	}

	protected abstract String getGenericAsText();

	public Class<?>[] getTypes() {
		return this.types;
	}

	protected abstract T readGenericValue(DataInput raf) throws IOException;

	@Override
	public T readValue(DataInput raf) throws IOException {
		for (int i = 0; i < this.types.length; i++)
			raf.readUTF();
		return readGenericValue(raf);
	}

	@Override
	public final void setAsText(String text) {
		for (int i = 0; i < this.types.length; i++)
			text = text.substring(text.indexOf(TYPE_SEPARATOR) + 1);
		setGenericAsText(text);
	}

	protected abstract void setGenericAsText(String text);

	protected abstract void writeGenericValue(DataOutput raf, T value) throws IOException;

	@Override
	public void writeValue(DataOutput raf, T value) throws IOException {
		for (Class<?> type : this.types)
			raf.writeUTF(BeanManager.getDescriptorFromClass(type));
		writeGenericValue(raf, value);
	}

	@Override
	public boolean canContainForbiddenCharacter() { // Je peux contenir un type avec des forbiden character mais je ne le saurai que en créant l'éditeur...
		// for (Class<?> type : types) {
		// PropertyEditor<?> e = PropertyEditorManager.findEditor(type, "");
		// if (e != null && e.canContainForbiddenCharacter())
		// return true;
		// }
		return true;
	}
}
