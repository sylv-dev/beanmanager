/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.struct;

import java.util.HashSet;
import java.util.Objects;

public class Selection<T> {
	private final HashSet<T> selected;
	private final Class<T> componentType;

	public Selection(HashSet<T> selected, Class<T> componentType) {
		if (selected == null)
			throw new IllegalArgumentException("selected argument cannot be null");
		this.selected = selected;
		this.componentType = componentType;
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof Selection ? ((Selection<?>) obj).componentType.equals(this.componentType) && this.selected.equals(((Selection<?>) obj).selected) : false;
	}

	public Class<T> getComponentType() {
		return this.componentType;
	}

	public HashSet<T> getSelected() {
		return new HashSet<>(this.selected);
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.componentType, this.selected);
	}

	public boolean isSelected(T selection) {
		return this.selected == null ? false : this.selected.contains(selection);
	}

	@Override
	public String toString() {
		return this.selected.toString();
	}
}
