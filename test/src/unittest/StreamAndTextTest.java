/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package unittest;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.lang.reflect.Array;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Year;
import java.util.Arrays;
import java.util.BitSet;
import java.util.HashSet;
import java.util.Random;
import java.util.function.Supplier;

import javax.vecmath.Color4f;
import javax.vecmath.GMatrix;
import javax.vecmath.GVector;
import javax.vecmath.Matrix3d;
import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4d;
import javax.vecmath.Matrix4f;
import javax.vecmath.Point2d;
import javax.vecmath.Point2f;
import javax.vecmath.Point2i;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Point3i;
import javax.vecmath.Point4d;
import javax.vecmath.Point4f;
import javax.vecmath.Point4i;
import javax.vecmath.Vector2d;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4d;
import javax.vecmath.Vector4f;

import org.beanmanager.editors.container.ArrayEditor;
import org.beanmanager.editors.primitive.number.ControlType;
import org.beanmanager.struct.Selection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import com.googlecode.junittoolbox.ParallelParameterized;

import javafx.scene.paint.Color;

@RunWith(value = ParallelParameterized.class)
public class StreamAndTextTest {
	private static final int NB_RUN = 10;
	private static final int SEED = 369;
	private static final int MAX_LEVEL = 3;
	private static final int MAX_SIZE = 10;
	private static final double NULL_RATIO = 0.1;

	private static final Random RAND = new Random(SEED);

	private static final Class<?>[] TESTED_TYPES = new Class<?>[] { boolean.class, byte.class, char.class, short.class, int.class, long.class, float.class, double.class, Boolean.class, Byte.class,
			Character.class, Short.class, Integer.class, Long.class, Float.class, Double.class, String.class, File.class, Color.class, Color4f.class, ControlType.class, InetSocketAddress.class,
			Selection.class, Point2i.class, Point2f.class, Point2d.class, Point3i.class, Point3f.class, Point3d.class, Point4i.class, Point4f.class, Point4d.class, Vector2f.class, Vector2d.class,
			Vector3f.class, Vector3d.class, Vector4f.class, Vector4d.class, Matrix3f.class, Matrix3d.class, Matrix4f.class, Matrix4d.class, LocalDate.class, LocalTime.class, LocalDateTime.class,
			GMatrix.class, GVector.class, BitSet.class/* , CanTrame.class */, Path.class };
	// static {
	// ModuleManager.loadEmbeddedAndInternalModules();
	// }

	@Parameter
	public Class<?> type;

	// Single parameter, use Object[]
	@Parameters(name = "{index}: type - {0}")
	public static Class<?>[] data() {
		return TESTED_TYPES;
	}

	@Test
	public void testType() {
		assertTrue(checkPropertyArray(RAND, MAX_LEVEL, MAX_SIZE, NULL_RATIO, this.type, () -> generateRandomValue(this.type, RAND, MAX_SIZE, TESTED_TYPES)));
	}

	@SuppressWarnings("unchecked")
	public static Object generateRandomValue(Class<?> type, Random rand, int maxSize, Class<?>[] testedTypes) {
		if (type.equals(boolean.class) || type.equals(Boolean.class))
			return rand.nextBoolean();
		else if (type.equals(byte.class) || type.equals(Byte.class))
			return (byte) rand.nextInt();
		else if (type.equals(char.class) || type.equals(Character.class)) {
			char value = (char) rand.nextInt();
			while (Character.getType(value) == Character.SURROGATE)
				value = (char) rand.nextInt();
			return value;
		} else if (type.equals(short.class) || type.equals(Short.class))
			return (short) rand.nextInt();
		else if (type.equals(int.class) || type.equals(Integer.class))
			return rand.nextInt();
		else if (type.equals(long.class) || type.equals(Long.class))
			return rand.nextLong();
		else if (type.equals(float.class) || type.equals(Float.class))
			return rand.nextFloat();
		else if (type.equals(double.class) || type.equals(Double.class))
			return rand.nextDouble();
		else if (type.equals(String.class)) {
			String s = new String();
			int nbChar = rand.nextInt(10);
			for (int i = 0; i < nbChar; i++) {
				char c = (char) rand.nextInt();
				while (Character.getType(c) == Character.SURROGATE || c == '\n' || c == '\r')
					c = (char) rand.nextInt();
				s = s + c;
			}
			return s;
		} else if (type.equals(File.class))
			return new File((String) generateRandomValue(String.class, rand, maxSize, testedTypes));
		else if (type.equals(Path.class))
			while (true)
				try {
					return Path.of((String) generateRandomValue(String.class, rand, maxSize, testedTypes));
				} catch (InvalidPathException e) {}
		else if (type.equals(Color.class))
			return new Color(rand.nextDouble(), rand.nextDouble(), rand.nextDouble(), rand.nextDouble());
		else if (type.equals(Color4f.class))
			return new Color4f(rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat());
		else if (type.equals(ControlType.class))
			return ControlType.values()[rand.nextInt(ControlType.values().length)];
		else if (type.equals(InetSocketAddress.class)) {
			int randChoice = rand.nextInt(4);
			if (randChoice == 0)
				return new InetSocketAddress(rand.nextInt(0xFFFF));
			else if (randChoice == 1)
				try {
					return new InetSocketAddress(InetAddress.getByAddress(new byte[] { (byte) rand.nextInt(0xFF), (byte) rand.nextInt(0xFF), (byte) rand.nextInt(0xFF), (byte) rand.nextInt(0xFF) }),
							rand.nextInt(0xFFFF));
				} catch (UnknownHostException e) {
					e.printStackTrace();
					return null;
				}
			else if (randChoice == 2)
				try {
					return new InetSocketAddress(InetAddress.getByAddress(new byte[] { (byte) rand.nextInt(0xFF), (byte) rand.nextInt(0xFF), (byte) rand.nextInt(0xFF), (byte) rand.nextInt(0xFF),
							(byte) rand.nextInt(0xFF), (byte) rand.nextInt(0xFF), (byte) rand.nextInt(0xFF), (byte) rand.nextInt(0xFF), (byte) rand.nextInt(0xFF), (byte) rand.nextInt(0xFF),
							(byte) rand.nextInt(0xFF), (byte) rand.nextInt(0xFF), (byte) rand.nextInt(0xFF), (byte) rand.nextInt(0xFF), (byte) rand.nextInt(0xFF), (byte) rand.nextInt(0xFF) }),
							rand.nextInt(0xFFFF));
				} catch (UnknownHostException e) {
					e.printStackTrace();
					return null;
				}
			else
				return InetSocketAddress.createUnresolved((String) generateRandomValue(String.class, rand, maxSize, testedTypes), rand.nextInt(0xFFFF));
		} else if (type.equals(Selection.class)) {
			Class<?> selectionType;
			do
				selectionType = testedTypes[rand.nextInt(testedTypes.length)];
			while (selectionType.isPrimitive());
			Object[] array = (Object[]) Array.newInstance(selectionType, rand.nextInt(maxSize));
			for (int i = 0; i < array.length; i++)
				array[i] = generateRandomValue(selectionType, rand, maxSize, testedTypes);
			return new Selection<>(new HashSet<>(Arrays.asList(array)), (Class<Object>) selectionType);
		} else if (type.equals(Point2i.class))
			return new Point2i(rand.nextInt(), rand.nextInt());
		else if (type.equals(Point2f.class))
			return new Point2f(rand.nextFloat(), rand.nextFloat());
		else if (type.equals(Point2d.class))
			return new Point2d(rand.nextDouble(), rand.nextDouble());
		else if (type.equals(Point3i.class))
			return new Point3i(rand.nextInt(), rand.nextInt(), rand.nextInt());
		else if (type.equals(Point3f.class))
			return new Point3f(rand.nextFloat(), rand.nextFloat(), rand.nextFloat());
		else if (type.equals(Point3d.class))
			return new Point3d(rand.nextDouble(), rand.nextDouble(), rand.nextDouble());
		else if (type.equals(Point4i.class))
			return new Point4i(rand.nextInt(), rand.nextInt(), rand.nextInt(), rand.nextInt());
		else if (type.equals(Point4f.class))
			return new Point4f(rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat());
		else if (type.equals(Point4d.class))
			return new Point4d(rand.nextDouble(), rand.nextDouble(), rand.nextDouble(), rand.nextDouble());
		else if (type.equals(Vector2f.class))
			return new Vector2f(rand.nextFloat(), rand.nextFloat());
		else if (type.equals(Vector2d.class))
			return new Vector2d(rand.nextDouble(), rand.nextDouble());
		else if (type.equals(Vector3f.class))
			return new Vector3f(rand.nextFloat(), rand.nextFloat(), rand.nextFloat());
		else if (type.equals(Vector3d.class))
			return new Vector3d(rand.nextDouble(), rand.nextDouble(), rand.nextDouble());
		else if (type.equals(Vector4f.class))
			return new Vector4f(rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat());
		else if (type.equals(Vector4d.class))
			return new Vector4d(rand.nextDouble(), rand.nextDouble(), rand.nextDouble(), rand.nextDouble());
		else if (type.equals(GVector.class)) {
			double[] val = new double[rand.nextInt(maxSize)];
			for (int i = 0; i < val.length; i++)
				val[i] = rand.nextDouble();
			return new GVector(val);
		} else if (type.equals(Matrix3f.class))
			return new Matrix3f(rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat());
		else if (type.equals(Matrix3d.class))
			return new Matrix3d(rand.nextDouble(), rand.nextDouble(), rand.nextDouble(), rand.nextDouble(), rand.nextDouble(), rand.nextDouble(), rand.nextDouble(), rand.nextDouble(),
					rand.nextDouble());
		else if (type.equals(Matrix4f.class))
			return new Matrix4f(rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat(),
					rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat());
		else if (type.equals(Matrix4d.class))
			return new Matrix4d(rand.nextDouble(), rand.nextDouble(), rand.nextDouble(), rand.nextDouble(), rand.nextDouble(), rand.nextDouble(), rand.nextDouble(), rand.nextDouble(),
					rand.nextDouble(), rand.nextDouble(), rand.nextDouble(), rand.nextDouble(), rand.nextDouble(), rand.nextDouble(), rand.nextDouble(), rand.nextDouble());
		else if (type.equals(GMatrix.class)) {
			int size = rand.nextInt(maxSize);
			double[] val = new double[size * size];
			for (int i = 0; i < val.length; i++)
				val[i] = rand.nextDouble();
			return new GMatrix(size, size, val);
		} else if (type.equals(LocalDate.class))
			return LocalDate.of(Year.MIN_VALUE + rand.nextInt(Year.MAX_VALUE - Year.MIN_VALUE), 1 + rand.nextInt(11), 1 + rand.nextInt(28));
		else if (type.equals(LocalTime.class))
			return LocalTime.of(rand.nextInt(23), rand.nextInt(60), rand.nextInt(60), rand.nextInt(999_999_999));
		else if (type.equals(LocalDateTime.class))
			return LocalDateTime.of((LocalDate) generateRandomValue(LocalDate.class, rand, maxSize, testedTypes), (LocalTime) generateRandomValue(LocalTime.class, rand, maxSize, testedTypes));
		else if (type.equals(BitSet.class)) {
			BitSet bs = new BitSet(rand.nextInt(maxSize));
			for (int i = 0; i < bs.size(); i++)
				if (rand.nextBoolean())
					bs.set(i);
			return bs;
		} else
			throw new IllegalArgumentException("No generator for type: " + type);
	}

	private static boolean checkPropertyArray(Random rand, int maxlevel, int maxSize, double nullRatio, Class<?> arrayType, Supplier<?> supplier) {
		try {
			int textError = 0;
			for (int k = 0; k < NB_RUN; k++) {
				Class<?> type = arrayType;
				for (int i = 0; i < maxlevel; i++)
					type = Array.newInstance(type, 0).getClass();
				Object array = Array.newInstance(type, (int) (rand.nextDouble() * maxSize));
				populateArray(array, rand, supplier, 0.01);
				// Text test
				ArrayEditor<?> ed = new ArrayEditor<>(array.getClass());
				ed.setValueFromObj(array);
				String text = ed.getAsText();
				ed.setAsText(text);
				Object resValue = ed.getValue();
				if (!compareArray(array, resValue)) {
					textError++;
					ed.setAsText(text);
					ed.setValueFromObj(array);
					text = ed.getAsText();
					ed.setAsText(text);
					Object v = ed.getValue();
					System.out.println(v);
					System.out.println(compareArray(array, resValue));
				}
			}
			int streamError = 0;
			for (int k = 0; k < NB_RUN; k++) {
				int level = 1 + (int) (rand.nextDouble() * maxlevel);
				Class<?> type = arrayType;
				for (int i = 0; i < level - 1; i++)
					type = Array.newInstance(type, 0).getClass();
				Object array = Array.newInstance(type, (int) (rand.nextDouble() * maxSize));
				populateArray(array, rand, supplier, 0.01);
				ArrayEditor<?> ed = new ArrayEditor<>(array.getClass());
				// Flow test
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				try (DataOutputStream dos = new DataOutputStream(baos)) {
					ed.writeValueFromObj(dos, array);
				}
				try (DataInputStream dis = new DataInputStream(new ByteArrayInputStream(baos.toByteArray()))) {
					Object array2 = ed.readValue(dis);
					if (!compareArray(array, array2))
						streamError++;
				}
			}
			if (textError != 0 || streamError != 0) {
				if (textError == 0)
					System.err.println(arrayType.getSimpleName() + ": Text ok, Stream fail");
				else if (streamError == 0)
					System.err.println(arrayType.getSimpleName() + ": Text fail, Stream ok");
				else
					System.err.println(arrayType.getSimpleName() + ": both failed");
				return false;
			}
			return true;
		} catch (Exception e) {
			System.err.println(arrayType.getSimpleName() + ": Exception");
			e.printStackTrace();
			return false;
		}
	}

	private static boolean compareArray(Object a1, Object a2) {
		if (a1 == null ^ a2 == null)
			return false;
		if (a1 == null && a2 == null)
			return true;
		if (a1.getClass().isArray() ^ a2.getClass().isArray())
			return false;
		if (a1.getClass().isArray()) {
			if (Array.getLength(a1) != Array.getLength(a2))
				return false;
			for (int i = 0; i < Array.getLength(a1); i++)
				if (!compareArray(Array.get(a1, i), Array.get(a2, i)))
					return false;
		} else
			return a1.equals(a2);
		return true;

	}

	private static void populateArray(Object array, Random rand, Supplier<?> supplier, double nullRatio) {
		int size = Array.getLength(array);
		Class<?> subType = array.getClass().getComponentType();
		for (int i = 0; i < size; i++)
			if (!subType.isArray())
				Array.set(array, i, !subType.isPrimitive() && rand.nextDouble() < nullRatio ? null : supplier.get());
			else if (rand.nextDouble() < nullRatio)
				Array.set(array, i, null);
			else {
				Class<?> subsubType = subType.getComponentType();
				Object subArray = Array.newInstance(subsubType, (int) (rand.nextDouble() * 10));
				populateArray(subArray, rand, supplier, nullRatio);
				Array.set(array, i, subArray);
			}
	}
}
