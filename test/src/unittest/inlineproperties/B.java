/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package unittest.inlineproperties;

import java.util.Arrays;
import java.util.Objects;

import org.beanmanager.editors.container.BeanInfo;

public class B {
	private String bId = new String("je{ suis ; }B ;");
	@BeanInfo(inline = true)
	private C c = new C();
	private int y = 4;
	private double[] tab;

	public B() {}

	public B(String bId, C c, double[] tab, int y) {
		this.bId = bId;
		this.c = c;
		this.tab = tab;
		this.y = y;
	}

	public String getbId() {
		return this.bId;
	}

	public void setbId(String bId) {
		this.bId = bId;
	}

	public C getC() {
		return this.c;
	}

	public void setC(C c) {
		this.c = c;
	}

	public int getY() {
		return this.y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public double[] getTab() {
		return this.tab;
	}

	public void setTab(double[] tab) {
		this.tab = tab;
	}

	@Override
	public boolean equals(Object obj) {
		return this.c.equals(((B) obj).c) && Arrays.equals(this.tab, ((B) obj).tab) && this.bId.equals(((B) obj).bId) && this.y == ((B) obj).y;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.c, this.tab, this.bId, this.y);
	}
}
