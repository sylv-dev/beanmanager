/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package unittest.inlineproperties;

import java.io.File;
import java.util.Random;

import org.beanmanager.BeanManager;
import org.beanmanager.tools.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import com.googlecode.junittoolbox.ParallelParameterized;

import unittest.StreamAndTextTest;

@RunWith(value = ParallelParameterized.class)
public class InlineTest {
	private static final int SEED_OFFSET = 648;
	private static final int NB_RUN = 1000;
	private static final int MAX_SIZE = 10;

	private static final Integer[] TESTED_SEEDS;
	static {
		TESTED_SEEDS = new Integer[10];
		for (int i = 0; i < TESTED_SEEDS.length; i++)
			TESTED_SEEDS[i] = i + SEED_OFFSET;
	}

	@Parameter
	public int seed;

	// Single parameter, use Object[]
	@Parameters(name = "{index}: type - {0}")
	public static Integer[] data() {
		return TESTED_SEEDS;
	}

	@Test
	public void checkInline() {
		Logger.setLevel(Logger.SAVE_OR_LOAD_FILE, false);
		Random rand = new Random(this.seed);
		File testFile = new File("InlineTest" + this.seed);
		testFile.deleteOnExit();
		int textError = 0;
		for (int i = 0; i < NB_RUN; i++) {
			A a = new A(
					new B((String) StreamAndTextTest.generateRandomValue(String.class, rand, MAX_SIZE, null),
							new C((String) StreamAndTextTest.generateRandomValue(String.class, rand, MAX_SIZE, null),
									(Integer) StreamAndTextTest.generateRandomValue(Integer.class, rand, MAX_SIZE, null), new String[0]),
							new double[0], (Integer) StreamAndTextTest.generateRandomValue(Integer.class, rand, MAX_SIZE, null)),
					(Integer) StreamAndTextTest.generateRandomValue(Integer.class, rand, MAX_SIZE, null));
			new BeanManager(a, "").save(testFile, true);
			A a2 = new A();
			new BeanManager(a2, "").load(testFile);// { et } ne sont pas génant, ; c'est la merde
			if (!a.equals(a2))
				textError++;
		}
		if (textError != 0)
			System.err.println("Text fail");
	}
}
