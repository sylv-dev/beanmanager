/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package unittest.inlineproperties;

import java.util.Arrays;
import java.util.Objects;

public class C {
	private String cId = new String("je{ suis ; }C ;");
	private int z;
	private String[] tabS;

	public C() {}

	public C(String cId, int z, String[] tabS) {
		this.cId = cId;
		this.z = z;
		this.tabS = tabS;
	}

	public String getcId() {
		return this.cId;
	}

	public void setcId(String cId) {
		this.cId = cId;
	}

	public int getZ() {
		return this.z;
	}

	public void setZ(int z) {
		this.z = z;
	}

	public String[] getTabS() {
		return this.tabS;
	}

	public void setTabS(String[] tabS) {
		this.tabS = tabS;
	}

	@Override
	public boolean equals(Object obj) {
		return this.cId.equals(((C) obj).cId) && Arrays.deepEquals(this.tabS, ((C) obj).tabS) && this.z == ((C) obj).z;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.cId, this.tabS, this.z);
	}
}
